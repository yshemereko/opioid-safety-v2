import React, { Component } from 'react';
import { HashRouter as Router, Switch } from "react-router-dom";
import 'typeface-roboto';

import DefaultLayout from './components/DefaultLayout';

import ReportContainer from './containers/ReportContainer';
import ReportTestContainer from './containers/ReportTestContainer';
import AssessmentChartsContainer from './containers/AssessmentChartsContainer';
import ResultsContainer from './containers/ResultsContainer';
import AssessmentContainer from './containers/AssessmentContainer';
import InfoPage from './components/InfoPage';
import About from './components/About';
import InfoLinks from './components/InfoLinks';
import Article from './components/Article';
import PageNotFound from './components/PageNotFound';
import ReportSummaryContainer from "./containers/ReportSummaryContainer"
import { MuiThemeProvider } from '@material-ui/core/styles';
import GraphsContextMenu from './components/menu/GraphsContextMenu'

import theme from './theme.js';


class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Router>
          <Switch>
            <DefaultLayout exact path="/test" component={ReportTestContainer} title="Report" />
            <DefaultLayout exact path="/" component={ReportContainer} title="Report" />
            <DefaultLayout exact path="/assessment/:assessmentId" component={AssessmentContainer} title="Report" />
            <DefaultLayout exact path="/graphs" component={AssessmentChartsContainer} title="Graphs" />
            <DefaultLayout exact path="/results/:reportId" component={ReportSummaryContainer} title="Summary" />
            <DefaultLayout path="/results" contextMenu={<GraphsContextMenu to={"/graphs"}/>} component={ResultsContainer} title="Results" />
            <DefaultLayout path="/about" component={About} title="About" />
            <DefaultLayout exact path="/info" component={InfoPage} title="Info" />
            <DefaultLayout exact path="/info/:subject" component={InfoLinks} title="" />
            <DefaultLayout path="/info/:subject/:slug" component={Article} title="" />
            <DefaultLayout component={PageNotFound} title="Not Found" />
          </Switch>
        </Router>
      </MuiThemeProvider>
    );
  }
}

export default App;
