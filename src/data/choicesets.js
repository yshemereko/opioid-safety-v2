// @flow
export interface ChoicesInterface{
  title: string;
  value: string;
  score: number;
}

export const choicesSet24: ChoicesInterface[] = [
  {title: 'Sleepless', value: '1', score: 1},

  {title: '', value: '2', score: 2},
  {title: '', value: '3', score: 3},
  {title: '', value: '4', score: 4},
  {title: '', value: '5', score: 5},

  {title: '', value: '6', score: 6},
  {title: '', value: '7', score: 7},
  {title: '', value: '8', score: 8},
  {title: '', value: '9', score: 9},

  {title: 'Rested', value: '10', score: 10},   
];

export const choicesSet25: ChoicesInterface[] = [
  {title: 'Hopeless', value: '1', score: 1},
  {title: '', value: '2', score: 2},
  {title: '', value: '3', score: 3},
  {title: '', value: '4', score: 4},
  {title: '', value: '5', score: 5},

  {title: '', value: '6', score: 6},
  {title: '', value: '7', score: 7},
  {title: '', value: '8', score: 8},
  {title: '', value: '9', score: 9},
  {title: 'Hopeful', value: '10', score: 10},   
];

export const choicesSet26: ChoicesInterface[] = [
  {title: 'Worthless', value: '1', score: 1},
  {title: '', value: '2', score: 2},
  {title: '', value: '3', score: 3},
  {title: '', value: '4', score: 4},
  {title: '', value: '5', score: 5},

  {title: '', value: '6', score: 6},
  {title: '', value: '7', score: 7},
  {title: '', value: '8', score: 8},
  {title: '', value: '9', score: 9},
  {title: 'Valuable', value: '10', score: 10},   
];

export const choicesSet27: ChoicesInterface[] = [
  {title: 'Sleepy', value: '1', score: 1},
  {title: '', value: '2', score: 2},
  {title: '', value: '3', score: 3},
  {title: '', value: '4', score: 4},
  {title: '', value: '5', score: 5},

  {title: '', value: '6', score: 6},
  {title: '', value: '7', score: 7},
  {title: '', value: '8', score: 8},
  {title: '', value: '9', score: 9},
  {title: 'Alert', value: '10', score: 10},   
];

export const choicesSet28: ChoicesInterface[] = [
  {title: 'Dull', value: '0', score: 1},
  {title: 'Sharp', value: '1', score: 2},
  {title: 'Radiating', value: '2', score: 3},
  {title: 'Continuous', value: '3', score: 4},
  {title: 'Intermittent', value: '4', score: 5},
  {title: 'Tingling', value: '5', score: 6},
  {title: 'Aching', value: '6', score: 7},
  {title: 'Numb', value: '7', score: 8},
  {title: 'Stabbing', value: '8', score: 9},
];

export const choicesSet29: ChoicesInterface[] = [
  {title: '1', value: '1', score: 1},
  {title: '2', value: '2', score: 2},
  {title: '3', value: '3', score: 3},
  {title: '4', value: '4', score: 4},
  {title: '5', value: '5', score: 5},
  {title: '6', value: '6', score: 6},
  {title: '7', value: '7', score: 7},
  {title: '8+', value: '8', score: 8},
];


export const choicesSet30: ChoicesInterface[] = [
  {title: 'No', value: '0', score: 0},
  {title: 'Yes', value: '1', score: 1},
];

export const choicesSetYesNo = choicesSet30;

export const choicesSet31: ChoicesInterface[] = [
  {title: '8oz',  value: '0', score: 1},
  {title: '16oz', value: '1', score: 2},
  {title: '24oz', value: '2', score: 3},
  {title: '32oz', value: '3', score: 4},
  {title: '40oz', value: '4', score: 5},
  {title: '48oz', value: '5', score: 6},
  {title: '56oz', value: '6', score: 7},
  {title: '64oz+', value: '7', score: 8},
];

export const choicesSet32: ChoicesInterface[] = [
  {title: '',  value: '0', score: 1}
];

export const choicesSet33: ChoicesInterface[] = [
  {title: 'Depressed', value: '1', score: 1},
  {title: '', value: '2', score: 2},
  {title: '', value: '3', score: 3},
  {title: '', value: '4', score: 4},
  {title: '', value: '5', score: 5},

  {title: '', value: '6', score: 6},
  {title: '', value: '7', score: 7},
  {title: '', value: '8', score: 8},
  {title: '', value: '9', score: 9},
  {title: 'Happy', value: '10', score: 10},   
];

export const choicesSet34: ChoicesInterface[] = [
  {title: 'Disconnected', value: '1', score: 1},
  {title: '', value: '2', score: 2},
  {title: '', value: '3', score: 3},
  {title: '', value: '4', score: 4},
  {title: '', value: '5', score: 5},

  {title: '', value: '6', score: 6},
  {title: '', value: '7', score: 7},
  {title: '', value: '8', score: 8},
  {title: '', value: '9', score: 9},
  {title: 'Involved', value: '10', score: 10},   
];

export const choicesSet35: ChoicesInterface[] = [
  {title: 'Unsafe', value: '1', score: 1},
  {title: '', value: '2', score: 2},
  {title: '', value: '3', score: 3},
  {title: '', value: '4', score: 4},
  {title: '', value: '5', score: 5},

  {title: '', value: '6', score: 6},
  {title: '', value: '7', score: 7},
  {title: '', value: '8', score: 8},
  {title: '', value: '9', score: 9},
  {title: 'Safe', value: '10', score: 10},   
];

export const choicesSet36: ChoicesInterface[] = [
  {title: '1', value: '1', score: 1},
  {title: '', value: '2', score: 2},
  {title: '', value: '3', score: 3},
  {title: '', value: '4', score: 4},
  {title: '', value: '5', score: 5},

  {title: '', value: '6', score: 6},
  {title: '', value: '7', score: 7},
  {title: '', value: '8', score: 8},
  {title: '', value: '9', score: 9},
  {title: '10', value: '10', score: 10},   
];


export const choicesSet37: ChoicesInterface[] = [
  {title: 'Worried', value: '1', score: 1},
  {title: '', value: '2', score: 2},
  {title: '', value: '3', score: 3},
  {title: '', value: '4', score: 4},
  {title: '', value: '5', score: 5},

  {title: '', value: '6', score: 6},
  {title: '', value: '7', score: 7},
  {title: '', value: '8', score: 8},
  {title: '', value: '9', score: 9},
  {title: 'Unconcerned', value: '10', score: 10},   
];

export const choicesSet38: ChoicesInterface[] = [
  {title: 'Tense', value: '1', score: 1},
  {title: '', value: '2', score: 2},
  {title: '', value: '3', score: 3},
  {title: '', value: '4', score: 4},
  {title: '', value: '5', score: 5},

  {title: '', value: '6', score: 6},
  {title: '', value: '7', score: 7},
  {title: '', value: '8', score: 8},
  {title: '', value: '9', score: 9},
  {title: 'Relaxed', value: '10', score: 10},   
];

export const choicesSet39: ChoicesInterface[] = [
  {title: 'Irritable', value: '1', score: 1},
  {title: '', value: '2', score: 2},
  {title: '', value: '3', score: 3},
  {title: '', value: '4', score: 4},
  {title: '', value: '5', score: 5},

  {title: '', value: '6', score: 6},
  {title: '', value: '7', score: 7},
  {title: '', value: '8', score: 8},
  {title: '', value: '9', score: 9},
  {title: 'Cheerful', value: '10', score: 10},   
];

export const choicesSet40: ChoicesInterface[] = [
  {title: 'Anxious', value: '1', score: 1},
  {title: '', value: '2', score: 2},
  {title: '', value: '3', score: 3},
  {title: '', value: '4', score: 4},
  {title: '', value: '5', score: 5},

  {title: '', value: '6', score: 6},
  {title: '', value: '7', score: 7},
  {title: '', value: '8', score: 8},
  {title: '', value: '9', score: 9},
  {title: 'Peaceful', value: '10', score: 10},   
];

export const choicesSet41: ChoicesInterface[] = [
  {title: 'Stressed', value: '1', score: 1},
  {title: '', value: '2', score: 2},
  {title: '', value: '3', score: 3},
  {title: '', value: '4', score: 4},
  {title: '', value: '5', score: 5},

  {title: '', value: '6', score: 6},
  {title: '', value: '7', score: 7},
  {title: '', value: '8', score: 8},
  {title: '', value: '9', score: 9},
  {title: 'Not Stressed', value: '10', score: 10},   
];

export const choicesSet42: ChoicesInterface[] = [
  {title: 'Tired', value: '1', score: 1},

  {title: '', value: '2', score: 2},
  {title: '', value: '3', score: 3},
  {title: '', value: '4', score: 4},
  {title: '', value: '5', score: 5},

  {title: '', value: '6', score: 6},
  {title: '', value: '7', score: 7},
  {title: '', value: '8', score: 8},
  {title: '', value: '9', score: 9},

  {title: 'Energetic', value: '10', score: 10},   
];
export const choicesSet43: ChoicesInterface[] = [
  {title: 'Distracted', value: '1', score: 1},

  {title: '', value: '2', score: 2},
  {title: '', value: '3', score: 3},
  {title: '', value: '4', score: 4},
  {title: '', value: '5', score: 5},

  {title: '', value: '6', score: 6},
  {title: '', value: '7', score: 7},
  {title: '', value: '8', score: 8},
  {title: '', value: '9', score: 9},

  {title: 'Focused', value: '10', score: 10},   
];

export const choicesSet44: ChoicesInterface[] = [
  {title: '1', value: '1', score: 1},
  {title: '2', value: '2', score: 2},
  {title: '3', value: '3', score: 3},
  {title: '4', value: '4', score: 4},
  {title: '5', value: '5', score: 5},

  {title: '6', value: '6', score: 6},
  {title: '7', value: '7', score: 7},
  {title: '8', value: '8', score: 8},
  {title: '9', value: '9', score: 9},
  {title: '10+', value: '10', score: 10},   
];

export const choicesSet45: ChoicesInterface[] = [
  {title: '1', value: '1', score: 1},
  {title: '2', value: '2', score: 2},
  {title: '3', value: '3', score: 3},
  {title: '4', value: '4', score: 4},
  {title: '5', value: '5', score: 5},
  {title: '6', value: '6', score: 6},
  {title: '7+', value: '7', score: 7}, 
];

export const choicesSet46: ChoicesInterface[] = [
  {title: '0-5 grams', value: '1', score: 1},
  {title: '6-10 grams', value: '2', score: 2},
  {title: '10+ grams', value: '3', score: 3}
];


