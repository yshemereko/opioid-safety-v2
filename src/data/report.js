// @flow
export type ReportInterface = {
  id: number,
  painMarkings: {[string]: any},
  created: number,
  updated: number,
  assessmentIds: number[],
  isComplete: boolean,
  step: number,
  painAllOver: number
};

export const makeReport = (id: number,created: number): ReportInterface => {
  return {
    id,
    painMarkings: {
      /*bodySectionId: painLevelId */

    },
    created,
    updated: 0,
    assessmentIds: [],
    isComplete: false,
    step: 0,
    painAllOver: 0
  }
}