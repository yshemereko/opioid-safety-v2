// @flow
import type {AssessmentConfigInterface,DisplayCallback,CalcScoreCallback} from './assessments'
import type {QuestionInterface} from './questions';
import {makeQuestion} from './questions';
export class GroupBuilder{
  id: number;
  title: string;
  images: string[];
  config: AssessmentConfigInterface;
  questionIds: number[] = [];
  display: DisplayCallback;
  score: CalcScoreCallback;
  constructor(config: AssessmentConfigInterface, id: number,title: string,images: string[]){
    this.id = id;
    this.title = title;
    this.images = images;
    this.config = config;
    this.display = config.getDisplay.bind(config);
    this.score = config.calcScore.bind(config);
  }

  addQuestion(question: QuestionInterface): GroupBuilder{
    this.questionIds.push(question.id);
    this.config.questions.push(question);//keep flattened copy of questions in config
    return this;
  }

  _getQuestionById(qid: number):QuestionInterface{
    let questions = this.config.questions.filter(q => q.id === qid);
    return questions.length > 0 ? questions[0] : makeQuestion(0,'')
  }

  getQuestions(): Array<QuestionInterface>{
    return this.questionIds.map(qid => this._getQuestionById(qid)).filter(q => q.id !== 0)
  }

  getDisplay(answers:{[string]: any}){

    return this.display(answers,this.getQuestions())
  }


  calcScore(answers:{[string]: any}){
    return this.score(answers,this.getQuestions())
  }

  getId(){
    return this.id
  }

  setDisplay(dispCb: DisplayCallback){
    this.display = dispCb.bind(this.config);
    return this;
  }

  setCalcScore(calcScoreCb: CalcScoreCallback){
    this.score = calcScoreCb.bind(this.config);
    return this;
  }

  getConfig(){
    return this.config;
  }
}

