// @flow
import {assessmentConfigIds,AssessmentConfig,graphAssessmentGroups} from './assessments';

const assessmentIds = graphAssessmentGroups.map(ga => ga.getConfig().id);

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function range(size, startAt = 0) {
    return [...Array(size).keys()].map(i => i + startAt);
}
const NUM_WEEKS = 5;
export const testData: any[] = range(NUM_WEEKS  /*weeks*/).map(weekIdx => {
  return assessmentIds.reduce((datum,aid) => {
    const aConf = AssessmentConfig[aid]
    datum['value' + aConf.id] = getRandomInt(10);
    datum['title' + aConf.id] = aConf.title
    return datum;
  },{name: 'week ' + (weekIdx + 1)});
});

export const testData2 = [];
// range(NUM_WEEKS  /*weeks*/).map(weekIdx => {
//   return assessmentIds.map((aid) => {
    
//     const aConf = AssessmentConfig[aid]
//     testData2.push({name: 'week ' + (weekIdx + 1), ["value" + aid]: getRandomInt(10),title: aConf.title})
//   });
// })

