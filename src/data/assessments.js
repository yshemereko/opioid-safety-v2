// @flow

import * as questions from './questions';
import * as scoring from './scoring';
import {GroupBuilder} from './groups';

import ExerciseIcon from '../images/assessments/exercise-24px.svg';
import SleepIcon from '../images/assessments/sleep-24px.svg';
import NutritionIcon from '../images/assessments/plant-24px.svg';
import PainIcon from '../images/assessments/pain-24px.svg';
import TobaccoIcon from '../images/assessments/tobacco.svg';
import FunctionIcon from '../images/assessments/functions-of-daily-living-24px.svg';
import AnxietyIcon from '../images/assessments/anxiety-24px.svg';
import PillCountIcon from '../images/assessments/pill_count-24px.svg';
import HeartIcon from '../images/assessments/Heart-24px.svg';
import AppIcon from '../images/assessments/apple-24px.svg';
import GlassIcon from '../images/assessments/glass-24px.svg';
import CoffeeIcon from '../images/assessments/coffee-24px.svg';
import RelaxIcon from '../images/assessments/relax-24px.svg';
import RelationshipsIcon from '../images/assessments/relationships-24px.svg';

import AppleFull from '../images/assessments/apple-fill-27x32.svg';
import AppleEmpty from '../images/assessments/apple-pick-27x32.svg';
import DrinkFull from '../images/assessments/Glass-full-24x32.svg';
import DrinkEmpty from '../images/assessments/Glass-pick-24x32.svg';

import CoffeeFull from '../images/assessments/coffee-fill-24px.svg';
import CoffeeEmpty from '../images/assessments/coffee-24px.svg';

import SugarIcon from '../images/assessments/sugar-24px.svg';

import type {QuestionInterface} from './questions';
import * as Choices from './choicesets';


export type DisplayCallbackReturnValue = {value:string, isTrue: boolean, count: number, amount: number};
export type DisplayCallback = (anwsers: any, questions: Array<QuestionInterface>) => DisplayCallbackReturnValue;
export type CalcScoreCallback = (anwsers: any, questions: Array<QuestionInterface>) => number;

export type AssessmentInterface = {
  id: number;
  title: string;
  subTitle: string;
  score: string;
  answers: any;
  configId: number;
  reportId: number;
};

export type AssessmentConfigInterface = {
  id: number;
  title: string;
  subTitle: string;
  maxScore: number;
  middleScore: number;
  minScore: number;
  scoringMode: number; //1: High score is good, 0: high score is bad
  scoring: scoring.ScoringInterface[];
  questions: Array<QuestionInterface>;
  groups: GroupBuilder[];
  calcQuestions: (values: {[propName: string]: string}) => questions.QuestionInterface[];
  calcScore: CalcScoreCallback;
  validate(data:{[propName: string]: string}): {data: {[string]: string[]}, isValid: boolean},
  icon: string,
  getDisplay: DisplayCallback
};

export type AssessmentWrapperInterface = {
  assessment: AssessmentInterface,
  config: AssessmentConfigInterface
};

const {makeQuestion} = questions;
export function makeDisplayReturnValue(value: string,isTrue: boolean,count:number = 0,amount:number = 0):DisplayCallbackReturnValue {
  if(!amount && count){
    amount = count;
  } else if(!count && amount) {
    if(amount === parseInt(amount,10)){
      count = amount;
    }
  }
  return {
    value,
    isTrue,
    count,
    amount
  }
}

export function calcQuestionMaxScore(question: QuestionInterface){
    return Math.max.apply(null,question.choices.map(ch => ch.score));
}

export function calcQuestionsTallyMaxScore(questions: QuestionInterface[]){
    return  questions.reduce((acc, q) => {
      acc += calcQuestionMaxScore(q)
      return acc;
    },0)
}

export function calcQuestionsAverageMaxScore(questions: QuestionInterface[]){
    if(questions.length === 0)
        return 0;

    return  questions.reduce((acc, q) => {
      acc += calcQuestionMaxScore(q)
      return acc;
    },0) / questions.length;
}

export function getDefaultDisplay(anwsers: any, questions:Array<QuestionInterface>): DisplayCallbackReturnValue{

  let appendScoreValue = '/' + calcQuestionsAverageMaxScore(questions);

  if(questions.length > 1){
    appendScoreValue += ' avg';
  }
  return makeDisplayReturnValue(this.calcScore(anwsers,questions) + appendScoreValue,true)
}


export function getAmountIfYesDisplay(targetIndex: number, suffix = ""){
  function displayFunc(answers:Object,questions:Array<QuestionInterface>):DisplayCallbackReturnValue{

    const ynMountDisp = getGroupYesNoAmountDisplay(0,suffix," ")(answers,questions);
    return ynMountDisp.isTrue ? makeDisplayReturnValue(ynMountDisp.count+"" + suffix,true) : makeDisplayReturnValue('',false);
  }

  return displayFunc;
}




export function getGroupYesNoDisplay(targetIndex: number){
  function displayFunc(answers:Object,questions:Array<QuestionInterface>):DisplayCallbackReturnValue{

    const qYesNoId:string|null = getQuestionIdByIndex(questions,targetIndex);
    let returnVal = null;
    if(valuePresent(answers,qYesNoId)){
        if(valuePresentAndEqualTo(answers,qYesNoId,"1")){
          returnVal = makeDisplayReturnValue("Yes",true);
        } else {
          returnVal = makeDisplayReturnValue("No",false);
        }
    } else {
      returnVal = makeDisplayReturnValue("Not answered",false);
    }
    return returnVal;
  }
  return displayFunc
}

export function getGroupMultiSelectDisplay(targetIndex: number){
  function displayFunc(answers:Object,questions:Array<QuestionInterface>):DisplayCallbackReturnValue{

    const qPainTypesId:string|null = getQuestionIdByIndex(questions,targetIndex);

    let returnVal = makeDisplayReturnValue('',false);
    if(valuePresent(answers,qPainTypesId)){
        const selected = Array.isArray(getAnswer(qPainTypesId,answers)) ? getAnswer(qPainTypesId,answers) : [];
        if(selected.length){
          const selectedTitles = questions[targetIndex].choices
                  .filter(ch => selected.indexOf(ch.value) > -1)
                  .map(ch => ch.title);

          returnVal = makeDisplayReturnValue(selectedTitles.join(', '), selectedTitles.length > 0, selectedTitles.length);

        }

    }

    return returnVal;
  }
  return displayFunc
}

export function getGroupSelectedDisplay(targetIndex: number){
  function displayFunc(answers:Object,questions:Array<QuestionInterface>):DisplayCallbackReturnValue{

    const qSelectedId:string|null = getQuestionIdByIndex(questions,targetIndex);

    let returnVal = makeDisplayReturnValue('',false);
    if(valuePresent(answers,qSelectedId)){
      const selectedAnswer = getAnswer(qSelectedId,answers,"")
      const selectedTitles = questions[targetIndex].choices
                .filter(ch => ch.value === selectedAnswer)
                .map(ch => ch.title);

        returnVal = makeDisplayReturnValue(selectedTitles.join(', '), selectedTitles.length > 0);

    }

    return returnVal;
  }
  return displayFunc
}

export function getGroupAmountDisplay(startIndex: number, suffix:string = ""){
  
  function displayFunc(answers: Object,questions:Array<QuestionInterface>){

    const qAmounQuestion = getQuestionByIndex(questions,startIndex);
    let returnVal;
    if(qAmounQuestion && valuePresent(answers,qAmounQuestion.id)){
      let amount = getAnswerOptionTitle(qAmounQuestion,getAnswer(qAmounQuestion.id,answers));
      returnVal = makeDisplayReturnValue(amount + suffix,true,0,parseFloat(amount))
    } else {
      returnVal = makeDisplayReturnValue("Not Specified",false);
    }
    return returnVal;
  }

  return displayFunc;
}

export function getGroupYesNoAmountDisplay(questionStartIndex:number, suffix:string = "",concatString:string = " - "){

  function displayFunc(answers: Object,questions:Array<QuestionInterface>){
    let returnVal = "";
    const yesNoDisplay = getGroupYesNoDisplay(questionStartIndex)(answers,questions);
  
    if(!yesNoDisplay.isTrue){ //they answered no
      returnVal = makeDisplayReturnValue("No",false);
    } else {
      const amountDisplay = getGroupAmountDisplay(questionStartIndex + 1)(answers,questions);

      if(amountDisplay.isTrue){
        returnVal = makeDisplayReturnValue("Yes" + concatString + amountDisplay.value + suffix,true,amountDisplay.count,amountDisplay.amount);
      } else {
        returnVal = makeDisplayReturnValue("Yes",true)
      }
    }

    return returnVal;
  }

  return displayFunc;
}

export function getAnswer(qid,answers:Object, defaultValue=""){
    if(typeof answers[qid] === 'undefined'){
      return defaultValue
    }
    return answers[qid]
}

export function getAnswerOptionTitle(question: QuestionInterface,answer,userDefaultTitle:string = ""){
    const defaultTitle = !userDefaultTitle.length ? answer : userDefaultTitle;
    const matchedChoices: any[] = question.choices.filter(ch => ch.value === answer);
    return matchedChoices.length > 0 ? matchedChoices[0].title : defaultTitle;
}

export function valuePresentAndEqualTo(values:Object,key,value){
  return values.hasOwnProperty(key) && values[key] === value;
}

export function getQuestionByIndex(questions: QuestionInterface[],index){
    if(typeof questions[index] === 'undefined'){
      return null
    }
    return questions[index];
}

export function getQuestionIdByIndex(questions: QuestionInterface[],index){
    if(typeof questions[index] === 'undefined'){
      return null
    }
    return questions[index].id.toString();
}

export function valuePresent(values,key){
  return values.hasOwnProperty(key)
}

const defaultCalcQuestion = function(values: {[propName: string]: string}){
      return this.questions;
};

const nutritionCalcQuestion = function(values: {[propName: string]: string}){
      const questions = this.questions.map(q => q);
      let qidRemove = [];
      
      const q2Id = getQuestionIdByIndex(questions,1);
      const q3Id = getQuestionIdByIndex(questions,2);

      const q4Id = getQuestionIdByIndex(questions,3);
      const q5Id = getQuestionIdByIndex(questions,4);

      const q6Id = getQuestionIdByIndex(questions,5);
      const q7Id = getQuestionIdByIndex(questions,6);

      if(!valuePresent(values,q2Id) || valuePresentAndEqualTo(values,q2Id,"0")){
          qidRemove.push(parseInt(q3Id,10))
      }

      if(!valuePresent(values,q4Id) || valuePresentAndEqualTo(values,q4Id,"0")){
          qidRemove.push(parseInt(q5Id,10))
      }

      if(!valuePresent(values,q6Id) || valuePresentAndEqualTo(values,q6Id,"0")){
          qidRemove.push(parseInt(q7Id,10))
      }

      return questions.filter(question => qidRemove.indexOf(question.id) === -1)
};

const pillsCalcQuestion = function(values: {[propName: string]: string}){
      const questions = this.questions.map(q => q);
      let qidRemove = [];
      
      const q1YesNoId = getQuestionIdByIndex(questions,0);
      const q3YesNoId = getQuestionIdByIndex(questions,2);

      const q2SliderId = getQuestionIdByIndex(questions,1);
      const q4SliderId = getQuestionIdByIndex(questions,3);

      if(!valuePresent(values,q1YesNoId) || valuePresentAndEqualTo(values,q1YesNoId,"0")){
          qidRemove.push(parseInt(q2SliderId,10))
      }

      if(!valuePresent(values,q3YesNoId) || valuePresentAndEqualTo(values,q3YesNoId,"0")){
          qidRemove.push(parseInt(q4SliderId,10))
      }

      return questions.filter(question => qidRemove.indexOf(question.id) === -1)
};

const exerciseCalcQuestion = function(values: {[propName: string]: string}){
      const questions = this.questions.map(q => q);
      let qidRemove = [];
      
      const q1YesNoId = getQuestionIdByIndex(questions,0);
      const q3YesNoId = getQuestionIdByIndex(questions,2);

      const q2SliderId = getQuestionIdByIndex(questions,1);
      const q4SliderId = getQuestionIdByIndex(questions,3);

      if(!valuePresent(values,q1YesNoId) || valuePresentAndEqualTo(values,q1YesNoId,"0")){
          qidRemove.push(parseInt(q2SliderId,10))
      }

      if(!valuePresent(values,q3YesNoId) || valuePresentAndEqualTo(values,q3YesNoId,"0")){
          qidRemove.push(parseInt(q4SliderId,10))
      }

      return questions.filter(question => qidRemove.indexOf(question.id) === -1)
};

const defaultCalcScore = function(values: any, defaultQuestions: any[] = []){

    function tallyScore (answers, questions: any[]) {
      var total = 0;
      questions.map(function (q,idx) {
          let question = questions[idx];
          if(answers && getAnswer(question.id,answers).length){
            let choiceValue = getAnswer(question.id,answers);
            let choices = questions[idx].choices;

            if(choices){
              choices.map((choice) => {
                if(choice.value === choiceValue){
                  total += parseInt(choice.score,10);
                }
                return choice;
              });
            }
          }
          return q;
      });

      return total;
    }
    let questions = defaultQuestions.length > 0 ? defaultQuestions : this.questions;
    return questions.length > 0 ? tallyScore(values,questions) / questions.length : tallyScore(values,questions);
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
function validateAnswer(answer,validations){
      const errors = [];
      const isEmpty = answer.length === 0;
      const is_numeric = !isEmpty ? isNumeric(answer) : false;
      validations.forEach((validate:string) => {
          switch(validate){
            case 'required':
              isEmpty && errors.push("Required.")
              break;
            case 'numeric':
            //don't mark numeric errors for empty answers
              (!isEmpty && !is_numeric) && errors.push("Must be number.")
              break;
            default:
              break;
          }
          return 
      });
      return errors;
}

function defaultValidation(data:{[propName: string]: string}): { data:{[string]: string[]}, isValid: boolean} {
    let hasErrors = false;

    const questionsMapped = this.calcQuestions(data).reduce((acc,q) => {
      acc[q.id] = q;
      return acc;
    },{})


    const filteredData = this.calcQuestions(data)
        .filter((question) => {
          return question.type !== 'label'
        })
        .reduce((acc,question) => {
            acc[question.id] = typeof data[question.id] !== 'undefined' ? data[question.id] : '';
            return acc;
        },{});

    const reduceCb = (errors, qid) => {
      const validations = questionsMapped[qid].validations;
      const answer = filteredData[qid];
      const questionErrors = validateAnswer(answer,validations)

      if(questionErrors.length){
        hasErrors = true;
        errors[qid] = questionErrors;
      } else {
        errors[qid] = []
      }
      
      return errors;
    };

    const errors = Object.keys(filteredData)
      .map((propName) => propName)
      .reduce(reduceCb,{});


    return {
      data: errors,
      isValid: !hasErrors
    }
}
export const makeAssessment = (id: number,config: AssessmentConfigInterface, score: string): AssessmentInterface  => {
  return {
    id,
    configId: config.id, 
    reportId: 0,
    title: config.title,
    subTitle: config.subTitle,
    score,
    answers: {},
    isComplete: false
  }
};

export const makeAssessmentWrapper = (assessment: AssessmentInterface, config: AssessmentConfigInterface):AssessmentWrapperInterface => {
  return {
    assessment,
    config
  }
}

export const makeAssessmentConfig = (id: number,title: string, subTitle: string, minScore: number, middleScore: number, maxScore: number, 
  scoring: scoring.ScoringInterface[], scoringMode: number,icon: string = '',calcQuestions: (any) => any = defaultCalcQuestion, calcScore: (any) => any = defaultCalcScore,validate: (any) => any = defaultValidation):AssessmentConfigInterface => {
  return {
    id,
    title,
    subTitle,
    minScore,
    middleScore,
    maxScore,
    scoring,
    scoringMode,//1: High score is good, 0: high score is bad
    questions: [],
    groups: [],
    calcQuestions,
    calcScore: function(answers: any, questions: QuestionInterface[]){
      const boundedFunction = defaultCalcScore.bind(this)
      return boundedFunction(answers,questions)
    },
    validate,
    icon,
    getDisplay: function(answers: any, questions: QuestionInterface[]){
      const boundedFunction = getDefaultDisplay.bind(this)
      return boundedFunction(answers,questions)
    }
  }
}


class AssessmentBuilder{
  groups: GroupBuilder[] = []; //TODO remove if not going to be used
  config: AssessmentConfigInterface;
  constructor(config: AssessmentConfigInterface){
    this.config = config;
  }

  addGroup(id:number,title:string,images: string[] = []){
    const group = new GroupBuilder(this.config,id,title,images)

    this.config.groups.push(group);

    return group;
  }

  getConfig(){
    return this.config;
  }

  setDisplay(display: DisplayCallback){
    this.config.getDisplay = display;
  }

  setQuestionCalc(calc_questions: (values: {[propName: string]: string}) => QuestionInterface[]){
    this.config.calcQuestions = calc_questions;
  }
}

export const SleepAssessmentBuilder =  new AssessmentBuilder(makeAssessmentConfig(7,'Sleep','How is your sleep?', 17, 39, 85,scoring.DefaultScoringList, 0, SleepIcon));
export const PainMoodAssessmentBuilder =  new AssessmentBuilder(makeAssessmentConfig(9,'Pain and Mood','', 17, 39, 85,scoring.DefaultScoringList, 0,PainIcon));
export const NutritionAssessmentBuilder =  new AssessmentBuilder(makeAssessmentConfig(10,'Nutrition','', 17, 39, 85,scoring.DefaultScoringList, 0,NutritionIcon,nutritionCalcQuestion));
export const FunctionalityAssessmentBuilder =  new AssessmentBuilder(makeAssessmentConfig(11,'Functionality','', 17, 39, 85,scoring.DefaultScoringList, 0,FunctionIcon));
export const AnxietyAssessmentBuilder =  new AssessmentBuilder(makeAssessmentConfig(12,'Anxiety','How anxious do you feel?', 17, 39, 85,scoring.DefaultScoringList, 0, AnxietyIcon));
export const PillCountAssessmentBuilder =  new AssessmentBuilder(makeAssessmentConfig(13,'Pill Count','', 17, 39, 85,scoring.DefaultScoringList, 0, PillCountIcon,pillsCalcQuestion));
export const ExerciseAssessmentBuilder =  new AssessmentBuilder(makeAssessmentConfig(14,'Exercise','', 17, 39, 85,scoring.DefaultScoringList, 0, ExerciseIcon, exerciseCalcQuestion));


const SleepGroup = SleepAssessmentBuilder
                    .addGroup(1,'Sleep',[SleepIcon])
                      .addQuestion(makeQuestion(1,'','slider',Choices.choicesSet24))
                      .addQuestion(makeQuestion(2,'','slider',Choices.choicesSet42))
                      .addQuestion(makeQuestion(4,'','slider',Choices.choicesSet27))
                      .addQuestion(makeQuestion(3,'How is your sleep impacting your daily living?','slider',Choices.choicesSet43))
                      ;
/**
 * 
 */
PainMoodAssessmentBuilder
  .addGroup(2,'Type of Pain',[PainIcon])
    .setDisplay(getGroupMultiSelectDisplay(0))
    .addQuestion(makeQuestion(5,'Type of Pain','grid_select',Choices.choicesSet28,true));

const MoodGroup = PainMoodAssessmentBuilder
                    .addGroup(14,'Mood',[HeartIcon])
                      .setDisplay(getDefaultDisplay)
                      .addQuestion(makeQuestion(6,'','slider',Choices.choicesSet25))
                      .addQuestion(makeQuestion(7,'','slider',Choices.choicesSet26))
                      .addQuestion(makeQuestion(18,'','slider',Choices.choicesSet33))
                      .addQuestion(makeQuestion(19,'','slider',Choices.choicesSet34))
                      .addQuestion(makeQuestion(20,'','slider',Choices.choicesSet35));
/**
 * 
 */
NutritionAssessmentBuilder
  .addGroup(3,'Plant-based Food',[NutritionIcon])
    .setDisplay(getGroupYesNoDisplay(0))
    .addQuestion(makeQuestion(8,'Have you eaten unprocessed plant based foods this week?','grid_select',Choices.choicesSet30));
NutritionAssessmentBuilder
  .addGroup(4,'Fruits & Vegatables',[AppIcon])
    .setDisplay(getGroupYesNoAmountDisplay(0," Servings"))
    .addQuestion(makeQuestion(9,'Have you eaten a variety of fuit and vegatables this week?','grid_select',Choices.choicesSet30))
    .addQuestion(makeQuestion(10,'How many servings per day?','icon_slider',Choices.choicesSet29,false,[AppleFull, AppleEmpty]));
NutritionAssessmentBuilder
  .addGroup(5,'Water',[GlassIcon])
    .setDisplay(getGroupYesNoAmountDisplay(0))
    .addQuestion(makeQuestion(11,'Have you drank plenty of water?','grid_select',Choices.choicesSet30))
    .addQuestion(makeQuestion(12,'How much water per day?','icon_slider',Choices.choicesSet31,false,[DrinkFull, DrinkEmpty]));
NutritionAssessmentBuilder
  .addGroup(6,'Limit Caffeine',[CoffeeIcon])
    .setDisplay(getGroupYesNoAmountDisplay(0))
    .addQuestion(makeQuestion(13,'Did you consume caffeinated beverages this week?','grid_select',Choices.choicesSet30))
    .addQuestion(makeQuestion(33,'On average how many caffeinated beverages did you drink per day?','icon_slider',Choices.choicesSet29,false,[CoffeeFull, CoffeeEmpty]));
NutritionAssessmentBuilder
  .addGroup(6,'Sugar',[SugarIcon])
    .setDisplay(getGroupSelectedDisplay(0))
    .addQuestion(
        makeQuestion(34,'On average how much sugar did you consume per day?','grid_select',Choices.choicesSet46)
        .setExample(
          "Sugar Examples",
          [
            "Can of soda is about 7 tsp.",
            "Chocolate bar is about 6 tsp.",
            "An apple is 2 tsp.",
            `Average healthy daily amounts for
              men is 9 tsp. per day and for
              women is 6 tsp per day.`
          ]
        )
     );
NutritionAssessmentBuilder  
  .addGroup(6,'Tobacco',[TobaccoIcon])
    .setDisplay(getGroupSelectedDisplay(0))
    .addQuestion(makeQuestion(35,'Did you use tobacco this week?','grid_select',Choices.choicesSetYesNo));
/**
 * 
 */
const FunctionsGroup = FunctionalityAssessmentBuilder
                        .addGroup(15,'Functions of Daily Living',[FunctionIcon])
                          .addQuestion(makeQuestion(21,'This week were you able to perform functions of daily living (including work, time with family or friends, complete errands)','slider',Choices.choicesSet36));
FunctionalityAssessmentBuilder
  .addGroup(7,'Physical Activity',[ExerciseIcon])
    .addQuestion(makeQuestion(22,'What is  your level of physical activity?','slider',Choices.choicesSet36))
FunctionalityAssessmentBuilder
  .addGroup(8,'Relationships',[RelationshipsIcon])
    .addQuestion(makeQuestion(23,'How are your relationships with friends and family?','slider',Choices.choicesSet36));
/**
 * 
 */
AnxietyAssessmentBuilder
  .addGroup(9,'',[AnxietyIcon])
    .addQuestion(makeQuestion(24,'','slider',Choices.choicesSet37))
    .addQuestion(makeQuestion(25,'','slider',Choices.choicesSet38))
    .addQuestion(makeQuestion(26,'','slider',Choices.choicesSet39))
    .addQuestion(makeQuestion(27,'','slider',Choices.choicesSet40))
    .addQuestion(makeQuestion(28,'','slider',Choices.choicesSet41));

/**
 * 
 */
PillCountAssessmentBuilder
  .addGroup(10,'',[PillCountIcon]) //getMed
    .setDisplay(getAmountIfYesDisplay(0," OTC"))
    .addQuestion(makeQuestion(29,'Have you taken any over the counter pain medications this week?','grid_select',Choices.choicesSet30))
    .addQuestion(makeQuestion(30,'How many per day?','slider',Choices.choicesSet44));
PillCountAssessmentBuilder
  .addGroup(11,'',[PillCountIcon])
    .setDisplay(getAmountIfYesDisplay(0," Prescription"))
    .addQuestion(makeQuestion(31,'Have you taken prescription pain pills this week?','grid_select',Choices.choicesSet30))
    .addQuestion(makeQuestion(32,'How many per day?','slider',Choices.choicesSet44));

ExerciseAssessmentBuilder
  .addGroup(12,'Exercise',[ExerciseIcon])
    .setDisplay(getGroupYesNoAmountDisplay(0," Minutes"))
    .addQuestion(makeQuestion(14,'Have you exercised week?','grid_select',Choices.choicesSet30))
    .addQuestion(makeQuestion(15,'How many minutes?','text',[],false,[],['required','numeric']));
ExerciseAssessmentBuilder
  .addGroup(13,'Relaxation',[RelaxIcon])
    .setDisplay(getGroupYesNoAmountDisplay(0," Minutes"))
    .addQuestion(makeQuestion(16,'Did you practice any relaxation techniques this week?','grid_select',Choices.choicesSet30))
    .addQuestion(makeQuestion(17,'How many minutes?','text',[],false,[],['required','numeric']));

const assessments = [];

assessments.push(FunctionalityAssessmentBuilder.getConfig());
assessments.push(PainMoodAssessmentBuilder.getConfig());
assessments.push(AnxietyAssessmentBuilder.getConfig());
assessments.push(SleepAssessmentBuilder.getConfig());
assessments.push(PillCountAssessmentBuilder.getConfig());
assessments.push(NutritionAssessmentBuilder.getConfig());
assessments.push(ExerciseAssessmentBuilder.getConfig());

export const AssessmentConfig = assessments.reduce((acc,config) => {
  acc[config.id] = config;
  return acc;
},{});

export const assessmentConfigIds: number[] = assessments.map(a => a.id)

export const graphAssessmentGroups = [];

graphAssessmentGroups.push(SleepGroup)
graphAssessmentGroups.push(MoodGroup)
graphAssessmentGroups.push(FunctionsGroup)

export const graphAssessmentGroupsConfig = graphAssessmentGroups.reduce((acc,group) => {
  acc[group.getId()] = group;
  return acc;
},{})


