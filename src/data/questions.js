// @flow
import * as Choices from './choicesets';

const defaultUserDisplay = (answers: any, type: string = 'normal') => {

  return ''
}

export type QuestionInterface = {
  id: number;
  title: string;
  multiChoice: boolean,
  type: string;
  choices: Choices.ChoicesInterface[];
  images: string[];
  display: (answers:any, type?: string) => string; 
  validations: string[],
  example: {title: string, info:string[]},
  setExample: (title: string,info: string[]) => {title: string, info:string[]};
};

export type GroupInterface = {
  id: number;
  title: string;
  images: string[];
};

export class Question {
  id: number;
  title: string;
  multiChoice: boolean;
  type: string;
  choices: Choices.ChoicesInterface[];
  images: string[];
  display: (answers:any, type?: string) => string; 
  validations: string[]
  example: {title: string, info:string[]}
  setExample: (title: string,info: string[]) => {title: string, info:string[]};

  constructor(id: number, title: string,type: string = 'text', choices:any[] = [],multiChoice: boolean = false, images: string[] = [] ,validations: string[] = ['required']){
    this.id = id;
    this.title = title;
    this.type = type;
    this.choices = choices;
    this.multiChoice = multiChoice;
    this.images = images;
    this.display = defaultUserDisplay;
    this.validations = validations;
    this.setExample = this.setExample.bind(this);
  }

  setExample(title: string,info: string[]){
    this.example = {title, info}
    return this;
  }
}

export const makeQuestion = (id: number, title: string,type: string = 'text', choices:any[] = [],multiChoice: boolean = false, images: string[] = [] ,validations: string[] = ['required']): QuestionInterface => {
  // return {
  //   id,
  //   title,
  //   type,
  //   choices,
  //   multiChoice,
  //   images,
  //   display: defaultUserDisplay,
  //   validations
  // }
  return new Question(id,
    title,
    type,
    choices,
    multiChoice,
    images,
    validations);
};




