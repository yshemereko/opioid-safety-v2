import {painLevelIds} from './painLevels';
import {bodySectionList} from './body';
import {AssessmentConfig} from './assessments';

export function getRandomInt(maxInclusive) {
  return Math.floor(Math.random() * Math.floor(maxInclusive+1));
}


export function blend(props: any[], values: any[]) {
  const blendAccumilator = (acc,itemId) => {
    acc[itemId] = values[getRandomInt(values.length - 1)];
    return acc;
  }
  return props.reduce(blendAccumilator,{})
}


export function createRandomPainMarkings(max: number) {
  let validSections = bodySectionList.filter(bs => !bs.isBlank).map(bs => bs.id);
  let pls = painLevelIds.map(pl => pl);
  let bsids = [];
  for(var i = 0; i < max; i++){
    let idx = getRandomInt(validSections.length - 1);
    let value = validSections[idx];
    bsids.push(value);
    validSections.slice(idx,1);
  }

  pls.pop();
  return blend(bsids,pls)
}

export function getRandomAnswersForAssessment(assessment){
    const config = AssessmentConfig[assessment.configId];
    const answers = config.questions.map(q => {
      const randomAnswer = q.choices[getRandomInt(q.choices.length -1)];
      const value = q.multiChoice ? [randomAnswer.value] : randomAnswer.value;
      return {assessmentId: assessment.id, question: q, answer: value};
    })
    return answers;
}

