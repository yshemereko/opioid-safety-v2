
import { normalize, schema } from 'normalizr';

import Front00 from '../images/front/front00.svg';
import Front01 from '../images/front/front01.svg';
import Front02 from '../images/front/front02.svg';
import Front03 from '../images/front/front03.svg';
import Front04 from '../images/front/front04.svg';
import Front05 from '../images/front/front05.svg';
import Front06 from '../images/front/front06.svg';
import Front07 from '../images/front/front07.svg';
import Front08 from '../images/front/front08.svg';
import Front09 from '../images/front/front09.svg';
import Front10 from '../images/front/front10.svg';

import Back00 from '../images/back/back00.svg';
import Back01 from '../images/back/back01.svg';
import Back02 from '../images/back/back02.svg';
import Back03 from '../images/back/back03.svg';
import Back04 from '../images/back/back04.svg';
import Back05 from '../images/back/back05.svg';
import Back06 from '../images/back/back06.svg';
import Back07 from '../images/back/back07.svg';
import Back08 from '../images/back/back08.svg';
import Back09 from '../images/back/back09.svg';
import Back10 from '../images/back/back10.svg';

const painLevelSchema = new schema.Entity('painLevels');
const painLevelListSchema = new schema.Array(painLevelSchema);

export type PainLevelInterface = {
    id: number,
    images: {bodyFront: string, bodyBack: string},
    title: string,
    description: string,
    level: number,
    color: string
}

const resolvePainColor = (level) => {
    if(level === 0){
      return '#000000'
    }
    if(level === 1){
      return '#6c4294';
    }
    if(level === 2) {
      return '#514195';
    }
    if(level === 3) {
      return '#3756a2';
    }
    if(level === 4) {
      return '#4b99b7';
    }
    if(level === 5) {
      return '#55b597';
    }
    if(level === 6) {
      return '#5cb157';
    }
    if(level === 7) {
      return '#f3c646';
    }
    if(level === 8) {
      return '#ea983e';
    }
    if(level === 9) {
      return '#e06536';
    }
    return '#da3731';
}

export const makePainLevel = (id, level, title, bodyFront, bodyBack, description = '') => {
  return {
    id,
    images: {bodyFront, bodyBack},
    title,
    description,
    level,
    color: resolvePainColor(level)
  }
}

export const painLevelsRaw = [
  makePainLevel(1,0,'0', Front00, Back00, "No pain"),
  makePainLevel(2,1,'1', Front01, Back01, "Hardly notice pain"),
  makePainLevel(3,2,'2', Front02, Back02, "Notice pain, does not interfere with activities"),
  makePainLevel(4,3,'3', Front03, Back03, "Sometimes distracts me"),
  makePainLevel(5,4,'4', Front04, Back04, "Distracts me, can do usual activities"),
  makePainLevel(6,5,'5', Front05, Back05, "Interrupts some activities"),
  makePainLevel(7,6,'6', Front06, Back06, "Hard to ignore, avoid usual activities"),
  makePainLevel(8,7,'7', Front07, Back07, "Focus of attention, prevents doing daily activities"),
  makePainLevel(9,8,'8', Front08, Back08, "Awful, hard to do anything"),
  makePainLevel(10,9,'9', Front09, Back09, "Can't bear the pain, unable to do anything"),
  makePainLevel(11,10,'10', Front10, Back10, "As bad as it could be, nothing else matters")
];
const normalizedPainLevels = normalize(painLevelsRaw,painLevelListSchema);


export const painLevels = normalizedPainLevels.entities.painLevels;
export const painLevelIds = normalizedPainLevels.result.reverse(); //most lists will show descending pain order


