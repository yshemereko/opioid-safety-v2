// @flow
import { normalize, schema } from 'normalizr';
const bodySectionSchema = new schema.Entity('bodySections');
const bodySectionListSchema = new schema.Array(bodySectionSchema);

export type BodySectionInterface = {
  id: number;
  row: number;
  col: number;
  title: string;
  description: string;
  image: string;
  isBlank: boolean;
  region: string;
}

const makeBodySection = (id:number, row:number, col:number, region:string, image:string, isBlank:boolean = false, title:string ='', description:string = ''): BodySectionInterface => {
  return {
    id,
    row,
    col,
    image,
    title,
    description,
    isBlank,
    region
  }
};

export type bodiesObjectInterface = {
  [propName: string]: BodySectionInterface
}

export const frontBodyMaskRanges:[number,number][] = [
  [1,21],
  [25,36],
  [40,51],
  [55,65],
  [71,79],
  [87,94],
  [102,109],
  [117,124],
  [132,138],
  [148,153],
  [163,168],
  [178,182],
  [185,185],
  [191,191],
  [194,197],
  [200,200],
  [206,206],
  [209,212],
  [215,215],
  [221,221],
  [224,230],
  [236,245],
  [251,260],
  [266,275],
  [281,290],
  [296,305],
  [311,320],
  [326,336],
  [340,351],
  [355,366],
  [370,390]
];

export const frontBodyMask = frontBodyMaskRanges.reduce((acc,range) => {
  for(let i = range[0]; i <= range[1]; i++){
    acc.push(i);
  }
  return acc;
},[]);

export const backBodyMask = frontBodyMask; //just being lazy for now //TODO

export const bodySectionList = [];

const bodyRows = 26;
const bodyCols = 15;
for(let i = 0; i < bodyRows; i++){
  for(let j = 0; j < bodyCols; j++){
    let frontId = (j+(i * 15))+1;
    let backId = bodyRows * bodyCols + frontId;
    let frontImage = '';
    let backImage = '';
    let row = Math.floor((frontId - 1)/15);
    let col = (frontId - 1) % 15;

    bodySectionList.push(
      makeBodySection(frontId, row, col, 'front', frontImage, frontBodyMask.indexOf(frontId) !== -1)
    );

    bodySectionList.push(
      makeBodySection(backId, row, col, 'back', backImage, backBodyMask.indexOf(frontId) !== -1)
    );
  }
}

export const frontBodySectionIds:any[] = bodySectionList
                                      .filter((sect) => sect.region === 'front')
                                      .map(sect => sect.id + '');
                                      
export const backBodySectionIds:any[] = bodySectionList
                                      .filter((sect) => sect.region === 'back')
                                      .map(sect => sect.id + '');

const normalizedBodySections = normalize(bodySectionList,bodySectionListSchema);


export const bodySections = normalizedBodySections.entities.bodySections;
export const bodySectionIds = normalizedBodySections.result;



