import { shallow,mount } from 'enzyme';
export const shallowWithStore = (component,store)=>{
  const context = {
    store,
  };
  return shallow(component, { context });
}
export function isFunction(functionToCheck) {
 return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}