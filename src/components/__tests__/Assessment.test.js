import React from 'react';
import Assessment from '../Assessment';
import { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';
import {getRandomInt,createRandomPainMarkings,getRandomAnswersForAssessment} from '../../data/utils';
import {
  makeAssessmentWrapper,
  makeAssessment,
  AssessmentConfig,
  assessmentConfigIds,
  NutritionAssessmentBuilder
} from '../../data/assessments'

describe('<Assessment />',() => {

  it('renders without crashing', () => {
    shallow(<Assessment />);
  });

  it("checks various Assessment functions",() => {

    const assessConf = AssessmentConfig[assessmentConfigIds[0]]
    const assessment = {...makeAssessment(1,(new Date()).getTime()),configId: assessConf.id};
    const assessmentAnswers = getRandomAnswersForAssessment(assessment);
    
    //make sure all answers are answered so the assessment validates
    assessment.answers = assessmentAnswers.reduce((acc, {question,answer}) => {
      acc[question.id] = answer;
      return acc;
    },{});

    const assessmentWrapper = makeAssessmentWrapper(assessment,assessConf);

    const props = {
      assessmentWrapped: assessmentWrapper,
      onComplete: jest.fn(),
      report: {id: 123, isComplete: false},
      answerQuestion: jest.fn()
    };

    const wrapper = mount(<Assessment {...props} />);
    const nextButton1 = wrapper.find('Button[onClick]');
    nextButton1.simulate('click')
    expect(props.onComplete.mock.calls.length).toBe(1);

    //to increase branch coverage well load an assessment with questions of type grid_select and icon_slider
    const assessConf2 = NutritionAssessmentBuilder.getConfig();
    //overrid calcQuestions with decides with question are shown. We want to show all.
    function returnAllFunctions(){
      return this.questions;
    }
    assessConf2.calcQuestions = returnAllFunctions.bind(assessConf2);
    assessConf2.questions[0].type = 'text'; //for branch coverage with need a text type
    
    const assessment2 = {...makeAssessment(2,(new Date()).getTime()),configId: assessConf.id};
    const assessmentWrapper2 = makeAssessmentWrapper(assessment2,assessConf2);
    const assessmentAnswers2 = getRandomAnswersForAssessment(assessment2);
    
    wrapper.setProps({assessmentWrapped: assessmentWrapper2});

    //answer a questions
    const textField = wrapper.find('InputField');
    textField.props().onChange(assessConf2.questions[0],"Kitty");
    
    //mark report as complete and make sure you can't edit the assessment
    wrapper.setProps({assessmentWrapped: assessmentWrapper2,report: {id: 123, isComplete: true}});

    const textFieldUpdated = wrapper.find('InputField');
    textFieldUpdated.props().onChange(assessConf2.questions[0],"doggy");


    //now lets render with some answers for branch coverage
    const assessment3 = {...makeAssessment(2,(new Date()).getTime()),configId: assessConf2.id};
    const assessmentAnswers3 = getRandomAnswersForAssessment(assessment3);
    assessment3.answers = assessmentAnswers3.reduce((acc, {question,answer}) => {
      acc[question.id] = answer;
      return acc;
    },{});
 
    const assessmentWrapper3 = makeAssessmentWrapper(assessment3,assessConf2);
    wrapper.setProps({assessmentWrapped: assessmentWrapper3,report: {id: 123, isComplete: false}});
  })
})