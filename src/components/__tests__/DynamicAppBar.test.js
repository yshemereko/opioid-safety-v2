import React from 'react';
import DynamicAppBar from '../DynamicAppBar';
import { MemoryRouter } from 'react-router';
import { shallow, mount } from 'enzyme';
import Paper from '@material-ui/core/Paper';
import renderer from 'react-test-renderer';

describe('<DynamicAppBar />',() => {
  it('renders Aboutwithout crashing', () => {
    const wrapper = mount(
      <MemoryRouter>
        <DynamicAppBar />
      </MemoryRouter>
    );
  });


  it('matches the old snapshot showing nothing changed', () => {
    const tree = renderer.create(<MemoryRouter><DynamicAppBar /></MemoryRouter>).toJSON();
    expect(tree).toMatchSnapshot();
  });

});