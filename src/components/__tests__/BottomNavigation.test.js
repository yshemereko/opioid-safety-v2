import React from 'react';
import BottomNavigation from '../BottomNavigation';
import { mount, shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';


describe('<BottomNavigation />',() => {

  let routerParams = {
      params: {},
      isExact: true, 
      path: "/", 
      url: ""
    };

  it('renders without crashing', () => {
    shallow(<MemoryRouter ><BottomNavigation match={routerParams}/></MemoryRouter>);
  });

  it('changes to state when nav clicked on', () => {
    const wrapper = mount(<MemoryRouter ><BottomNavigation match={routerParams}/></MemoryRouter>);

    const element = wrapper.find(BottomNavigationAction).at(2);
    element.simulate('click');

    expect(element.props().label).toBe('Info');

  });

  it('matches the old snapshot showing nothing changed', () => {
    const tree = renderer.create(<MemoryRouter><BottomNavigation match={routerParams}/></MemoryRouter>).toJSON();
    expect(tree).toMatchSnapshot(); 
  });
})