import React from 'react';
import PainMap from '../PainMap';
import { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';

import {makeReport} from '../../data/report';
import {makeAssessment, assessmentConfigIds, AssessmentConfig} from '../../data/assessments';
import {getReportAssessmentsWrapped} from '../../containers/selectors';
import {painLevels,painLevelIds} from '../../data/painLevels';
import {bodySectionIds, bodySections as bodySectionsOriginal} from '../../data/body';

describe('<PainMap />',() => {
  it('renders without crashing', () => {

    shallow(<PainMap />);
    
  });


  it('test that function props and instance methods are working and tests state changes', () => {
    // we need a report
    const reportId = 1111;
    const report = makeReport(reportId, (new Date()).getTime());

    const bodySections = bodySectionIds.reduce((acc,bsid) => {
      const bsOg = bodySectionsOriginal[bsid];
      acc[bsid] = {...bsOg,isBlank: bsOg.id % 2 === 0};
      return acc
    },{})
  
    //need some pain markings
    const painLevel1 = painLevels[painLevelIds[5]];
    const painLevel2 = painLevels[painLevelIds[6]];
    const painLevel3 = painLevels[painLevelIds[7]];
    const painLevel4 = painLevels[painLevelIds[7]];

    const bodySection1 = bodySections[bodySectionIds[0]];
    const bodySection2 = bodySections[bodySectionIds[1]];
    const bodySection3 = bodySections[bodySectionIds[2]];
    const bodySection4 = bodySections[bodySectionIds[3]];
  
    bodySection3.region = 'back';
    bodySection3.isBlank = false;
    const painMarkings = [
      {section: bodySection1, painLevel: painLevel1},
      {section: bodySection2, painLevel: painLevel2},
      {section: bodySection3, painLevel: painLevel3},
      {section: bodySection4, painLevel: painLevel4}
    ];


    //here are the initial props
    const props = {
      bodySections: bodySectionIds.map(bsid => bodySections[bsid]),
      bodyImage:  'fake/image/url.png',
      report:  report,
      gridSize:  15,
      markPain: jest.fn(),
      markPainAllOver: jest.fn(),
      painMarkings: painMarkings,
      deletePain: jest.fn(),
      freeze: false,
      painAllOverDialogOpen: false,
      currentPainAllOver: painLevels[painLevelIds[2]],
      handlePainAllOverClose: jest.fn(),
      debug: false,
      appInfo: {maxPainPoints: 3},
      side: 'front'
    };

    const wrapper = shallow(<PainMap {...props} />);

    //smoke test on debug prop
    wrapper.setProps({debug: true});
    wrapper.dive();
    wrapper.setProps({debug: false});
    

    //lets call some function properties on PainLevelAllOverDialog;
    let painAllOverDiaWrapper = wrapper.dive().find('PainLevelAllOverDialog');
    painAllOverDiaWrapper.props().selectPain(painLevelIds[4]);
    expect(props.markPainAllOver.mock.calls.length).toBe(1);
    //check first arg is correct painlevel id
    expect(props.markPainAllOver.mock.calls[0][0]).toBe(painLevelIds[4]);
    //second arg should be the correct report
    expect(props.markPainAllOver.mock.calls[0][1].id).toBe(reportId);
    //smoke test PainLevelAllOverDialog handleClose prop
    painAllOverDiaWrapper.props().handleClose();




    //lets call some function properties on PainLevelDialog;
    let painLevelDialogWrapper = wrapper.dive().find('PainLevelDialog');

    const painLevelId1 = painLevelIds[5]
    painLevelDialogWrapper.props().selectPain(bodySection1,painLevelId1);
    expect(props.markPain.mock.calls.length).toBe(1);
    //deleteSection prop of PainLevelDialog
    painLevelDialogWrapper.props().deleteSection(bodySection1)
    expect(props.deletePain.mock.calls.length).toBe(1)
    expect(props.deletePain.mock.calls[0][0]).toBe(bodySection1)
    expect(props.deletePain.mock.calls[0][1]).toBe(report)
    //smoke test for handleClose
    painLevelDialogWrapper.props().handleClose();



    // Pain map method testing
    //smoke test for handleDialogOpen
    wrapper.dive().instance().handleDialogOpen();
    //smoke test for handleGetPainMarkings
    wrapper.dive().instance().handleGetPainMarkings();
    //smoke test for handleShowAllGridItems
    wrapper.dive().instance().handleShowAllGridItems();
    //pass invalid row and column numbers handleResolveBodySection 
    wrapper.dive().instance().handleResolveBodySection(-22,-55);
    //pass valid row and column numbers handleResolveBodySection
    wrapper.setProps({side: bodySection2.region}); //make sure props.sid == bodySection.region
    wrapper.dive().instance().handleResolveBodySection(bodySection2.row,bodySection2.col);
    //check to confirm a saved body section
    expect(wrapper.dive().instance().isSectionSaved(bodySection2)).toBe(true);



    //handle grid click handleGridClick
    //this is complicated because of use of ref property and click event
    //this will miss
    const gridClickEventMiss = {
      clientX: 0,
      clientY: 0,
      preventDefault: jest.fn(),
      stopPropagation: jest.fn()
    };
    let painMapInstance = wrapper.dive().instance();

    painMapInstance.mapBox = {
      getBoundingClientRect: jest.fn()
      .mockReturnValue({left: 0, top: 0})
    }

    painMapInstance.handleGridClick(gridClickEventMiss);
    
    //this will hit
    wrapper.setProps({side: bodySection3.region});
    const gridClickEventHit = {
      clientX: props.gridSize * bodySection3.col,
      clientY: props.gridSize * bodySection3.row,
      preventDefault: jest.fn(),
      stopPropagation: jest.fn()
    };

    painMapInstance.handleGridClick(gridClickEventHit);

    //now do same test with too many pain parkings
    const painMarkings2 = [
      {section: bodySection1, painLevel: painLevel1},
      {section: bodySection2, painLevel: painLevel2},
      // {section: bodySection3, painLevel: painLevel3},
      {section: bodySection4, painLevel: painLevel4}
    ];

    wrapper.setProps({painMarkings: painMarkings2})
    painMapInstance = wrapper.dive().instance();
    painMapInstance.mapBox = {
      getBoundingClientRect: jest.fn()
      .mockReturnValue({left: 0, top: 0})
    }
    painMapInstance.handleGridClick(gridClickEventHit);

    //now do same test with with painMarkings available
    const painMarkings3 = [
      {section: bodySection4, painLevel: painLevel4}
    ];
    
    wrapper.setProps({painMarkings: painMarkings3})
    painMapInstance = wrapper.dive().instance();
    painMapInstance.mapBox = {
      getBoundingClientRect: jest.fn()
      .mockReturnValue({left: 0, top: 0})
    }
    painMapInstance.handleGridClick(gridClickEventHit);


    //smoke test for when grid is frozen
    wrapper.setProps({freeze: true})
    painMapInstance = wrapper.dive().instance();
    painMapInstance.handleGridClick(gridClickEventHit);

  });
});