import React from 'react';
import Results from '../Results';
import ReportTileItem from '../ReportTileItem';
import { shallow, mount } from 'enzyme';
import Paper from '@material-ui/core/Paper';
import renderer from 'react-test-renderer';

describe('<Results />',() => {
  it('renders without crashing and test for existance of things', () => {
    //should run without crashing with not results
    shallow(<Results />);

    const reports = [{id: 1,created: 111111},{id: 2,created: 222222},{id: 3,created: 3333333}];

    const wrapper = mount(<Results reports={reports} />);

    expect(wrapper.find(ReportTileItem).length).toBe(3);

  });

  it('matches the old snapshot showing nothing changed', () => {
    const tree = renderer.create(<Results />).toJSON();
    expect(tree).toMatchSnapshot(); 
  });

});