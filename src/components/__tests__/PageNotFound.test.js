import React from 'react';
import PageNotFound from '../PageNotFound';
import { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';

describe('<PageNotFound />',() => {
  it('renders without crashing', () => {

    shallow(<PageNotFound />);
    
  });

  it('matches the old snapshot showing nothing changed', () => {
    const tree = renderer.create(<PageNotFound />).toJSON();
    expect(tree).toMatchSnapshot(); 
  });
  
});