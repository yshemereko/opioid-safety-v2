import React from 'react';
import Report from '../Report';
import { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';
import ReportInfoContainer from '../../containers/ReportStartContainer'
import PainMapCombined from '../../components/PainMapCombined';
import {makeReport} from '../../data/report';
import {makeAssessment, assessmentConfigIds, AssessmentConfig} from '../../data/assessments';
import {getReportAssessmentsWrapped} from '../../containers/selectors';
import {painLevels,painLevelIds} from '../../data/painLevels';
import {bodySectionIds, bodySections} from '../../data/body';

describe('<Report />',() => {
  it('renders without crashing', () => {

    shallow(<Report />);
    
  });

  it("test steps and other props functions and method functions",() => {
    const reportId = 1111;
    const report = makeReport(reportId, (new Date()).getTime());

    const props = {
      report: report,
      nextStep: jest.fn(),
      prevStep: jest.fn(),
      answerQuestion: jest.fn(),
      confirmReport: jest.fn(),
      reportInProgress: true,
      reportPainMarkings: [],
      assessmentsWrapped: [], 
      classes: {},
      history: {},
      markPainAllOver: jest.fn(),
      currentPainAllOver: {},
      bodySections: [],
      markPain: jest.fn(),
      deletePain: jest.fn(),
      screen: {width: 800, height: 800}
    }

    const outerWrapper = shallow(<Report {...props} />);
    const painMapWrapper = outerWrapper.dive().dive();
    const reportWrapper = outerWrapper.dive();

    //make sure there are 3 buttons: 
    //1. toggle body map
    //2. pain overal dialogue
    //3. next button
    const btnToggleWrapper = painMapWrapper.find('#btn_toggle_map');
    const btnDialogWrapper = painMapWrapper.find('#btn_pain_dialog');
    const btnNextWrapper = painMapWrapper.find('#btn_next_step');
    expect(btnToggleWrapper.length).toBe(1);
    expect(btnDialogWrapper.length).toBe(1);
    expect(btnNextWrapper.length).toBe(1);

    //click each button and make sure applicable function props were called correctly
    //toggle body

    expect(painMapWrapper.state().mapView).toBe('front');
    btnToggleWrapper.simulate('click');
    expect(painMapWrapper.state().mapView).toBe('back');

    //dialogue button
    expect(painMapWrapper.state().painAllOverDialogOpen).toBe(false);
    btnDialogWrapper.simulate('click');
    expect(painMapWrapper.state().painAllOverDialogOpen).toBe(true);
    //next button
    expect(props.nextStep.mock.calls.length).toBe(0);
    btnNextWrapper.simulate('click');
    expect(props.nextStep.mock.calls.length).toBe(1);

    //go through steps 0 through 8
    //Not sure why I had to brute force every step ... Figure out later
    let handleResolveWrapper = painMapWrapper;

    expect(reportWrapper.props().report.step).toBe(0);

    outerWrapper.setProps({report: {...report, step: 1}});
    outerWrapper.dive().dive().instance().handleResolveComponent();
    expect(outerWrapper.dive().props().report.step).toBe(1);


    outerWrapper.setProps({report: {...report, step: 2}});
    outerWrapper.dive().dive().instance().handleResolveComponent();
    expect(outerWrapper.dive().props().report.step).toBe(2);

    outerWrapper.setProps({report: {...report, step: 3}});
    outerWrapper.dive().dive().instance().handleResolveComponent();
    expect(outerWrapper.dive().props().report.step).toBe(3);

    outerWrapper.setProps({report: {...report, step: 4}});
    outerWrapper.dive().dive().instance().handleResolveComponent();
    expect(outerWrapper.dive().props().report.step).toBe(4);

    outerWrapper.setProps({report: {...report, step: 5}});
    outerWrapper.dive().dive().instance().handleResolveComponent();
    expect(outerWrapper.dive().props().report.step).toBe(5);

    outerWrapper.setProps({report: {...report, step: 6}});
    outerWrapper.dive().dive().instance().handleResolveComponent();
    expect(outerWrapper.dive().props().report.step).toBe(6);

    outerWrapper.setProps({report: {...report, step: 7}});
    outerWrapper.dive().dive().instance().handleResolveComponent();
    expect(outerWrapper.dive().props().report.step).toBe(7);

    outerWrapper.setProps({report: {...report, step: 8}});
    outerWrapper.dive().dive().instance().handleResolveComponent();
    expect(outerWrapper.dive().props().report.step).toBe(8);

    //Click next button on invalid assessment Component
    //since no assessments were injected into props above all
    //steps will yield the "invalid assessment message" which has a next and prev button
    outerWrapper.setProps({report: {...report, step: 1}});
    outerWrapper.dive().dive().instance().handleResolveComponent();
    expect(outerWrapper.dive().props().report.step).toBe(1);
    
    outerWrapper.dive().dive().find('#btn_bad_assess_next').simulate('click');
    expect(props.nextStep.mock.calls.length).toBe(2); //this second time it was called
    outerWrapper.dive().dive().find('#btn_bad_assess_prev').simulate('click');
    expect(props.prevStep.mock.calls.length).toBe(1); //first time prev was called

    //lets add a dummy wrapped assessment to test the assessment loading on step 1
    outerWrapper.setProps({report: {...report, step: 1}});
    outerWrapper.setProps({assessmentsWrapped: [{id: 433,tite: 'adsasd'}]});
    outerWrapper.dive().dive().instance().handleResolveComponent();
    expect(outerWrapper.dive().props().report.step).toBe(1);
    //now text the next button on loaded assessment
    outerWrapper.dive().dive().find('#btn_assess_next').simulate('click');
     
    //go to step 0 and trigger some props functions
    outerWrapper.setProps({report: {...report, step: 0}});
    const reportWrapper2 = outerWrapper.dive();
    const painMapWrapper2 = outerWrapper.dive().dive().find('PainMapCombined[report]');
    expect(painMapWrapper2.length).toBe(1);


    //smoke test only on handlePainAllOverClose.
    painMapWrapper2.props().handlePainAllOverClose();

    //handleMarkPainAllOver
    outerWrapper.dive().dive().instance().handleMarkPainAllOver(9999,{id:88888});

    //lastly we test the report === null view
    outerWrapper.setProps({report: null});
    expect(outerWrapper.dive().dive().find(ReportInfoContainer).length).toBe(1)


  })
});