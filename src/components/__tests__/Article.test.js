import React from 'react';
import Article from '../Article';
import { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';

describe('<Article />',() => {
  let routerParams = {
      params: 
  	  {
  		  id: 1, subject: "cpgs", slug: "va-dod-opioid-cpgs"
      }, 
  		isExact: true, 
  		path: "", 
  		url: ""
  	};

  it('renders without crashing', () => {
    shallow(<Article match={routerParams}/>);
  });

  it('matches the old snapshot showing nothing changed', () => {
    const tree = renderer.create(<Article match={routerParams}/>).toJSON();
    expect(tree).toMatchSnapshot(); 
  });

})