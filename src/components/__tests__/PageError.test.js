import React from 'react';
import PageError from '../PageError';
import { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';

describe('<PageError />',() => {
  it('renders without crashing', () => {

    shallow(<PageError />);
    
  });

  it('matches the old snapshot showing nothing changed', () => {
    const tree = renderer.create(<PageError />).toJSON();
    expect(tree).toMatchSnapshot(); 
  });
  
});