import React from 'react';
import ReportStart from '../ReportStart';
import { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';


describe('<ReportStart />',() => {
  it('renders without crashing', () => {

    shallow(<ReportStart />);
    
  });


  it('matches the old snapshot showing nothing changed', () => {
    const tree = renderer.create(<ReportStart />).toJSON();
    expect(tree).toMatchSnapshot(); 
  });
  
});