import React from 'react';
import About from '../About';
import { shallow, mount } from 'enzyme';
import Paper from '@material-ui/core/Paper';
import renderer from 'react-test-renderer';

describe('<About />',() => {
  it('renders Aboutwithout crashing', () => {
    const wrapper = mount(<About />);
    // console.log(wrapper.dive().debug());
    expect(wrapper.find(Paper).length).toBe(2);
    expect(wrapper.contains('Christopher Spevak')).toBe(true);
    expect(wrapper.contains('Amy Osik')).toBe(true);

  });

  it('matches the old snapshot showing nothing changed', () => {
    // if the snapshot needs updating run with update flag e.g. yarn run test --updateSnapshot
    const tree = renderer.create(<About />).toJSON();
    expect(tree).toMatchSnapshot(); //triggers error and displays diff whenever anything changes
  });

});