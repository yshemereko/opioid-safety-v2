import React from 'react';
import ReportSummary from '../ReportSummary';
import { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';
import {makeReport} from '../../data/report';
import {makeAssessment, assessmentConfigIds, AssessmentConfig} from '../../data/assessments';
import {getReportAssessmentsWrapped} from '../../containers/selectors';
import {painLevels,painLevelIds} from '../../data/painLevels';
import {bodySectionIds, bodySections} from '../../data/body';
describe('<ReportSummary />',() => {
  it('renders without crashing', () => {
    shallow(<ReportSummary />);
  });

  it('test that method props and class functions work', () => {


    // we need a report
    const reportId = 1111;
    const report = makeReport(reportId, (new Date()).getTime());


    // we need the report to have assessmentsWrapped
    const assessmentId = 123;
    const config = AssessmentConfig[assessmentConfigIds[0]]
    const assessment = {...makeAssessment(assessmentId,config),reportId: reportId};
    const state1 = {
      assessments: {
        [assessmentId]: assessment
      },
      reports: {
        [reportId]: {...report,assessmentIds: [assessmentId]}
      },
      reportIds: [reportId],
      assessmentIds: [assessmentId]
    }
    const assessmentsWrapped = getReportAssessmentsWrapped(state1,reportId);


    //We need some painMarkings
    const painLevel1 = painLevels[painLevelIds[5]];
    const painLevel2 = painLevels[painLevelIds[6]];
    const painLevel3 = painLevels[painLevelIds[7]];
    const bodySection1 = bodySections[bodySectionIds[0]];
    const bodySection2 = bodySections[bodySectionIds[1]];
    const bodySection3 = bodySections[bodySectionIds[2]];
    const painMarkings = [
      {section: bodySection1, painLevel: painLevel1},
      {section: bodySection2, painLevel: painLevel2},
      {section: bodySection3, painLevel: painLevel3}
    ];


    const props1 = {
      report,
      painMarkings: painMarkings,
      assessmentsWrapped: assessmentsWrapped,
      confirmReport: jest.fn(),
      painBoxSize: 20,
      prevStep: jest.fn(),
      nextStep: jest.fn(),
      history: {
        push: jest.fn()
      },
      currentPainAllOver: {}, //TODO
      appInfo: {
        maxPainPoints: 3
      }
    }

    const wrapper = shallow(<ReportSummary {...props1} />)

    //ReportSummary is wrapped by 3 HOCs so we need to dive 3 times
    
    const RSWrapper = wrapper.dive().dive().dive();
 
    //test the toggle state which controlles which side of the humanoid we see
    expect(RSWrapper.state().side).toBe("front");
    RSWrapper.instance().handleToggleBodyMap(); //call instance toggle method
    expect(RSWrapper.state().side).toBe("back");

    //test previous step functions
    RSWrapper.instance().handlePrev();
    expect(props1.prevStep.mock.calls.length).toBe(1);
    //the first argument should be the report and we test it's the correct id
    expect(props1.prevStep.mock.calls[0][0].id).toBe(reportId);
    RSWrapper.instance().handlePrev();
    expect(props1.prevStep.mock.calls.length).toBe(2);

    //test confirm report functions
    RSWrapper.instance().handleConfirm();
    RSWrapper.instance().handleConfirm();
    expect(props1.confirmReport.mock.calls.length).toBe(2);
    expect(props1.confirmReport.mock.calls[0][0].id).toBe(reportId);
    RSWrapper.instance().handleConfirm();
    expect(props1.confirmReport.mock.calls.length).toBe(3);

    let bodyImageToggleButton = RSWrapper.find('WithStyles(Button)[size="small"]');
    //onClick(below) should call our toggle function handleToggleBodyMap() we tested above

    bodyImageToggleButton.props().onClick(); 
    expect(RSWrapper.state().side).toBe("front");


    //handleAssessmentClick should cause navigation via history.push method
    expect(props1.history.push.mock.calls.length).toBe(0);
    RSWrapper.instance().handleAssessmentClick(assessmentId);
    expect(props1.history.push.mock.calls.length).toBe(1);

    
    
  });
  
});