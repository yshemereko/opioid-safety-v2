import React from 'react';
import InfoPage from '../InfoPage';
import { MemoryRouter } from 'react-router';
import { shallow, mount } from 'enzyme';
import Paper from '@material-ui/core/Paper';
import renderer from 'react-test-renderer';

describe('<InfoPage />',() => {
  it('renders Aboutwithout crashing', () => {
    const wrapper = mount(
      <MemoryRouter>
        <InfoPage />
      </MemoryRouter>
    );
  });


  it('matches the old snapshot showing nothing changed', () => {
    const tree = renderer.create(<MemoryRouter><InfoPage /></MemoryRouter>).toJSON();
    expect(tree).toMatchSnapshot();
  });

});