import React from 'react';
import TimeStamp from '../TimeStamp';
import { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';


describe('<TimeStamp />',() => {
  it('renders without crashing', () => {

    shallow(<TimeStamp />);
    
  });


  it('matches the old snapshot showing nothing changed', () => {
    //1540839735301 is opoch for October 29, 2018
    const tree = renderer.create(<TimeStamp epochMS={1540839735301} />).toJSON();
    expect(tree).toMatchSnapshot(); 
  });
  
});