// @flow
import React from 'react';
import BarChart from '@material-ui/icons/BarChart';
import IconButton from '@material-ui/core/IconButton';
import { withRouter } from "react-router";
export type Props = {
  history:{push: (string) => void},
  to: string
};

function GraphsContextMenu(props: Props){
  const onClick = (event: any) => {
    props.history.push(props.to)
  }

  return (<IconButton onClick={onClick} aria-label="View Graphs">
              <BarChart />
  </IconButton>)
}

export default withRouter(GraphsContextMenu);