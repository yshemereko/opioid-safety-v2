// @flow
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import {withRouter} from 'react-router-dom'

type Props = {
  classes: any,
  match: any,
  history: any
};

type State = { 
};

const styles = {
  root: {
    width: '100%',
  },
  nested: {
    paddingLeft: '45px',
  },
  chevron: {
    color: 'white'
  }
};

class InfoLinks extends React.Component<Props, State> {
  state = {};

  handleClick = (e, slug) => {
    this.setState((state) => {
      let o = {};
      o[slug] = !state[slug];
      return o;
    });
  };

  data = require(`../data/${this.props.match.params.subject}.json`);

  redirect = (subject, slug) => {
    this.props.history.push(`/info/${subject}/${slug}`)
  }

  renderListItems = (data) => {
    return (
      data.map((item, index) => {
        return (
          <div key={`item-${index}`}>
            <ListItem button onClick={item.children ? (e) => this.handleClick(e, item.slug) : () => this.redirect(this.props.match.params.subject, item.slug)}>
              <ListItemText primary={item.label} />
              {item.children && this.state[item.slug] &&
                  <ExpandLess color='action'/>
              }
              {item.children && !this.state[item.slug] &&
                  <ExpandMore color='action'/>
              }
            </ListItem>
            {item.children &&
              <Collapse in={this.state[item.slug]} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                  {this.renderListItems(item.children)}
                </List>
              </Collapse>
            }
          </div>
        );
      })
    );
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <List component="nav">
          {this.renderListItems(this.data)}
        </List>
      </div>
    );
  }
}

export default withRouter(withStyles(styles)(InfoLinks));

