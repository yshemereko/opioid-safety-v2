// @flow
import React from 'react';

type Props = { 
  size: number,
  section: { col: number, description: string, id: number, image: string, isBlank: boolean, region: string, row: number, title: string },
};

type State = { 
};

class BodySectionGridItem extends React.Component<Props, State> {

  static defaultProps = {
    size: 15,
    row: 0,
    col: 0
  }


  render () {
    const {section} = this.props;
    const ypos = section.row *  this.props.size;
    const xpos = section.col *  this.props.size;
    

    const markingSize = this.props.size - 2;
    const content = <div style={{border: '1px solid orange', zIndex: 100 , display: 'block', position: 'absolute', width: markingSize, height: markingSize, top: ypos, left: xpos}} >{section.id}</div>;
    return content;
  }
}

export default BodySectionGridItem;