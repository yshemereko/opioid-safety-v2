import React from 'react';
import { shallow,mount } from 'enzyme';
import renderer from 'react-test-renderer';


import ChartHeaderItem from '../ChartHeaderItem';


it('<ChartHeaderItem />', () => {
    const onClickMock = jest.fn();
    const wrapper = shallow(<ChartHeaderItem id={55} onClick={onClickMock} active={false} image={'/barg'} color={'#FF0000'} />);
    const rootGridWraper = wrapper.dive();
    rootGridWraper.props().onClick({}/*Event ob*/)
    expect(onClickMock.mock.calls.length).toBe(1);
    expect(onClickMock.mock.calls[0][0]).toBe(55);

    expect(rootGridWraper.props().style.backgroundColor).toBe('inherit');
    
    wrapper.setProps({active: true});
    expect(wrapper.dive().props().style.backgroundColor).toBe("#FF0000")
});