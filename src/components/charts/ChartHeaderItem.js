import React from 'react';
import Grid from '@material-ui/core/Grid';
type Props = { 
  color: string,
  image: string,
  title: string,
  active?: boolean,
  onClick: (id: number) => void,
  id: number
};

type State = { 

};

class ChartHeaderItem extends React.Component<Props, State>{

  static defaultProps = {
    active: true
  };

  handleOnclick = (event: any) => {
    const {id} = this.props;
    this.props.onClick(id)
  };

  render(){
    const {id,color,image,title,active} = this.props;
    const bgColor = active ? color : 'inherit'
    return <Grid onClick={this.handleOnclick} key={id} xs={3} item style={{backgroundColor: bgColor}}>
      <Grid container style={{height: '100%'}} justify={'center'} alignItems={'center'}>
                <Grid item>
                  <Grid container direction={'column'} justify={'center'} alignItems={'center'}>
                    <Grid item>
                      <img style={{width: 30, height: 30}} src={image} alt="" />
                    </Grid>
                    <Grid item style={{color: 'white',width: '100%', textAlign: 'center'}}>
                      {title}
                    </Grid>
                  </Grid>
                </Grid>
        </Grid>
      </Grid>;
  }
}

export default ChartHeaderItem;