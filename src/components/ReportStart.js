import React from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';


const styles = {

  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: 20
    }
};

class ReportStart extends React.Component {
  handleOnClick = (event) => {
    event.preventDefault();
    this.props.startReport();
  }
  render () {

    return(
      <div style={styles.container}>
        <Typography variant="headline" gutterBottom  >Pain Assesment</Typography>
        <Button onClick={this.handleOnClick} variant="contained" style={{fontWeight: 'bold'}}>
          Start
        </Button>
      </div>
    );
  }
}

export default ReportStart;
