import React from 'react';
import { Route } from "react-router-dom";
import 'typeface-roboto';
import BottomNavigation from './BottomNavigation';
import DynamicAppBar from './DynamicAppBar';
import Paper from '@material-ui/core/Paper';
import {withErrorBoundary} from './hoc';
import PageError from './PageError';

const styles = {
  content: {
    paddingTop: '69px',
    paddingBottom: '56px',
    minHeight: '80vh'
  },
  paper:
  {
    minHeight: '100vh'
  }
}

type Props = { 
  component: any,
  rest: 
  { 
    computedMatch: {path: string, url: string, isExact: boolean, params: Object},
    exact: boolean,
    location: {pathname: string, search: string, hash: string, state: any},
    path: string,
    title: string
  },
  contextMenu: any
};

type State = {
  contextMenu: any
};

class DefaultLayout extends React.Component<Props, State>{
  
  static defaultProps = {
    contextMenu: null
  }

  render(){
    const {component: Component, ...rest} = this.props;
    const Content = withErrorBoundary(Component,PageError);

    return (
      <Route {...rest} render={matchProps => (
        <Paper style={styles.paper}>
          <DynamicAppBar contextMenu={rest.contextMenu} title={rest.title || rest.computedMatch.params.subject} />

          <div style={styles.content} >
            <Content {...matchProps} />
          </div>

          <BottomNavigation {...matchProps}/>
        </Paper>
      )} />
    )
  }
}

export default DefaultLayout;
