// @flow

import React from 'react';
import PainMapCombined from '../components/PainMapCombined'
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TimeStamp from './TimeStamp';
import { withStyles } from '@material-ui/core/styles';
import AssessmentGridItem from './assessment/AssessmentGridItem';
import type {AssessmentWrapperInterface} from '../data/assessments';
import type {ReportInterface} from '../data/report'
import type {BodySectionInterface} from '../data/body';
import type {PainLevelInterface} from '../data/painLevels';
import {withAppInfo, withScreenInfo} from './hoc';
import type {ScreenInfo} from './hoc/withScreenInfo';
import type {AppInfo} from './hoc/withAppInfo';
type PainMarking = {section:BodySectionInterface,painLevel: any};
type GridSpacing = 0|8|16|24|32|40;
type Props = { 
    report: ReportInterface,
    painMarkings:  PainMarking[],
    confirmReport: (report: ReportInterface) => void,
    painBoxSize: number,
    painBoxSpacing: GridSpacing,
    classes: any,
    prevStep: (report: ReportInterface) => void,
    assessmentsWrapped: AssessmentWrapperInterface[],
    history: any,
    currentPainAllOver: PainLevelInterface,
    appInfo: AppInfo,
    screen: ScreenInfo
};

type State = { 
    side: string,
};
const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  rootContainer: {
  },
  topLeftItem: { 
    borderRight: '1px solid white',
    borderTop:'1px solid white'
  },
  topRightItem: { 
    borderTop:'1px solid white'
  },
  insideLeftItem: { 
    borderRight: '1px solid white',
    borderTop:'1px solid white',
    color: 'white',
    minHeight: 50
  },
  insideRightItem: { 
    borderTop:'1px solid white',
    color: 'white',
    minHeight: 50
  },
  bottomLeftItem: { 
    borderRight: '1px solid white',
    borderBottom:'1px solid white',
    borderTop:'1px solid white',
    color: 'white',
    minHeight: 50
  },
  bottomRightItem: { 
    borderBottom:'1px solid white',
    borderTop:'1px solid white',
    color: 'white',
    minHeight: 50
  },
  basicContentItem: {
    color: 'white',
    padding: '10px 0 0px 0'
  },
  blueBar: {
    width: 300, 
    height: 4, 
    borderRadius: 2, 
    backgroundColor: 'rgb(0,73,252)'
  },
  whiteButton: {
    backgroundColor: 'white',
    color: 'rgb(66,0, 110)'
  }
});

class ReportSummary extends React.Component<Props, State> {

  static defaultProps = {
    painBoxSize: 50,
    painBoxSpacing: 24, // enum: 0, 8, 16, 24, 32, 40
  }

  constructor(props){
    super(props)
    this.state = {
      side: 'front'
    }
  }

  render () {
    const {painMarkings: allPainMarkings, report,classes,painBoxSize,assessmentsWrapped,appInfo,currentPainAllOver,screen} = this.props;
    const gridSize = screen.width > 350 ? 15 : 12;

    const boxPaddingTop:number =  12;
    const boxLabelPaddingTop:number =  4;
    const textBoxSize = painBoxSize / 3;
    const painMarkingBoxHeight = (appInfo.maxPainPoints + 1) * (painBoxSize + textBoxSize + boxPaddingTop + boxLabelPaddingTop);
    return(
      <div className={classes.root}>
        <Grid container className={classes.rootContainer}>
          <Grid item xs={4} className={classes.topLeftItem}>
            <Grid container direction={"column"} alignItems={"center"}>
              <Grid item xs={9} className={classes.basicContentItem}>
                Pain Rating
              </Grid>
              <Grid item xs={9} className={classes.basicContentItem}>
                <TimeStamp epochMS={report.created} />
              </Grid>
              <Grid item xs={6}>
                <Grid container style={{height: painMarkingBoxHeight}} direction={"column"} justify={'flex-start'}>

                  {allPainMarkings
                    .filter(({section}) => section.region === 'front')
                    .map(({section,painLevel},i) => {
                    const title = section.region.charAt(0).toUpperCase() + section.region.slice(1);
                    return this.getPainBox(section.id,title,painLevel,boxPaddingTop);
                  })}

                  {allPainMarkings
                    .filter(({section}) => section.region === 'back')
                    .map(({section,painLevel},i) => {
                      const title = section.region.charAt(0).toUpperCase() + section.region.slice(1);
                      return this.getPainBox(section.id,title,painLevel,boxPaddingTop);
                  })}

                  {currentPainAllOver && currentPainAllOver.level !== 0 && this.getPainBox('pain_all_over','Overall',currentPainAllOver,boxPaddingTop)}

                </Grid>
              </Grid>

              <Grid item xs={9} style={{padding: '10px 0 10px 0'}}>
                <Button size={'small'} onClick={() => this.handleToggleBodyMap()} variant={'raised'}>
                    {this.state.side === 'front' ? 'See Back' : 'See Front'}
                </Button>
              </Grid>
            </Grid>
            
          </Grid>
          <Grid item xs={8} className={classes.topRightItem}>
            <Grid container justify={"center"} alignItems={"center"}>
              <Grid item>
                <PainMapCombined currentPainAllOver={this.props.currentPainAllOver} bodySections={[]} painMarkings={this.props.painMarkings} gridSize={gridSize} freeze={true} report={this.props.report} side={this.state.side} />
              </Grid>
            </Grid>
          </Grid>


          {assessmentsWrapped.map(a => <AssessmentGridItem onClick={this.handleAssessmentClick} key={a.assessment.id} assessmentWrapped={a} />)}

          <Grid item xs={12} style={{borderTop: '1px solid white'}} />


          {!report.isComplete && <Grid item xs={12}>
            <Grid container direction={'column'} alignItems={"center"} >
               <Grid item xs={9}>
                  <Grid container style={{height: 40}} alignItems={"center"} justify={"center"} >
                    <Grid item className={classes.blueBar} >
                       {/* Blue Bar */}
                    </Grid>
                  </Grid>
               </Grid>
               <Grid item>
                  <Grid container spacing={24}>
                    <Grid item xs={6}>
                      <Button onClick={this.handlePrev} variant="outlined">Go Back</Button>
                    </Grid>
                    <Grid item xs={6}>
                      <Button variant={'raised'} onClick={this.handleConfirm}>Confirm</Button>
                    </Grid>
                  </Grid>
               </Grid>
            </Grid>
          </Grid>}
        </Grid>

      </div>
    );
  }

  getPainBox = (key: number|string, title: string, painLevel: any,boxPaddingTop) => {

    const {painBoxSize} = this.props;
    const boxLabelPaddingTop:number =  4;
    const textBoxSize = painBoxSize / 3;

    return <React.Fragment key={key}>
      <Grid item style={{padding: boxPaddingTop + 'px 0px 0px 0px'}}>
            <Grid container style={{width: painBoxSize, height: painBoxSize, color: 'white',backgroundColor: painLevel.color}} justify={"center"} alignItems={"center"}>
              <Grid item>{painLevel.level}</Grid>
            </Grid>
      </Grid>
      <Grid item style={{height: textBoxSize,color: 'white',padding: boxLabelPaddingTop +'px 0px 0px 0px',textAlign: 'center'}}>
        {title}
      </Grid>
    </React.Fragment>
  }

  handleAssessmentClick = (assessmentId: number) => {
    this.props.history.push('/assessment/'+assessmentId)
  }

  handleToggleBodyMap = () => {
    this.setState({
      side: this.state.side === 'front' ? 'back' : 'front'
    })
  }

  handlePrev = () => {
    this.props.prevStep(this.props.report);
  }

  handleConfirm = () => {
    this.props.confirmReport(this.props.report);
  }

}

export default withScreenInfo(withAppInfo(withStyles(styles)(ReportSummary)));