import React from 'react';

type Props = { 
};

type State = { 
};

class PageError extends React.Component<Props, State>{
  static defaultProps = {
  }

  render(){
    return <React.Fragment><h1>There has been an error on this page.</h1></React.Fragment>
  }
}

export default PageError;