// @flow
import React from 'react';
import Typography from '@material-ui/core/Typography';

type Props = { 
  match: any,

};

type State = { 
};

const styles={
  text: {
    paddingLeft: '10px',
    paddingRight: '10px',
  },
  link: {
    margin: '1rem'
  }
};

class Artcle extends React.Component<Props, State> {

  data = require(`../data/${this.props.match.params.subject}.json`);

  displayContent = () => {
    const item = this.getItem(this.data, this.props.match.params.slug);

    switch (true) {
      case !!item.content:
        return (
          <Typography style={styles.text} dangerouslySetInnerHTML={{ __html: item.content }} />
        );
      case !!item.url:
        return (
          <Typography style={styles.link} >
            <a href={item.url} target="_blank">{item.label}</a>
          </Typography>
        );
      case !!item.pdf:
        this.handlePdf(item.pdf);
        return (
          <Typography style={styles.link}>
            Opened PDF in new tab...
          </Typography>
          )
      default:
        console.log('Item type not supported');
    }
  };

  getItem = (data: Object, slug: String) => {
    return data.reduce(this.reducer, []).find((item) => item.slug === slug);
  };

  reducer = (accumulator: Object, currentValue: Object) => {
    if (currentValue.children) {
      return [...accumulator, ...currentValue.children];
    }
    return [...accumulator, currentValue];
  }

  handlePdf = (filename: any) => {
    const pdf = `${String(process.env.PUBLIC_URL)}/${filename}`;
    window.open(pdf, '_blank' );
  }

  render() {
    return (
      <React.Fragment>
        {this.displayContent()}
      </React.Fragment>
    );
  }
}

export default Artcle;
