import React from 'react';
import Grid from '@material-ui/core/Grid';
import ChevronRight from '@material-ui/icons/ChevronRight'
import { withStyles } from '@material-ui/core/styles';
const styles = theme => ({
  topLeftItem: { 
    borderRight: '1px solid white',
    borderTop:'1px solid white'
  },
  topRightItem: { 
    borderTop:'1px solid white'
  },
  insideRightItem: { 
    borderTop:'1px solid white',
    color: 'white',
    minHeight: 50
  },
  bottomRightItem: { 
    borderBottom:'1px solid white',
    borderTop:'1px solid white',
    color: 'white',
    minHeight: 50
  },
  basicContentItem: {
    color: 'white',
    padding: '10px 0 10px 0'
  }
});

type Props = { 
  onClick: () => void,
  labelText: string,
  gridCols?: number
};

type State = { 
};

class ReportItemRight extends React.Component<Props, State>{
  static defaultProps = {
    gridCols: 8
  }

  render(){
    const {classes} = this.props;

    return (<Grid item xs={this.props.gridCols} className={classes.insideRightItem}>
                <Grid container style={{height: '100%'}} justify={'center'} alignItems={"center"}>
                  <Grid item xs={1} />
                  <Grid item xs={9}>
                    {this.props.labelText}
                  </Grid>
                  <Grid item xs={2}>
                    <ChevronRight onClick={() => this.props.onClick()} />
                  </Grid>
                </Grid>
              </Grid>);
  }
}

export default withStyles(styles)(ReportItemRight);