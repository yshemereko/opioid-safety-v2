//@flow
import * as React from 'react'
import ReportItemLeft from './ReportItemLeft';
import ReportItemRight from './ReportItemRight';
import Grid from '@material-ui/core/Grid';
import {withScreenInfo} from '../hoc';
import type {AssessmentWrapperInterface} from '../../data/assessments';
type Props = { 
  // icon: React.ComponentType<any>, 
  assessmentWrapped: AssessmentWrapperInterface;
  onClick: (assessmentId: number) => void;
  screen: {width: number, height: number}
};

type State = { 
};

class AssessmentGridItem extends React.Component<Props, State>{
  static defaultProps = {
  }

  handleOnClick = () => {
    const {assessmentWrapped} = this.props;
    this.props.onClick(assessmentWrapped.assessment.id);
  }

  handleGetItems = () => {
    const {assessmentWrapped,screen} = this.props;
    const config = assessmentWrapped.config;
    const assessment = assessmentWrapped.assessment;
    return config.groups.map((g,i) => {
      const title  = g.title ? g.title : assessment.title;
      const label = g.getDisplay(assessment.answers).value;
      const icon  = g.images.length > 0 && g.images[0] ? g.images[0] : config.icon;
      // console.log(this.props);
      const isSmallScreen = screen.width <= 370;
      return  <Grid key={assessment.id + "_" + i} item xs={12}>
                <Grid container>
                  <ReportItemLeft gridCols={isSmallScreen  ? 6 : 4} icon={icon} labelText={title} />
                  <ReportItemRight gridCols={isSmallScreen  ? 6 : 8} labelText={label} onClick={this.handleOnClick} />
        </Grid>
      </Grid>;
    })
  }

  render(){
    return  this.handleGetItems()
  }
}

export default withScreenInfo(AssessmentGridItem);
