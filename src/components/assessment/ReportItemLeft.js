import React from 'react';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import {withScreenInfo} from '../hoc';
const styles = theme => ({
  topLeftItem: { 
    borderRight: '1px solid white',
    borderTop:'1px solid white'
  },
  insideLeftItem: { 
    borderRight: '1px solid white',
    borderTop:'1px solid white',
    color: 'white',
    minHeight: 50
  },
  bottomLeftItem: { 
    borderRight: '1px solid white',
    borderBottom:'1px solid white',
    borderTop:'1px solid white',
    color: 'white',
    minHeight: 50
  },
  basicContentItem: {
    color: 'white',
    padding: '10px 0 10px 0'
  }
});

type Props = { 
  icon: string,
  labelText: string,
  gridCols?: number,
  screen: {width: number, height: number}
};

type State = { 
};

class ReportItemLeft extends React.Component<Props, State>{
  static defaultProps = {
    // format: "MM/DD/YYYY"
    gridCols: 4
  }

  render(){
    const {classes,screen} = this.props;
    const isSmallScreen = screen.width <= 420;
    const iconSize = isSmallScreen ? 24 : 32;
    return (<Grid item xs={this.props.gridCols} className={classes.insideLeftItem}>
                <Grid container style={{height: '100%'}} justify={'center'} alignItems={"center"}>
                  <Grid item xs={3} style={{paddingLeft: 5}}>
                    {this.props.icon ? <img style={{width: iconSize, height: iconSize}} src={this.props.icon} alt="" /> : ''}
                  </Grid>
                  <Grid item xs={9}>
                    {this.props.labelText}
                  </Grid>
                </Grid>
            </Grid>);
  }
}

export default withScreenInfo(withStyles(styles)(ReportItemLeft));