import React from 'react';
import AssessmentGridItem from '../AssessmentGridItem';
import ReportItemRight from '../ReportItemRight';
import { shallow,mount } from 'enzyme';
import renderer from 'react-test-renderer';
import ChevronRight from '@material-ui/icons/ChevronRight'
// import type {AssessmentWrapperInterface} from '../../data/assessments';
import {makeAssessment, AssessmentConfig, assessmentConfigIds, makeAssessmentWrapper} from '../../../data/assessments';
it('displays correct number of ReportItemRight and respondes to onClick event', () => {
  
  //setting up properties to feed into the component
  const assessmentConf = AssessmentConfig[assessmentConfigIds[0]];
  const assessment = makeAssessment(123, assessmentConf)
  const assessmentWrapper = makeAssessmentWrapper(assessment,assessmentConf);
  const onClickMock = jest.fn();
  const wrpr = mount(<AssessmentGridItem screen={{width: 500, height: 800}} onClick={onClickMock} assessmentWrapped={assessmentWrapper} />);
  
  //We expect the number of ReportItemRight components to match the number of groups as asssessment config has.
  const el_rir = wrpr.find(ReportItemRight);
  expect(el_rir.length).toBe(assessmentConf.groups.length);

  //We click on the first ReportItemRight's ChevronRight component to test the onClick callback
  expect(el_rir.first().find(ChevronRight).length).toBe(1);
  el_rir.first().find(ChevronRight).simulate('click');
  //The 1st argument for the click should return the assessment id 
  expect(onClickMock.mock.calls.length).toBe(1); //only called once
  expect(onClickMock.mock.calls[0][0]).toBe(123); //assessment.id === 123
  
  //Test the RWD by make sure the component responds to changes in screen width
  expect(wrpr.props().screen.width).toBe(500)
  //ReportItemRight - larger screens [above width of 370] should set gridCols prop to 8
  expect(el_rir.first().props().gridCols).toBe(8);
  //Now change the screen with to small [below 370]
  wrpr.setProps({screen: {width: 330, height: 800}});
  //ReportItemRight - larger screens [below width of 370] should set gridCols prop to 6
  expect(wrpr.find(ReportItemRight).first().props().gridCols).toBe(6);

  const tree = renderer.create(<AssessmentGridItem screen={{width: 400, height: 800}} onClick={onClickMock} assessmentWrapped={assessmentWrapper} />).toJSON();
  expect(tree).toMatchSnapshot(); 

});
