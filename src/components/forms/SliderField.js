import React from 'react';
import Slider from '@material-ui/lab/Slider';
import {QuestionInterface} from '../../data/questions'
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
type Props = { 
  question: QuestionInterface,
  onChange: (question: QuestionInterface, answer: string) => void,
  answer: string,
  errors: string[]
};

type State = { 
  isDragging: boolean,
  isActive: boolean
};

const styles = theme => ({
    trackBefore: {
      backgroundColor: 'rgb(253, 129, 8)'
    },
    trackBeforeInactive: {
      backgroundColor: 'rgb(187, 186, 193)'
    },
    trackAfter:{
      backgroundColor: 'rgb(187, 186, 193)'
    },
    thumb: {
      backgroundColor: 'white',
      width: 37,
      height: 37
    },
    thumbInactive: {
      backgroundColor: '#D3D3D3',
      opacity: 0.9,
      width: 37,
      height: 37
    },
    track: {
      height: 4
    }
})

type ButtonProps = {
  value: string;
  isActive: boolean;
};

function ButtonIcon(props: ButtonProps){
  const {value,isMoving} = props;
  return <Grid style={{fontSize: isMoving ? '10px' : '18px',color: 'black', fontWeight: 'bold'}} container justify="center" alignItems="center">
    <Grid item>{value}</Grid>
  </Grid>;
}

class SliderField extends React.Component<Props, State>{
  static defaultProps = {
    answer: null,
    errors: []
  }

  constructor(props){
    super(props);
    this.state = {
      isDragging: false,
      isActive: props.answer !== null
    }
    this.handleOnDragEnd = this.handleOnDragEnd.bind(this)
  }

  handleChange = (event: any,answer: number|string) => {
    const {question} = this.props;
    this.props.onChange(question,answer.toString())
    this.handleOnChangeEffects();
  }

  getMinValue = () => {
    const {question} = this.props;
    const {choices} = question;
    return Math.min.apply(null,choices.map(q => parseInt(q.value,10)));
  }

  getMaxValue = () => {
    const {question} = this.props;
    const {choices} = question;
    return Math.max.apply(null,choices.map(q => parseInt(q.value,10)));
  }

  getMiddleValue = () => {

    return (this.getMaxValue() + this.getMinValue())/2;
  }

  handleOnDragStart = () => {
    
    this.setState({
      isDragging: true,
      isActive: true
    })
  }

  handleOnChangeEffects = () => {
    if(!this.state.isDragging){
      this.handleOnDragStart();
      setTimeout(() => this.handleOnDragEnd() ,500)
    }
  }

  handleOnDragEnd(){
    const {answer} = this.props;
    this.setState({
      isDragging: false,
      isActive: answer !== null
    })
  };

  render(){
    const {question, classes, answer, errors} = this.props;
    const {isDragging, isActive} = this.state;
    const slider = classes;
    const sliderClasses = {
      trackBefore: isActive ? slider.trackBefore : slider.trackBeforeInactive,
      thumb: isActive ? slider.thumb : slider.thumbInactive,
      trackAfter: slider.trackAfter,
      track: slider.track
    };

    return <Grid container direction={'column'} style={{padding: 10, color: 'white'}}>
      <Grid item>
        <Grid container justify={'center'} alignItems={'center'}>
          <h3>{question.title}</h3>
        </Grid>
      </Grid>
      <Grid item>
        <Grid container justify={'center'} alignItems={'center'}>
          <Grid item xs={11} style={{padding: 10}}>
            <Slider 
              onDragStart={this.handleOnDragStart} 
       
              onDragEnd={this.handleOnDragEnd} 
              classes={sliderClasses} 
              value={isActive ? parseInt(answer,10) : this.getMiddleValue()}
              thumb={<ButtonIcon  isMoving={isDragging} value={answer} />} 
              min={this.getMinValue()} 
              max={this.getMaxValue()} step={1} 
              onChange={this.handleChange} />
          </Grid>
          <Grid item xs={11} style={{paddingLeft: 15}}>
            <Grid container justify="space-between" alignItems={"center"}>
              {question.choices.filter(ch => ch.title.length > 0).map(ch => {
                return (<Grid item key={ch.value}>
                         {ch.title}
                  </Grid>)
              })}
            </Grid>
          </Grid>
          {errors.length > 0 && <Grid item xs={11} style={{color: 'red',textAlign: 'center'}}>
            {errors.join('')}
          </Grid>}
        </Grid>
      </Grid>
    </Grid>
  }
}

export default withStyles(styles)(SliderField);