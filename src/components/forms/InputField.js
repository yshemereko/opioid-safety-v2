import React from 'react';
import {QuestionInterface} from '../../data/questions'
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';

type Props = { 
  question: QuestionInterface,
  onChange: (question: QuestionInterface, answer: string) => void,
  answer: string|number,
  screen: any,
  errors: string[]
};

type State = { 
  errors: string[];
};


class InputField extends React.Component<Props, State>{
  static defaultProps = {
    errors: []
  }
  handleChange = (event: any) => {
    const {question} = this.props;
    const answer = event.target.value;
    this.props.onChange(question,answer)
  }

  render(){
    const {answer,question,errors} = this.props;
    return  <Grid container direction={'column'} justify="center" alignItems="center" style={{color: 'white'}}>
              <Grid item>
                <h3>{question.title}</h3>
              </Grid>
              <Grid item>
                <FormControl error={errors.length > 0} aria-describedby={"component-helper-text" + question.id}>
                  {/*<InputLabel htmlFor={"component-helper"+ question.id}></InputLabel> */}
                  <Input id={"component-helper"+ question.id} onChange={this.handleChange} value={answer ? answer : ""} />
                  {errors.length > 0 && <FormHelperText id={"component-helper-text" + question.id}>{errors.join('')}</FormHelperText>}
                </FormControl>

              </Grid>
    </Grid>;
  }
}


export default InputField;