import React from 'react';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import type {QuestionInterface} from '../data/questions'
type Props = { 
  question: QuestionInterface,
  gridCols: number,
  onChange: (question: QuestionInterface, answer: number[]|string[]) => void,
  answer: string|any[],
  errors: string[]
};

type State = { 

};
const styles = theme => ({
  item: {
    fontSize: 18,
    height: 40,
    textAlign: 'center',
    textTransform: 'uppercase',
    borderTop: '1px solid white',
    borderRight: '1px solid white',
  },
  itemEnd: {
    fontSize: 18,
    height: 40,
    textAlign: 'center',
    textTransform: 'uppercase',
    borderTop: '1px solid white'
  },
  itemSelected: {
    backgroundColor: 'white',
    color: 'black',
    fontWeight: 'bold'
  },
  itemBlurred: {
    color: 'white',
    backgroundColor: '#010A50'
  },
  optionsContainer: {
    borderBottom: '1px solid white'
  }
});

class GridSelectField extends React.Component<Props, State>{
  static defaultProps = {
    errors: []
  }

  render(){
    const {question,gridCols,classes,errors} = this.props;
    const {choices} = question;
    return <Grid container direction={'column'} style={{color: 'white'}}>
            <Grid item>
              <Grid container  justify={'center'} alignItems={'center'}>
                <Grid item xs={9} style={{textAlign: 'center'}}>
                  <h3>{question.title}</h3>
                </Grid>
              </Grid>
            </Grid>
            <Grid item>
              <Grid container className={classes.optionsContainer}>
                {choices.map((ch,i) => {

                  const itemClass = {
                    root: this.isValueSelected(ch.value) ? classes.itemSelected : classes.itemBlurred
                  }

                  return <Grid item className={this.resolveItemClass(i)} xs={gridCols} key={ch.value} >
                    <Grid onClick={this.handleOnSelect(ch.value)} className={itemClass.root} style={{height: '100%'}} container justify="center" alignItems="center">
                      <Grid item>
                        {ch.title}
                      </Grid>
                    </Grid>
                  </Grid>
                })}
              </Grid>
            </Grid>
            {errors.length > 0 && <Grid item style={{color: 'red',textAlign: 'center'}}>
              {errors.join('')}
            </Grid>}
    </Grid>
  }

  resolveItemClass = (index) => {
    const {gridCols,classes} = this.props;
    const cols = 12/gridCols;
    const modulus = index % cols
    const isLast = modulus === (cols - 1);
    return isLast ? classes.itemEnd : classes.item
  }

  arrayUnique = (arr: any[]) => {
      return arr.filter(function(item:any, pos: number) {
          return arr.indexOf(item) === pos;
      });
  };


  arrayPushUnique = (item:any, arr: any[]) => {
      arr.push(item);
      return this.arrayUnique(arr);
  };

  arrayRemove = (item: any,arr: any[]) => {
      let uniqueArray = this.arrayUnique(arr);
      let pos = uniqueArray.indexOf(item)
      if(pos > -1){
        uniqueArray.splice(pos,1);
      }
      return uniqueArray;
  };

  handleOnSelect = (value) => {
    return (event) => {
 
        const {question} = this.props;
        let values = this.props.answer;
        if(question.multiChoice){
          if(this.isValueSelected(value)){
            values = this.arrayRemove(value,values);
          } else {
            values = this.arrayPushUnique(value,values);
          }
        }else{
          values = value;
        }

        this.props.onChange(question,values);
    }
  };

  isValueSelected = (value) => {

      const {answer,question} = this.props;
      if(question.multiChoice){
        return answer.indexOf(value) > -1
      }else{
        return answer === value;
      }
      
  };
}

export default withStyles(styles)(GridSelectField);

