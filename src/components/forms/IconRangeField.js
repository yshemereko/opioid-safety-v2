import React from 'react';
import {QuestionInterface} from '../../data/questions'
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import {withScreenInfo} from '../hoc';
type Props = { 
  question: QuestionInterface,
  onChange: (question: QuestionInterface, answer: string) => void,
  answer: string|number,
  screen: any,
  errors: string[]
};

type State = { 
};

const styles = theme => ({
    trackBefore: {
      backgroundColor: 'rgb(253, 129, 8)'
    },
    trackAfter:{
      backgroundColor: 'rgb(187, 186, 193)'
    },
    thumb: {
      backgroundColor: 'white',
      width: 37,
      height: 37
    },
    track: {
      height: 4
    }
})

class IconRangeField extends React.Component<Props, State>{
  static defaultProps = {
    answer: 0,
    errors: []
  }

  constructor(props){
    super(props);
    this.state = {
      isDragging: false
    }
  }
  handleChange = (answer: number|string) => {
    const {question} = this.props;
    this.props.onChange(question,answer)
  }

  render(){
    const {question, answer, screen, errors} = this.props;

    const {width} = screen;
    const shrinkIcons = width <= 370;
    const [imageActive,imageBlur] = question.images

    return <Grid container direction={'column'} style={{padding: 10, color: 'white'}}>
      <Grid item>
        <Grid container justify={'center'} alignItems={'center'}>
          <Grid item>
            <h3>{question.title}</h3>
          </Grid>
        </Grid>
        
      </Grid>
      <Grid item>
        <Grid container direction={'column'} justify={'center'} alignItems={'center'}>
          <Grid item xs={shrinkIcons ? 12 : 11}>
            <Grid container justify="space-between" alignItems={"center"}>
              {question.choices.filter(ch => ch.title.length > 0).map(ch => {
                return (<Grid item key={ch.value}>
                          <Grid container direction={'column'} alignItems="center">
                            <Grid item onClick={() => this.handleChange(ch.value)}>
                              <img style={{width: shrinkIcons ? 20 : 40, height: shrinkIcons ? 20 : 40}} src={parseInt(ch.value,10) <= parseInt(answer,10) ? imageActive : imageBlur} alt="" />
                            </Grid>
                            <Grid item>
                              {ch.title}
                            </Grid>
                          </Grid>
                         </Grid>)
              })}
            </Grid>
          </Grid>
          {errors.length > 0 && <Grid item style={{color: 'red',textAlign: 'center'}}>
            {errors.join('')}
          </Grid>}
        </Grid>
      </Grid>
    </Grid>
  }
}

export default withScreenInfo(withStyles(styles)(IconRangeField));