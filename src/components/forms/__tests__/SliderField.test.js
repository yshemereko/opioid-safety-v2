import React from 'react';
import { shallow,mount } from 'enzyme';
import renderer from 'react-test-renderer';


import SliderField from '../SliderField';
import Slider from '@material-ui/lab/Slider';
import {makeQuestion} from '../../../data/questions';
import {NutritionAssessmentBuilder} from  '../../../data/assessments';

const choicesSet: ChoicesInterface[] = [
  {title: 'Sleepless', value: '1', score: 1},
  {title: '', value: '2', score: 2},
  {title: '', value: '3', score: 3},
  {title: '', value: '4', score: 4},
  {title: '', value: '5', score: 5},
  {title: '', value: '6', score: 6},
  {title: '', value: '7', score: 7},
  {title: '', value: '8', score: 8},
  {title: '', value: '9', score: 9},
  {title: 'Rested', value: '10', score: 10},   
];

it('<SliderField />', () => {
  const question = makeQuestion(123,"What is your quest?","slider",choicesSet);
  const onChangeDispatch = jest.fn();
 
  const wrapper = mount(<SliderField answer={null} onChange={onChangeDispatch} question={question} />);

  expect(wrapper.props().answer).toBe(null);
  
  const slider = wrapper.find(Slider);
  expect(slider.length).toBe(1);

  slider.props().onChange({},'2');

  expect(onChangeDispatch.mock.calls.length).toBe(1);
  expect(onChangeDispatch.mock.calls[0][1]).toBe('2');
  
  const wrapperShallow = shallow(<SliderField answer={null} onChange={onChangeDispatch} question={question} />);
  const SliderWrapper = wrapperShallow.dive();
  SliderWrapper.instance().handleOnDragStart();
  // wrapperShallow.dive().update();
  expect(SliderWrapper.state('isDragging')).toBe(true);
  SliderWrapper.instance().handleOnDragEnd();
  expect(SliderWrapper.state('isDragging')).toBe(false);



});

