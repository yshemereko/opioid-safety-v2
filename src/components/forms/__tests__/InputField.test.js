import React from 'react';
import { shallow,mount } from 'enzyme';
import renderer from 'react-test-renderer';
import {makeQuestion} from '../../../data/questions';
import InputField from '../InputField';

const choicesSet: ChoicesInterface[] = [
  {title: 'Sleepless', value: '1', score: 1},
  {title: '', value: '2', score: 2},
  {title: '', value: '3', score: 3},
  {title: '', value: '4', score: 4},
  {title: '', value: '5', score: 5},

  {title: '', value: '6', score: 6},
  {title: '', value: '7', score: 7},
  {title: '', value: '8', score: 8},
  {title: '', value: '9', score: 9},
  {title: 'Rested', value: '10', score: 10},   
];

it("<InputField />",() => {
  const question = makeQuestion(123,"What is your quest?","slider",choicesSet);
  const onChangeDispatch = jest.fn();

  const wrapper = mount(<InputField answer={""} question={question} onChange={onChangeDispatch} errors={['invalid cheese']} />);

  const inputField = wrapper.find("input");
  expect(inputField.length).toBe(1);

  let textAnswer = '';
  inputField.props().onChange({ target: { value: 'c' } })
  textAnswer = onChangeDispatch.mock.calls[0][1];
  expect(textAnswer).toBe('c');

  wrapper.setProps({answer: 'turkey lurky'})
  const inputField2 = wrapper.find("input");
  expect(inputField2.props().value).toBe("turkey lurky");
})
