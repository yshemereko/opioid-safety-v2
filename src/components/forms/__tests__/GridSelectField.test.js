import React from 'react';
import { shallow,mount } from 'enzyme';
import renderer from 'react-test-renderer';

import GridSelectField from '../GridSelectField';
import Grid from '@material-ui/core/Grid';
import {makeQuestion} from '../../../data/questions';

const choicesSet: ChoicesInterface[] = [
  {title: 'Sleepless', value: '1', score: 1},
  {title: '', value: '2', score: 2},
  {title: '', value: '3', score: 3},
  {title: '', value: '4', score: 4},
  {title: '', value: '5', score: 5},

  {title: '', value: '6', score: 6},
  {title: '', value: '7', score: 7},
  {title: '', value: '8', score: 8},
  {title: '', value: '9', score: 9},
  {title: 'Rested', value: '10', score: 10},   
];

it('renders without crashing', () => {
   //create a SINGLE CHOICE question
  const question = makeQuestion(123,"What is your quest?","slider",choicesSet);
  const onChangeDispatch = jest.fn();
  const wrapper  = mount(<GridSelectField answer={undefined} onChange={onChangeDispatch} question={question} gridCols={4} errors={[]} />);
  
  const selectables = wrapper.find('Grid[onClick]'); //expecting each choice to create a Grid has onClick property

  expect(selectables.length).toBe(choicesSet.length);

  selectables.first().simulate('click');
  selectables.last().simulate('click');

  expect(onChangeDispatch.mock.calls.length).toBe(2);
  //get second argument of second call
  const valueOfLastClick = onChangeDispatch.mock.calls[1][1];
  expect(valueOfLastClick).toBe(choicesSet[choicesSet.length - 1].value); //should be the value for the last choiceset item

  //create a MULTIPLE CHOICE question
  const question2 = makeQuestion(123,"What is your quest?","slider",choicesSet,true);
  const onChangeDispatch2 = jest.fn();
  const wrapper2  = mount(<GridSelectField answer={[]} onChange={onChangeDispatch2} question={question2} gridCols={4} errors={[]} />);
  const selectables2 = wrapper2.find('Grid[onClick]');
  //select the second and third item
  let answers = [];

  selectables2.at(1).simulate('click');
  answers = onChangeDispatch2.mock.calls[0][1];
  wrapper2.setProps({answer: answers});

  selectables2.at(2).simulate('click');
  answers = onChangeDispatch2.mock.calls[1][1];
  expect(answers).toEqual(['2','3']);
  wrapper2.setProps({answer: answers});

  //click choice 3 again and make sure it is removed
  selectables2.at(2).simulate('click');
  answers = onChangeDispatch2.mock.calls[2][1];
  expect(answers).toEqual(['2']);

  const tree = renderer.create(<GridSelectField answer={[]} onChange={onChangeDispatch} question={question} gridCols={4} errors={[]} />).toJSON();
  expect(tree).toMatchSnapshot(); 




});