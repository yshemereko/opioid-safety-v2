import React from 'react';
import { shallow,mount } from 'enzyme';
import renderer from 'react-test-renderer';


import IconRangeField from '../IconRangeField';

import {NutritionAssessmentBuilder} from  '../../../data/assessments';
/**
 * Test the icon range selector
 * 
 * This test is dependant on NutritionAssessmentBuilder which contains icon range questions
 * we can use.
 */

it('renders without crashing', () => {
 
    const group = NutritionAssessmentBuilder.getConfig().groups[1]; //second group contains apple icon selector
    const iconQuestion = group.getQuestions()[1];
    const [imageActive,imageBlur] = iconQuestion.images;


    const onChangeDispatch = jest.fn();
    const wrapper = mount(<IconRangeField screen={{width: 400, height: 800}} answer={undefined} question={iconQuestion} onChange={onChangeDispatch} errors={[]} />);
    const unselectedIcons = wrapper.find(`img[src="${imageBlur}"]`);
    expect(unselectedIcons.length).toBe(iconQuestion.choices.length);

    //click on 3rd icon which should result in 3 icons being active
    // and the remaining inactive
    unselectedIcons.at(2).simulate('click');

    const selectedAnswer = onChangeDispatch.mock.calls[0][1];
    expect(selectedAnswer).toBe(iconQuestion.choices[2].value);

    wrapper.setProps({answer: selectedAnswer});

    const unselectedIcons2 = wrapper.find(`img[src="${imageBlur}"]`);
    expect(unselectedIcons2.length).toBe(iconQuestion.choices.length - 3);

    const selectedIcons = wrapper.find(`img[src="${imageActive}"]`);
    expect(selectedIcons.length).toBe(3);

    //RWD test, Currently the icons should be at there large size because screen.width > 370
 
    expect(unselectedIcons2.first().props().style.width).toBe(40);
    wrapper.setProps({screen: {width: 300, height: 800}});

    const unselectedIcons3 = wrapper.find(`img[src="${imageBlur}"]`);
    expect(unselectedIcons3.first().props().style.width).toBe(20);


});