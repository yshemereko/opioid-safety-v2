// @flow
import * as React from "react";
import PainMap from '../components/PainMap';
import type {ReportInterface} from '../data/report';
import type {BodySectionInterface} from '../data/body';
import Grid from '@material-ui/core/Grid';
type Props = { 
    report: ReportInterface,
    children?: any,
    side: string,
    freeze?: boolean,
    gridSize: number,
    painAllOverDialogOpen?: boolean,
    painMarkings: any[],
    currentPainAllOver: Object,
    markPainAllOver?: Function,
    handlePainAllOverClose?: Function,
    bodySections: BodySectionInterface[]
 };

type State = {
};

class PainMapCombined extends React.Component<Props, State>{

  static defaultProps = {
   freeze: false,
   handlePainAllOverClose: () => {},
   markPainAllOver: () => {},
   painAllOverDialogOpen: false
  }

  render(){

    return(
	    <div >
        <PainMap debug={false} {...this.props} />
          <Grid container direction="column" justify={"center"} alignItems={"center"}>    
            <Grid item>
              {this.props.children}
            </Grid>
          </Grid>
	    </div>
    );
  }
}

export default PainMapCombined;

