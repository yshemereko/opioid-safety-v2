// @flow

import * as React from "react";
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import PainSelector from '../containers/PainSelector';

type Props = { 
  open: boolean,
  handleClose: Function,
  selectPain: Function,
  painLevel: ?Object,
  handlePainAllOverClose: Function
 };

type State = {
};

class PainLevelAllOverDialog extends React.Component<Props, State> {

  handleSelect = (painLevelId: number) => {
    const {selectPain} = this.props;
    selectPain(painLevelId);
   
    this.handleClose(); 
  }

  handleClose = () => {
    const {handleClose} = this.props;
    handleClose();
  }

  render() {

    return (
      <React.Fragment>
        <Dialog
        	style={{width: '100%'}}
      		fullScreen
          title={<div><span>Pain Level</span></div>}
          open={this.props.open}
          scroll={'paper'}
        >
          <DialogContent>

             <PainSelector painLevel={this.props.painLevel} selectPain={this.handleSelect} /> 

          </DialogContent>
          <DialogActions>
          	<Button onClick={this.props.handlePainAllOverClose} >
              Cancel
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }

}

export default PainLevelAllOverDialog;
