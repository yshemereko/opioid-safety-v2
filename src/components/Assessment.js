// @flow
import * as React from 'react';
import SliderField from './forms/SliderField';
import GridSelectField from './forms/GridSelectField';
import IconRangeField from './forms/IconRangeField';
import InputField from './forms/InputField';
import type {AssessmentWrapperInterface} from '../data/assessments'
import type {QuestionInterface} from '../data/questions'
import type {ReportInterface} from '../data/report';
import Grid from '@material-ui/core/Grid';
import {withStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
type Props = { 
  assessmentWrapped: AssessmentWrapperInterface,
  answerQuestion: (assessmentId:number,question:QuestionInterface, answer: string|any[]) => void,
  children?: React.Node,
  report: ReportInterface,
  classes: any,
  onComplete?: (report: ReportInterface) => void
};

type State = {
  editedQuestions: {[string]: number},
  errors: {isValid: boolean,data: {[string]: string[]}} 
};

const styles = theme => ({
  title: {
    fontSize: '140%',
    borderBottom: '1px solid rgb(187, 186, 193)',
    textAlign: 'center',
    padding: 10
  },
  subTitle: {
    fontSize: '120%',
    textAlign: 'center',
    padding: 10
  }
});


class Assessment extends React.Component<Props, State>{

  constructor(props){
    super(props);
    this.state = {
      editedQuestions: {},
      errors: {isValid: true,data: {}} 
    }
  }

  noEditNotified: boolean = false

  render(){

    const {assessmentWrapped,classes,onComplete} = this.props;

    return <Grid container direction="column">
      <Grid item>
        <Grid container direction="column" style={{color: 'white'}}>
          <Grid item className={classes.title}>
            {assessmentWrapped.config.title}
          </Grid>
          <Grid item className={classes.subTitle}>
            {assessmentWrapped.config.subTitle}
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid container direction="column" style={{color: 'rgb(187, 186, 193)'}}>
          <Grid item>
            {assessmentWrapped.config.calcQuestions(assessmentWrapped.assessment.answers).map(q => {
                return this.resolveInputField(q,assessmentWrapped.assessment.answers);
            })}
          </Grid>
          <Grid item>
            <Grid container justify={'center'}>
              <Grid item>
                <div style={{padding: 10}}>
                  {this.props.children}
                  {onComplete && <Button onClick={this.handleAssessmentValidation(assessmentWrapped)} variant="outlined">Next</Button>}
                </div>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>;
  }

  handleOnSelectionChange = (question: QuestionInterface,answer: string|any[]) => {
    const {assessmentWrapped,report} = this.props;
    if(report.isComplete){
      if(!this.noEditNotified){
        alert("You cannot change submitted assessments.");//TODO
        this.noEditNotified = true;
      }
      return;
    }
    this.props.answerQuestion(assessmentWrapped.assessment.id,question,answer);
    // this.handleSetQuestionEdited(question.id);
  }
  // handleSetQuestionEdited =(qid) => {
  //   this.setState((prevState,props) => ({
  //     editedQuestions: {...prevState.editedQuestions, [qid]: qid}
  //   }))
  // }
  // handleIsQuestionEdited =(qid) => {
  //   const {editedQuestions} = this.state;
  //   return typeof editedQuestions[qid] !== 'undefined';
  // }

  resolveInputField = (q: any, answers: any) => {
    const {errors} = this.state;
    const answer = answers.hasOwnProperty(q.id) ? answers[q.id] : null;
    let defaultAnswer = q.multiChoice ? [] : "";

    let question_errors = [];

    if(typeof errors.data[q.id] !== 'undefined'  && Array.isArray(errors.data[q.id])){
      question_errors = errors.data[q.id];
    }
    switch(q.type){
      case 'grid_select':
        return <GridSelectField  answer={answer ? answer : defaultAnswer} onChange={this.handleOnSelectionChange} errors={question_errors} gridCols={q.choices.length % 3 === 0 ? 4 : 6} question={q} key={q.id} />;
      case 'icon_slider':
        return <IconRangeField answer={answer ? answer : defaultAnswer} onChange={this.handleOnSelectionChange} errors={question_errors} gridCols={q.choices.length > 3 ? 4 : 6} question={q} key={q.id} />;
      case 'text':
        return <InputField key={q.id} question={q} answer={answer} onChange={this.handleOnSelectionChange} errors={question_errors} />
      default:
        return <SliderField answer={answer ? answer : null} onChange={this.handleOnSelectionChange} errors={question_errors} question={q} key={q.id} />;
    }
  }

  handleAssessmentValidation(assessmentWrapped: AssessmentWrapperInterface){
    const {report, onComplete} = this.props;
    return (Event: any) => {
      const {assessment,config} = assessmentWrapped;

      const validateResult = config.validate(assessment.answers);
      this.setState({
        errors: validateResult
      });
      
      if(validateResult.isValid && onComplete){
          onComplete(report);
      }
    }
  }
}

export default withStyles(styles)(Assessment);