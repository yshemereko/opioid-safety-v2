// @flow
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import { Link } from 'react-router-dom';
import RestoreIcon from '@material-ui/icons/Restore';
import InfoOutlineIcon from '@material-ui/icons/InfoOutlined'
import PollIcon from '@material-ui/icons/Poll'
import BookIcon from '@material-ui/icons/Book'

type Props = { 
  match: any,
  classes: any
};

type State = { 
  value: any
};

const styles = {
  root: {
    bottom: '0',
    position: 'fixed',
    width: '100%',
    background: 'white',
    zIndex: 3
  },
};

class LabelBottomNavigation extends React.Component<Props, State> {
  state = {
    value: 'report',
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;
    const value = this.props.match.path.split('/')[1];

    return (
      <BottomNavigation showLabels value={value} onChange={this.handleChange} className={classes.root} >
        <BottomNavigationAction label="Report" component={Link} to="/" value="" icon={<RestoreIcon />} />
        <BottomNavigationAction label="Results" component={Link} to="/results" value="results" icon={<PollIcon />} />
        <BottomNavigationAction label="Info" component={Link} to="/info" value="info" icon={<BookIcon />} />
        <BottomNavigationAction label="About" component={Link} to="/about" value="about" icon={<InfoOutlineIcon />} />
      </BottomNavigation>
    );
  }
}

export default withStyles(styles)(LabelBottomNavigation);
