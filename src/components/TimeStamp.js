
import moment from 'moment';
import React from 'react';

type Props = { 
  format: string,
  epochMS: number
};

type State = { 

};

class TimeStamp extends React.Component<Props, State>{
  static defaultProps = {
    format: "MM/DD/YYYY"
  }
  constructor(props){
    super(props);
    this.state = {
      date: moment(this.props.epochMS)
    }
  }

  render(){
    const {format} = this.props;
    return <React.Fragment>{this.state.date.format(format)}</React.Fragment>
  }
}

export default TimeStamp;