import React from 'react';

export type ScreenInfo = {
  screen: {width: number,height: number,orientation: string}
};
const withScreenInfo = (WrappedComponent) => {

  return class extends React.Component{

    constructor(props){
      super(props);
      this.state = {
        screen: this.getScreenDimensions()
      }
    }

    render(){
      return <WrappedComponent screen={this.state.screen} {...this.props} />
    }

    componentDidMount(){
      this.handlePageResize();
    }


    getScreenDimensions = () => {
      const orientation = window.innerWidth >= window.innerHeight ? 'landscape' : 'portrait';
      //setState here
      return {
         width: window.innerWidth,
         height: window.innerHeight,
         orientation
      }
    }

    hasScreenChanged = () => {
      const {width, height} = this.state.screen;
      const currentDims = this.getScreenDimensions();

      if(width !== currentDims.width){
        return true;
      }
      if(height !== currentDims.height){
        return true;
      }
      return false;
    }

    handlePageResize = () => {
      let resizeTimeoutId = null;

      window.addEventListener("resize",() => {

         if(resizeTimeoutId){
           clearTimeout(resizeTimeoutId);
         }

         if(this.hasScreenChanged()){

           resizeTimeoutId = setTimeout(
                  () => {
                    
                   this.setState({
                     screen: this.getScreenDimensions()
                   }); 
                    
                  },
                100);
         }

        
      });
    }
  }
}
export default withScreenInfo;