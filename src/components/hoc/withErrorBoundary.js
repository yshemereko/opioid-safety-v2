import React from 'react';

const withErrorBoundary = (WrappedComponent,OnErrorComponent) => {

  return class extends React.Component{

    constructor(props){
      super(props);
      this.state = {
        hasError: false
      }
    }
    
    static getDerivedStateFromError(error) {
      // Update state so the next render will show the fallback UI.
      return { hasError: true };
    }


    render(){
      if(this.state.hasError){
        return <OnErrorComponent {...this.props} />
      }
      return <WrappedComponent {...this.props} />
    }
  }
}

export default withErrorBoundary;