// @flow
import * as React from 'react';
import defaultFrontBodyImage from '../../images/front/front00.svg';
import defaultBackBodyImage from '../../images/back/back00.svg';

// const defaultFrontBodyImage = require('../../images/Front_326.png');
// const defaultBackBodyImage = require('../../images/Back_326.png');

export type AppInfo = {
  defaultFrontBodyImage: string,
  defaultBackBodyImage: string,
  maxPainPoints: number
};

export type Props = {

};

export type State = {
  appInfo: AppInfo
};

const withAppInfo = (WrappedComponent: React.ComponentType<{appInfo: AppInfo}>) => {

  return class extends React.Component<Props,State>{

    constructor(props: Props){
      super(props);
      this.state = {
        appInfo: {
          defaultFrontBodyImage,
          defaultBackBodyImage,
          maxPainPoints: 3
        }
      }
    }

    render(){
      return <WrappedComponent appInfo={this.state.appInfo} {...this.props} />
    }

  };
}

export default withAppInfo;