import React from 'react';
import { shallow,mount } from 'enzyme';
import renderer from 'react-test-renderer';


import withErrorBoundary from '../withErrorBoundary';
it('withErrorBoundary', (done) => {

    const  OuterGoodComponent = (props) => {
      return <div>
            <h3>Should Render</h3>
      </div>;
    }

    // const  OuterBadComponent = (props) => {
    //   throw new Error("Nooooo!!!!!");
    //   return <div>
    //         <h3>Should Not Render</h3>
    //   </div>;
    // }
    
    const  ErrorMessageComponent = (props) => {
      return <div><h3>Oh no!</h3></div>;
    }
    const ErrorBoundaryComponent = withErrorBoundary(OuterGoodComponent,ErrorMessageComponent);
    const wrapper = shallow(<ErrorBoundaryComponent />);
    expect(wrapper.dive().find('h3').text()).toBe("Should Render");
  
    wrapper.setState({hasError: true},() => {
      expect(wrapper.dive().find('h3').text()).toBe("Oh no!");
      done();
    });
    
});