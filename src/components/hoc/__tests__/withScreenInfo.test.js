import React from 'react';
import { shallow,mount } from 'enzyme';
import renderer from 'react-test-renderer';
import withScreenInfo from '../withScreenInfo';

const SimpleComponent = (props) => {
  return <div id="scid" style={{width: props.screen.width,height: props.screen.height}}>
  Stuff!
  </div>
}



it('withScreenInfo', () => {
    jest.useFakeTimers();
    global.innerWidth = 803;
    global.innerHeight = 1001;
    const WrappedComponent = withScreenInfo(SimpleComponent);
    const wrapper1 = shallow(<WrappedComponent />);
    const simpleComponent = wrapper1.dive();

    expect(simpleComponent.props().style.width).toBe(803);
    expect(simpleComponent.props().style.height).toBe(1001);
    
    //test width change
    global.innerWidth = 444;
    global.dispatchEvent(new Event('resize'));
    jest.runAllTimers();
    const simpleComponentWResized = wrapper1.dive();
    expect(simpleComponentWResized.props().style.width).toBe(444);

    //test width height
    global.innerHeight= 1234;
    global.dispatchEvent(new Event('resize'));
    jest.runAllTimers();
    const simpleComponentHResized = wrapper1.dive();
    expect(simpleComponentHResized.props().style.height).toBe(1234);

});