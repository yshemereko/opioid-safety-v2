import React from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TimeStamp from './TimeStamp';
import Visibility from '@material-ui/icons/Visibility'
import { withStyles } from '@material-ui/core/styles';
type Props = { 
  report: any,
  onClick: (reportId: number) => void
};

type State = { 

};
const tileSize = 200;
const styles = theme => ({
  paper: {
    height: tileSize
  },
  dateDate: {
    fontSize: '400%',
    lineHeight: 0,
    color: 'white',
    textAlign: 'right'
  },
  dateMonth: {
    fontSize: '180%',
    lineHeight: 0,
    color: 'white',
    textAlign: 'left'
  },
  icon: {
    color: 'white',
    fontSize: '300%'
  },
  textLarge: {
    color: 'white'
  }
})

class ReportTileItem extends React.Component<Props, State>{
  static defaultProps = {
    // format: "MM/DD/YYYY"
  }

  render(){
    const {report, classes} = this.props;
    return <Grid item xs={6} sm={4}>
            <Paper elevation={10} className={classes.paper} onClick={this.handleOnClick}>
              <Grid container style={{height: '100%'}} direction={'column'} justify={'space-evenly'}>
                <Grid item>
                  <Grid container>
                    <Grid item xs={1}  />
                    <Grid item xs={10}>
                      <Grid container justify={'center'} alignItems={'center'} spacing={8}>
                        <Grid item xs={6} className={classes.dateDate}>
                          <TimeStamp format={'D'} epochMS={report.created} />
                        </Grid>
                        <Grid item xs={6} className={classes.dateMonth}>
                          <TimeStamp format={'MMM'}epochMS={report.created} />
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item xs={1}/>
                  </Grid>
                </Grid>
                <Grid item>
                  <Grid container direction={'column'} justify={'center'} alignItems={'center'}>
                     <Grid item ><Visibility className={classes.icon} /></Grid>
                     <Grid item className={classes.textLarge}>SUMMARY</Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
    </Grid>;
  }

  handleOnClick = (event: any) => {
    this.props.onClick(this.props.report.id)
  }
}

export default withStyles(styles)(ReportTileItem);
