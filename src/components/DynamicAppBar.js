// @flow
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import ArrowBack from '@material-ui/icons/ArrowBack';
import {withRouter} from 'react-router-dom'

const styles = {
  root: {
    flexGrow: 1,
  },
  flex: {
    flexGrow: 1,
    textTransform: 'uppercase',
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

// const pathBack = ((path) => {
//   if (path === '/') {
//     return '';
//   }

//   const a = path.split('/').filter(String);
//   a.pop();
//   return `/${a.join('/')}`;
// });

const onBackClick = ((history) => {
  return (Event: Object) => {
    history.goBack()
  }
});

type Props = { 
  contextMenu: any|null,
  classes: any,
  location: any,
  history: any,
  title: string
};

function DynamicAppBar(props: Props) {
  const { classes,contextMenu } = props;

  return (
    <div className={classes.root}>
      <AppBar position="fixed">
        <Toolbar>
          {!(props.location.pathname === '/') && 
            <IconButton onClick={onBackClick(props.history)} className={classes.menuButton}  aria-label="Go Back">
              <ArrowBack />
            </IconButton>
          }
          <Typography variant="title" className={classes.flex}>
            {props.title || 'Opioid Safety'}
          </Typography>
          {contextMenu}
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default withRouter(withStyles(styles)(DynamicAppBar));
