import * as React from "react";
import { withStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';


const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }
})


class PainSelector extends React.Component{



  handleChange = (event) => {
      const {selectPain} = this.props;

      event.target.value && selectPain(parseInt(event.target.value, 10));
  }

  renderSelectItemText = (lvl,currentLevel) => {
    return <React.Fragment>

	            <span style={{float: 'left',display: 'block',fontWeight: 'bolder',background: lvl.color, padding:10, minWidth: 18, marginRight: 10, textAlign: 'center'}}>{lvl.title}</span> 
	            <span style={{}}>{lvl.description}</span>
	         </React.Fragment>;
  }

  render(){
    const {painLevels,skipNoPain} = this.props;
    const pLevels = painLevels.filter((v,i) => {
      if(skipNoPain && !i){
        return false;
      }
      return true;
    });
    return (
			    <RadioGroup 
            name="painSelect" 
            onChange={this.handleChange}  
            value={this.props.painLevel ? this.props.painLevel.id + "" : null}
            >

			      {pLevels.map((lvl,index) => {
               return (<FormControlLabel 
                          value={lvl.id + ""} 
                          key={index}
                          label={this.renderSelectItemText(lvl,this.props.painLevel)} 
                          control={<Radio style={{minHeight: 30}} key={index} label={this.renderSelectItemText(lvl,this.props.painLevel)} />}
                          />)
                  })}
			    </RadioGroup>

	      );
   }
}

PainSelector.defaultProps = {
    painLevel: null,
    skipNoPain: false
  };

export default withStyles(styles)(PainSelector);