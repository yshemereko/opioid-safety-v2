// @flow
import React from 'react';
import ReportInfoContainer from '../containers/ReportStartContainer'
import PainMapCombined from '../components/PainMapCombined'
import ReportSummary from '../components/ReportSummary'
import Assessment from '../components/Assessment'
import Button from '@material-ui/core/Button';
import type {QuestionInterface} from '../data/questions';
import type {BodySectionInterface} from '../data/body';
import type {AssessmentWrapperInterface} from '../data/assessments'
import type {PainLevelInterface} from '../data/painLevels';
import { withStyles } from '@material-ui/core/styles';
import type {ReportInterface} from '../data/report';
import {withScreenInfo} from './hoc';

const styles = {
  flipButton: {
    position: 'fixed',
    top: '75px',
    right: '20px',
    backgroundColor: 'white',
    color: 'black',
    fontWeight: 'bold'
  },
  painAllOverButton: {
    position: 'fixed',
    top: '75px',
    left: '20px',
    maxWidth: '100px',
    backgroundColor: 'white',
    color: 'black',
    fontWeight: 'bold'
  },
  nextButton: {
    backgroundColor: 'white',
    color: 'black',
    fontWeight: 'bold'
  }
};

type Props = { 
    report: ReportInterface,
    nextStep: (report: {step: number}) => void,
    prevStep: (report: {step: number}) => void,
    answerQuestion: (assessmentId: number,question:QuestionInterface, answer: string|any[]) => void,
    confirmReport: (report:ReportInterface) => void,
    reportInProgress: boolean,
    reportPainMarkings: {section: BodySectionInterface, painLevel: PainLevelInterface}[],
    assessmentsWrapped: AssessmentWrapperInterface[], 
    classes: any,
    history: any,
    markPainAllOver: (painLevelId: number, report: Object) => void,
    currentPainAllOver: Object,
    bodySections: BodySectionInterface[],
    markPain: Function,
    deletePain: Function,
    screen: {width: number, height: number}
 };

type State = {
  mapView: string,
  painAllOverDialogOpen: boolean
};

class Report extends React.Component<Props, State> {
  state = {
   mapView: 'front',
   painAllOverDialogOpen: false
  }

  render () {
    return(
      <div>
        {this.handleResolveComponent()}
      </div>
    );
  }

  handleToggleBodyMap = () => {
    this.setState({
      mapView: this.state.mapView === 'front' ? 'back' : 'front'
    })
  }

  handlePainAllOverDialog = () => {
    this.setState({
      painAllOverDialogOpen: !this.state.painAllOverDialogOpen
    })
  }

  handleMarkPainAllOver = (painLevelId,report) => {
    this.setState({
      painAllOverDialogOpen: false
    })
    this.props.markPainAllOver(painLevelId,report)
  }

  handlePainAllOverClose = () => {
    this.setState({painAllOverDialogOpen: false});
  }


  handleResolveComponent = () => {
    const {report, nextStep,prevStep, assessmentsWrapped, classes,screen} = this.props;
    const gridSize = screen.width > 470 ? 30 : 20;
    if(!report){
        return <ReportInfoContainer />;
    }

    const loadAssessmentSafe = (index) => {
       const {report} = this.props;
    
       if(typeof assessmentsWrapped[index] === 'undefined'){
          return <div>
              <h3 style={{color: 'white'}}>Oops! This Assessment has been removed. Click Next</h3>
              <Button id={"btn_bad_assess_prev"} onClick={() => prevStep(report)} variant="outlined">Prev</Button>
              <Button id={"btn_bad_assess_next"} className={classes.nextButton} onClick={() => nextStep(report)} variant="outlined">Next</Button>
          </div>;
       }else{
          return <Assessment report={report} assessmentWrapped={assessmentsWrapped[index]} onComplete={() => nextStep(report)} answerQuestion={this.props.answerQuestion}>
                    <Button id={'btn_assess_next'} onClick={() => prevStep(report)} variant="outlined">Prev</Button>
          </Assessment>;
       }
    }

    switch(report.step){
      case 1:
        return loadAssessmentSafe(0);
      case 2:
        return loadAssessmentSafe(1);
      case 3:
        return loadAssessmentSafe(2);
      case 4:
        return loadAssessmentSafe(3);
      case 5:
        return loadAssessmentSafe(4);
      case 6:
        return loadAssessmentSafe(5);
      case 7:
        return loadAssessmentSafe(6);
      case 8:
        return <ReportSummary currentPainAllOver={this.props.currentPainAllOver} bodySections={this.props.bodySections} painMarkings={this.props.reportPainMarkings} history={this.props.history} assessmentsWrapped={assessmentsWrapped} prevStep={this.props.prevStep} report={this.props.report} confirmReport={this.props.confirmReport} side={this.state.mapView} onToggleBody={this.handleToggleBodyMap} />;

      default:
        return (<PainMapCombined report={report} side={this.state.mapView} gridSize={gridSize} markPain={this.props.markPain} deletePain={this.props.deletePain} bodySections={this.props.bodySections}  painMarkings={this.props.reportPainMarkings}  painAllOverDialogOpen={this.state.painAllOverDialogOpen} currentPainAllOver={this.props.currentPainAllOver} markPainAllOver={this.handleMarkPainAllOver} handlePainAllOverClose={this.handlePainAllOverClose}>
                    <Button id={"btn_toggle_map"} className={classes.flipButton} onClick={() => this.handleToggleBodyMap()} variant="outlined">{this.state.mapView === 'front' ? 'See Back' : 'See Front'}</Button>
                    <Button id={"btn_pain_dialog"} className={classes.painAllOverButton} onClick={() => this.handlePainAllOverDialog()} variant="outlined">Pain All Over</Button>
                    <Button id={"btn_next_step"} className={classes.nextButton} onClick={() => nextStep(report)} variant="outlined">Next</Button>
               </PainMapCombined>);
    }
  }



}

export default withScreenInfo(withStyles(styles)(Report));
