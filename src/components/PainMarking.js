// @flow
import React from 'react';

type Props = { 
  painLevel: {color: string, description: string, id: number,images: {bodyBack: string, bodyFront: string}, level: number, title: string},
  size: number,
  col: number,
  row: number,
  section: { col: number, description: string, id: number, image: string, isBlank: boolean, region: string, row: number, title: string },
  onClick: Function
};

type State = { 
};

class PainMarking extends React.Component<Props, State> {

  static defaultProps = {
    size: 15,
    row: 0,
    col: 0
  }

  handleGetColor = () => {
    const {painLevel} = this.props;
    return painLevel.color;
  };

  handleOnClick = (event: Object) => {
    const {section,painLevel} = this.props;
    this.props.onClick(section,painLevel)
  };

  render () {
    const ypos = this.props.row *  this.props.size;
    const xpos = this.props.col *  this.props.size;
    const {painLevel} = this.props;
    const markingSize = this.props.size - 2;
    const content = painLevel.level !== 0 ? <div onClick={this.handleOnClick} style={{borderRadius: 25, border: '2px solid black', zIndex: 99, backgroundColor: this.handleGetColor() , display: 'block', position: 'absolute', width: markingSize, height: markingSize, top: ypos, left: xpos}} /> : null;
    return content;
  }
}

export default PainMarking;