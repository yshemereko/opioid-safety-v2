// @flow
import React from 'react';
import { Link } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import FavoriteIcon from '@material-ui/icons/Favorite';
import PersonIcon from '@material-ui/icons/Person';
import CheckIcon from '@material-ui/icons/Check';
import ChevronIcon from '@material-ui/icons/ChevronRight';


import logo from '../images/ncr_logo.png';
import quote from '../images/quote.png';

type Props = { 
};

type State = { 
};

const styles = {
	patientsItem: {
		background: '#57ba93'
	},
	cpgsItem: {
		background: '#4bbc00'
	},
	providersItem: {
		background: '#4348be'
	},
	logo: {
		height: 'auto',
		maxWidth: '100%',
	},
	
	infoPage: {
		display: 'grid',
		gridTemplateRows: 'auto 1fr',
		minHeight: '70vh',
	},
	quote: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center'
	},
}


class InfoPage extends React.Component<Props, State> {
  render() {
    return (
    	<div style={styles.infoPage}>

	   		<List component="nav">
	      	<img style={styles.logo} src={logo} alt="NCR logo"/>

	        <ListItem button component={Link} to="/info/patients" style={styles.patientsItem}>
	        	<ListItemIcon>
	            <FavoriteIcon />
	          </ListItemIcon>
	          <ListItemText primary="Patient Resources" />
	          <ListItemIcon>
	            <ChevronIcon />
	          </ListItemIcon>
	        </ListItem>

	        <ListItem button component={Link} to="/info/cpgs" style={styles.cpgsItem}>
	        	<ListItemIcon>
	            <CheckIcon />
	          </ListItemIcon>
	          <ListItemText primary="Clinical Practice Guidelines" />
	          <ListItemIcon>
	            <ChevronIcon />
	          </ListItemIcon>
	        </ListItem>

	        <ListItem button component={Link} to="/info/providers" style={styles.providersItem}>
	        	<ListItemIcon>
	            <PersonIcon />
	          </ListItemIcon>
	          <ListItemText primary="Providers Resources" />
	          <ListItemIcon>
	            <ChevronIcon />
	          </ListItemIcon>
	        </ListItem>
	      </List>

	      <div style={styles.quote}>
	      	<img style={styles.logo} src={quote} alt="NCR logo"/>
	      </div>

    	</div>
    );
  }
}

export default withStyles(styles)(InfoPage);

