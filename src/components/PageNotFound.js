import React from 'react';

type Props = { 
};

type State = { 
};

class PageNotFound extends React.Component<Props, State>{
  static defaultProps = {
  }

  render(){
    return <React.Fragment><h1>Page Not Found</h1></React.Fragment>
  }
}

export default PageNotFound;