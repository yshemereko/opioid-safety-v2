// @flow

import React from 'react';

import PainMarking from './PainMarking';
import PainLevelDialog from './PainLevelDialog';
import PainLevelAllOverDialog from './PainLevelAllOverDialog';
import BodySectionGridItem from './BodySectionGridItem';
import {withAppInfo} from './hoc';
import type {AppInfo} from './hoc/withAppInfo';
import Grid from '@material-ui/core/Grid';

type Side = 'front' | 'back';

type Props = { 
  bodySections: Array<Object>,
  bodyImage:  String,
  report:  Object,
  gridSize:  number,
  markPain: Function,
  markPainAllOver: Function,
  painMarkings: Array<Object>,
  deletePain: any,
  freeze: boolean,
  painAllOverDialogOpen: boolean,
  currentPainAllOver: Object,
  handlePainAllOverClose: Function,
  debug?: boolean,
  appInfo: AppInfo,
  side: Side
};

type State = {
  dotPos: {top: number, left: number},
  painMarkings: Array<mixed>,
  dialogOpen: boolean,
  dialogAllOverOpen: boolean,
  activeSection: ?number,
  currentPainLevel: ?any
};



class PainMap extends React.Component<Props, State> {

  static defaultProps = {
    gridSize: 20,
    painAllOverDialogOpen: false,
    debug: false
  }

  boundingRect: any;
  mapBox: any;


  state = {
    dotPos: {top: 10, left: 10},
    painMarkings: [],
    dialogOpen: false,
    activeSection: null,
    currentPainLevel: null,
    dialogAllOverOpen: false
  }


  constructor(props: Props){
    super(props)
    this.mapBox = null;
    this.boundingRect = null;
  }



  handleUpdateBoundingClientRect = () => {
    this.boundingRect = this.mapBox.getBoundingClientRect();
  }

  handleLocateSection = (x: number, y: number) => {

    let relX = x - this.boundingRect.left;
    let relY = y - this.boundingRect.top;
    let col =  Math.floor(relX / this.props.gridSize);
    let row =  Math.floor(relY / this.props.gridSize);


    if(!this.shouldIgnore(row, col)){

      const section = this.handleResolveBodySection(row, col);

      if(section && !this.isSectionSaved(section)){
        const {painMarkings,appInfo} = this.props;
         if(painMarkings.length < appInfo.maxPainPoints){
           this.handleDialogOpen(section);
         } else {
           alert("Too many markings");
         }        
      }
    }
  }


  shouldIgnore = (row: number, col: number) => {
    const section = this.handleResolveBodySection(row, col);
    return !section || section.isBlank;
  }

  isSectionSaved = (section: any) => {
     const {painMarkings} = this.props;
     return painMarkings.filter(mark => mark.section.id === section.id).length > 0;
  }

  getBodySectionsForRegion(){
    return this.props.bodySections.filter(bs => bs.region === this.props.side)
  }

  getPainMarkingsForRegion(){
    return this.props.painMarkings.filter(({section}) => section.region === this.props.side)
  }

  handleResolveBodySection = (row: number, col: number) => {
    let filtered = this.getBodySectionsForRegion().filter((section) => {
       if(section.row === row && section.col === col){
         return true;
       }
       return false;
    });
    return filtered.length ? filtered[0] : null;
  }


  handleGridClick = (event: any) => {
    if(this.props.freeze){
      return;
    }
     event.preventDefault();
     event.stopPropagation();
     this.handleUpdateBoundingClientRect();
     if(typeof event.clientX !== 'undefined'){
       this.handleLocateSection(event.clientX,event.clientY);
     }

  }

  handleGetPainMarkings = () => {
     const {gridSize} = this.props;
     const onClick = !this.props.freeze ? this.handleDialogOpen : () => {};
     return this.getPainMarkingsForRegion().map((data,i) => {
       const {row,col} = data.section;

       return <PainMarking section={data.section} onClick={onClick} painLevel={data.painLevel} key={i} row={row} col={col} size={gridSize} />
     })
  }

  handleShowAllGridItems = () => {
     const {gridSize} = this.props;
     return this.getBodySectionsForRegion().map((section,i) => {
       return <BodySectionGridItem key={section.id} section={section} size={gridSize} />
     })
  }

  handleDialogOpen = (section: any,painLevel: any = null) => {
    this.setState({
      dialogOpen: true,
      activeSection: section,
      currentPainLevel: painLevel
    });
  }

  handleDialogClose = () => {
    this.setState({
      dialogOpen: false,
      activeSection: null,
      currentPainLevel: null
    });
  } 

  handleDialogAllOverClose = () => {
    this.setState({
      dialogAllOverOpen: false
    });
  } 

  handleResolveBodyImage = () => {
    const {appInfo,currentPainAllOver,side} = this.props;

    let  frontBody = appInfo.defaultFrontBodyImage;
    let  backBody = appInfo.defaultBackBodyImage;
    if(currentPainAllOver){
       backBody = currentPainAllOver.images.bodyBack;
       frontBody = currentPainAllOver.images.bodyFront;
    }
    return side === 'back' ? backBody : frontBody
  }


 handleSelectPain = (bodySection: any, painLvlId: number) => {

    this.props.markPain(bodySection,painLvlId,this.props.report);
 }

 handleSelectAllOverPain = (painLvlId: number) => {
   this.props.markPainAllOver(painLvlId,this.props.report);
 }

  handleDeletePain = (section: Object) => {
    this.props.deletePain(section,this.props.report)
  }

  render() {

   const { debug } = this.props;
   const painMarkings = this.handleGetPainMarkings();
   const bodySectionsDebug = debug ? this.handleShowAllGridItems() : [];
   const imgStyle = !debug ? {} : {border: '1px solid red'};
    return (
      <div style={{position: 'relative'}}>
        <Grid container direction="column" justify={"center"} alignItems={"center"}>    
         <Grid item style={{width: this.props.gridSize * 15}}>
            <div onClick={this.handleGridClick}  style={{position: 'relative', width: (this.props.gridSize * 15), height: (this.props.gridSize * 26)}} ref={(el) => { this.mapBox= el; }} >
              {painMarkings}
              {bodySectionsDebug}
              <img alt="Body" style={imgStyle} width={(this.props.gridSize * 15)} height={(this.props.gridSize * 26)} src={ this.handleResolveBodyImage() } />
            </div>
          </Grid>
        </Grid>
        
        
        <PainLevelDialog section={this.state.activeSection} deleteSection={this.handleDeletePain} painLevel={this.state.currentPainLevel} handleClose={this.handleDialogClose} selectPain={this.handleSelectPain} open={this.state.dialogOpen} />
        <PainLevelAllOverDialog painLevel={this.props.currentPainAllOver} handleClose={this.handleDialogAllOverClose} selectPain={this.handleSelectAllOverPain} open={this.props.painAllOverDialogOpen} handlePainAllOverClose={this.props.handlePainAllOverClose}/>

      </div>
    );
  }

}

export default withAppInfo(PainMap);

