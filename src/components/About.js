// @flow
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

type Props = { 
  classes: any
};

type State = { 
};

const styles = {
    contact: {
        textAlign: "center",
        listStyleType: "none",
        padding: "10px"
    },
    version: {
        position: 'fixed',
        bottom: '9%',
        right: '8%',
    }

};

export const contacts = {
    contact1: {
        name: "Christopher Spevak",
        licenses: "MD, MPH, JD",
        title: "NCR Opioid Safety Program Director",
        email: "christopher.j.spevak.civ@mail.mil"
    },
    contact2: {
        name: "Amy Osik",
        licenses: "MS",
        title: "Program Manager",
        email: "amy.j.osik.ctr@mail.mil"
    }
};


class About extends React.Component<Props, State> {

  render() {
    const { classes } = this.props;

      return (
        <div>
          <Paper>
                <Typography className={classes.contact}>
                  <li><strong>{contacts.contact1.name} , {contacts.contact1.licenses}</strong></li>
                  <li>{contacts.contact1.title}</li>
                  <li><a href={`mailto:${contacts.contact1.email}`}>{contacts.contact1.email}</a></li>
                </Typography>
            </Paper>
            <Paper>
                <Typography className={classes.contact}>
                    <li><strong>{contacts.contact2.name} , {contacts.contact2.licenses}</strong></li>
                    <li>{contacts.contact2.title}</li>
                    <li><a href={`mailto:${contacts.contact2.email}`}>{contacts.contact2.email}</a></li>
                </Typography>
            </Paper>
            <Typography className={classes.version}>v2.0.0</Typography>
        </div>
    );
  }
}

export default withStyles(styles)(About);
