// @flow
import React from 'react';
import {LineChart, Line, XAxis, YAxis, Tooltip} from 'recharts';
import ChartHeaderItem from './charts/ChartHeaderItem';
import Grid from '@material-ui/core/Grid';
import type {ReportInterface} from '../data/report';

type Props = { 
  data: {name: string}[],
  lines: {id: number, title: string}[],
  screen: {width: number,height: number,orientation: string},
  reports: ReportInterface[]
};

type State = {
  lineFilters: {[string|number]: boolean}
};

const colors = [
  'rgb(238,0,30)', //red
  'rgb(45,127,130)',//green
  'rgb(244,127,42)', // orange
  'rgb(27,65,162)', // blue
]


class AssessmentCharts extends React.Component<Props, State>{
    static defaultProps = {
    }

  constructor(props: Props){
    super(props);
    this.state = {
      lineFilters: props.lines.reduce((acc,line) => {
              acc[line.id] = true
            return acc
          },{})
    }
  }

  handleLineFilterToggle = (id: number) => {

    this.setState({
       lineFilters: {...this.state.lineFilters,[id]: !this.state.lineFilters[id]}
    });
  }

  handleLineValue = (id: number) => (datum: any) => {
    return datum['value' + id]
  }

  handleResolveColor = (index: number) => {
    return colors[index % colors.length]
  }

  // handleMoveLeft = (currentPage: number, maxPer: number) => {
  //   this.props.moveLeft(currentPage);
  // }

  // handleMoveRight = (currentPage: number, maxPer: number) => {
  //   this.props.moveRight(currentPage);
  // }

  render(){
    const {data,lines,screen,reports} = this.props;

    const {lineFilters} = this.state;
    const graphVisableWidth = screen.width * 1;
    let dataLength =  (reports.length/4) * graphVisableWidth;
    let graphTotalWidth = dataLength <  graphVisableWidth ? graphVisableWidth : dataLength;

    return (
      <div>
        <Grid container direction={'column'}>
          <Grid item>
            <Grid container style={{height: 100}}>
              {lines.map((line,i) => {
                return <ChartHeaderItem active={lineFilters[line.id]} id={line.id} onClick={this.handleLineFilterToggle} key={line.id} color={this.handleResolveColor(i)} title={line.title} image={line.image} />
              })}
            </Grid>
          </Grid>
          <Grid item style={{paddingTop: 20,backgroundColor: 'white'}}>
            <Grid container justify={'center'} alignItems={"center"}>
              <Grid item style={{width: graphVisableWidth, overflowX: 'scroll'}}>
                <LineChart width={graphTotalWidth} height={300} data={data}
                      margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                    <XAxis hide={true} dataKey={"name"} padding={{left: 20, right: 20}} />
                    <YAxis hide={true} padding={{ top: 10 }} />
                    <Tooltip />
                    {/*<Legend iconType="circle" />*/}
                    {lines.map((line,i) => {
                      const lineColor = this.handleResolveColor(i)
                      const isActive = lineFilters[line.id];
                      if(!isActive){
                        return null
                      } 
                      return <Line isAnimationActive={false} dot={{ stroke: lineColor, fill: lineColor, r: 10 }} key={line.id}  name={line.title} dataKey={this.handleLineValue(line.id)} stroke={lineColor} />
                    })}
                </LineChart>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }

}

export default AssessmentCharts;