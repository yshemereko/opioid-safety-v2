import React from 'react';
import Grid from '@material-ui/core/Grid';
import ReportTileItem from './ReportTileItem';
type Props = { 
  reports: any[],
  onClick: (reportId: number) => void,
  setContextMenu: (any) => void
};
type State = { 

};
class Results extends React.Component<Props, State> {
  
  static defaultProps = {
    reports: []
  }

  render () {
    const {reports} = this.props;
    return(
      <div style={{padding: 6}}>
        <Grid container direction={'row'} spacing={8} >
          {reports.map((r) => {
            return <ReportTileItem onClick={this.props.onClick} key={r.id} report={r} />
          })}
        </Grid>
      </div>
    );
  }
}

export default Results;
