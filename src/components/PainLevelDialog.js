// @flow

import * as React from "react";
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import PainSelector from '../containers/PainSelector';

type Props = { 
  open: boolean,
  section: any,
  handleClose: Function,
  selectPain: Function,
  painLevel: ?Object,
  deleteSection: Function
 };

type State = {
  open: boolean
};

class PainLevelDialog extends React.Component<Props, State>  {

  static defaultProps = {
    deleteSection: (sectionId: number) => {}
  };

  constructor(props: Props){
    super(props);
    this.state = {
      open: this.props.open
    }
  }

  componentWillReceiveProps(nextProps: Props){
      this.setState(
        {
          open: nextProps.open
        }
      );
  }

  handleOpen = () => {
    this.setState({open: true});
  }

  handleDelete = (event: any) => {
    const {deleteSection, section} = this.props;

    section && deleteSection(section);
    this.handleClose();
  }

  handleClose = () => {
    const {handleClose} = this.props;
    handleClose();
    this.setState({open: false});
  }

  handleSelect = (painLevelId: number) => {
    const {selectPain} = this.props;
    if(painLevelId !== 1){
      selectPain(this.props.section,painLevelId);
    } else {
      //TODO add delete action
      this.props.deleteSection(this.props.section);
    }
    this.handleClose(); 
  }

  render() {

    return (
      <React.Fragment>
        <Dialog
        	style={{width: '100%'}}
      		fullScreen
          title={<div><span>Pain Level</span></div>}
          open={this.state.open}
          scroll={'paper'}
        >
          <DialogContent>

             <PainSelector painLevel={this.props.painLevel} selectPain={this.handleSelect} /> 

          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} >
              Cancel
            </Button>
            {this.props.painLevel ? <Button onClick={this.handleDelete} >
              Remove
            </Button> : null}
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }

}

export default PainLevelDialog;
