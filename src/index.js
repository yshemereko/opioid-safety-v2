import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
import configureStore from './configureStore';
import { PersistGate } from 'redux-persist/integration/react'
import { HashRouter } from 'react-router-dom';

const {store, persistor} = configureStore();





if(process.env.REACT_APP_PLATFORM === 'cordova'){
  document.addEventListener("deviceready", function(){
    renderApp()
  })
} else {
  renderApp()
}



function renderApp(){

  store.subscribe(() => {
    console.log(store.getState())
  });

  ReactDOM.render(

    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <HashRouter>
            <App />
          </HashRouter>
        </PersistGate>
    </Provider>, 
    document.getElementById('root'));
}