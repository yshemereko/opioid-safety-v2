// @flow
import {connect} from 'react-redux';
import Report from '../components/Report'
import type {QuestionInterface} from '../data/questions'

import {nextStep, prevStep, confirmReport, answerQuestion, markPainAllOver,markPain,deletePain} from '../actions';


import {getLastIncompletReport, getReportAssessmentsWrapped,getAllBodySections,getAllSavedPainMarkings,getPainLevelById} from './selectors';



const mapStateToProps = (state, ownProps) => {
  const reportInProgress = state.reportIds.filter(id => state.reports[id].isComplete === false).length > 0;
  const report: any = reportInProgress ? getLastIncompletReport(state) : null
  const reportPainMarkings = report ? getAllSavedPainMarkings(report.id,state) : [];

  return {
    reportInProgress: reportInProgress, /*boolean*/
    report: report,
    bodySections: getAllBodySections(state.bodySections,state.bodySectionIds),
    reportPainMarkings: reportPainMarkings,
    assessmentsWrapped: report ? getReportAssessmentsWrapped(state,report.id) : [],
    currentPainAllOver: report ? getPainLevelById(state,report) : null
  }
}


const mapDispatchToProps = (dispatch:(action: any) => any, ownProps) => {
  return {
    nextStep: (report) => {
      dispatch(nextStep(report))
    },
    prevStep: (report) => {
      dispatch(prevStep(report))
    },
    confirmReport: (report) => {
      dispatch(confirmReport(report))
      ownProps.history.push('/results');
    },
    answerQuestion: (assessmentId: number, question: QuestionInterface,answer: string|any[]) => {
      dispatch(answerQuestion(assessmentId,question,answer))
    },
    markPainAllOver: (painLvlId,report) => {
        dispatch(markPainAllOver(painLvlId,report))
    },
    markPain: (bodySection,painLevelId,report) => {
        dispatch(markPain(bodySection,painLevelId,report))
    },
    deletePain: (bodySection,report) => {
        dispatch(deletePain(bodySection,report))
    }
  }
}


export default connect(mapStateToProps,mapDispatchToProps)(Report)

