import PainSelector from '../components/PainSelector';
import {connect} from 'react-redux';
//TODO consider replacing this container and instead passing painLevels in from a parent

const stateToProps = (state, ownProps) => {
  return {
    painLevels: state.painLevelIds.map(pid => state.painLevels[pid + ''])
  }
}
const dispatchToProps = (dispatch) => {
  return {
  }
}
export default connect(stateToProps,dispatchToProps)(PainSelector);