// @flow
import AssessmentCharts from '../components/AssessmentCharts';
import {connect} from 'react-redux';
import {graphAssessmentGroups} from '../data/assessments';
import {getChartDataForReports} from './selectors';
import {withScreenInfo} from '../components/hoc';
import PainIcon from '../images/assessments/pain-24px.svg';

const stateToProps = (state, ownProps) => {
  let lines = graphAssessmentGroups.map(g => ({id:g.id, title: g.title,image: g.images[0]}));
  lines.unshift({id:'pain', title: 'Pain',image: PainIcon})

  const reports = state.reportIds.map(rid => state.reports[rid]).filter(report => report.isComplete);
  const data = getChartDataForReports(state,reports);
  return {
    reports: reports,
    totalReports: state.reportIds.length,
    data: data,
    lines: lines
  }
}
const dispatchToProps = (dispatch) => {
  return {
  }
}
export default withScreenInfo(connect(stateToProps,dispatchToProps)(AssessmentCharts));
