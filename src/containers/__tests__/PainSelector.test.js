import React from 'react';
import { shallow,mount } from 'enzyme';
import renderer from 'react-test-renderer';
import PainSelectorContainer from '../PainSelector';
import { createMockStore } from 'redux-test-utils';
import {createReportAction} from '../../actions';
import {shallowWithStore} from '../../_test_helpers';

const testState = {
  reportIds: [],
  reports: {},
  painLevels: {},
  painLevelIds: []
};

describe('<PainSelector />', () => {
  it('will render without crashing', () => {
    const store = createMockStore(testState);
    store.dispatch(createReportAction({}));

    const wrapper = shallowWithStore(<PainSelectorContainer />,store);
    wrapper.dive();
  })
})