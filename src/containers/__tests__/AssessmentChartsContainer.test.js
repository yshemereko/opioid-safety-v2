import React from 'react';
import { shallow,mount } from 'enzyme';
import renderer from 'react-test-renderer';
import AssessmentChartsContainer from '../AssessmentChartsContainer';
import { createMockStore } from 'redux-test-utils';
import {createReportAction} from '../../actions';
import {shallowWithStore} from '../../_test_helpers';

const testState = {
  reportIds: [],
  reports: {}
};

describe('<AssessmentChartsContainer />', () => {
  it('will render without crashing', () => {

    const store = createMockStore(testState);
    store.dispatch(createReportAction({}));

    const wrapper = shallowWithStore(<AssessmentChartsContainer />,store);
    wrapper.dive();
  });
});
