//TODO add to docs the use of redux-mock-store
// include the fact that state doesn't update and this is as-designed
import React from 'react';
import { shallow,mount } from 'enzyme';
import renderer from 'react-test-renderer';
import ReportContainer from '../ReportContainer';
// import { createMockStore } from 'redux-test-utils';
import configureStore from 'redux-mock-store'
import {createReportAction} from '../../actions';
import {shallowWithStore} from '../../_test_helpers';
import {bodySections,bodySectionIds} from '../../data/body';
import {makeReport} from '../../data/report';
import {makeQuestion} from '../../data/questions';
import reducer from '../../reducers';
import thunk from 'redux-thunk';
const testState1 = {
  reportIds: [],
  reports: {},
  painLevels: {},
  painLevelIds: [],
  bodySections,
  bodySectionIds
};

const testState2 = {
  reportIds: [1],
  reports: {
    "1": makeReport(1,(new Date()).getTime())
  },
  painLevels: {},
  painLevelIds: [],
  bodySections,
  bodySectionIds
};

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('<ReportContainer />', () => {
  it('will render without crashing', () => {
    const store = mockStore(testState1);
    const report = makeReport(1,(new Date()).getTime());
    const question = makeQuestion(1,"Fake question","slider");
    const pushMockFN = jest.fn();
    const props = {
      history: {
        push: pushMockFN
      }
    }
    const wrapper = shallowWithStore(<ReportContainer {...props} />,store);

    wrapper.props().confirmReport(report);
    expect(pushMockFN.mock.calls.length).toBe(1);
    expect(pushMockFN.mock.calls[0][0]).toBe("/results");

    //check report step starts at 0
    expect(report.step).toBe(0);
    //increment to step 1
    store.clearActions();
    wrapper.props().nextStep(report);

    expect(store.getActions()[0].report.step).toBe(1);
    //decrement back to step 0
    report.step = 1; //set report step to > 0 so we can test prevStep
    store.clearActions();
    wrapper.props().prevStep(report);
    expect(store.getActions()[0].report.step).toBe(0);

    store.clearActions();
    wrapper.props().markPainAllOver(2,report);
    wrapper.props().markPain(bodySections[bodySectionIds[0]], 2,report);
    wrapper.props().answerQuestion(1/*assessmentId*/,question,"1");
    wrapper.props().deletePain(bodySections[bodySectionIds[0]],report);
    expect(store.getActions().length).toBe(4);

    //this test state will cause different branching
    const store2 = mockStore(testState2);
    const wrapper2 = shallowWithStore(<ReportContainer {...props} />,store2);
  })
})