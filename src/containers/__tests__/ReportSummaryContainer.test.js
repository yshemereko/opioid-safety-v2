import React from 'react';
import { shallow,mount } from 'enzyme';
import renderer from 'react-test-renderer';
import ReportSummaryContainer from '../ReportSummaryContainer';
// import { createMockStore } from 'redux-test-utils';
import configureStore from 'redux-mock-store'
import {createReportAction} from '../../actions';
import {shallowWithStore,isFunction} from '../../_test_helpers';
import {bodySections,bodySectionIds} from '../../data/body';
import {makeReport} from '../../data/report';
import {makeQuestion} from '../../data/questions';
import reducer from '../../reducers';
import thunk from 'redux-thunk';
const testState1 = {
  reportIds: [1],
  reports: {
    "1": makeReport(1,(new Date()).getTime())
  },
  painLevels: {},
  painLevelIds: [],
  bodySections,
  bodySectionIds
};

const testState2 = {
  reportIds: [],
  reports: {
  },
  painLevels: {},
  painLevelIds: [],
  bodySections,
  bodySectionIds
};

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('<ReportSummaryContainer />', () => {
  it('will render without crashing', () => {
    const store = mockStore(testState1);

    const props = {
      match: {
        params: {
          reportId: 1
        }
      }
    };

    const wrapper = shallowWithStore(<ReportSummaryContainer {...props} />,store);
    
    //testState2 will cause different branching because report doesn't exist
    const store2 = mockStore(testState2);
    const wrapper2 = shallowWithStore(<ReportSummaryContainer {...props} />,store2);
  })
});