import React from 'react';
import { shallow,mount } from 'enzyme';
import renderer from 'react-test-renderer';
import AssessmentContainer from '../AssessmentContainer';
import { createMockStore } from 'redux-test-utils';
import {createReportAction} from '../../actions';
import {shallowWithStore} from '../../_test_helpers';
import {SleepAssessmentBuilder} from '../../data/assessments'

const testState = {
  reportIds: [],
  reports: {},
  assessments: {'1': {
    reportId: 1,
    configId: SleepAssessmentBuilder.getConfig().id
  }},
  assessmentIds: [1]
};

describe('<AssessmentContainer />', () => {
  it('will render without crashing', () => {
    const store = createMockStore(testState);
    store.dispatch(createReportAction({}));
    const mockProps = {
      match: {
        params: {
          assessmentId: 1
        }
      }
    }
    const wrapper = shallowWithStore(<AssessmentContainer {...mockProps} />,store);
    //wrapper.dive();
  })
})