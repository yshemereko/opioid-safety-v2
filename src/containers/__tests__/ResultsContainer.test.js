import React from 'react';
import { shallow,mount } from 'enzyme';
import renderer from 'react-test-renderer';
import ResultsContainer from '../ResultsContainer';
// import { createMockStore } from 'redux-test-utils';
import configureStore from 'redux-mock-store'
import {createReportAction} from '../../actions';
import {shallowWithStore,isFunction} from '../../_test_helpers';
import {bodySections,bodySectionIds} from '../../data/body';
import {makeReport} from '../../data/report';
import {makeQuestion} from '../../data/questions';
import reducer from '../../reducers';
import thunk from 'redux-thunk';

const testState1 = {
  reportIds: [1],
  reports: {
    "1": makeReport(1,(new Date()).getTime())
  },
  painLevels: {},
  painLevelIds: [],
  bodySections,
  bodySectionIds
};

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('<ResultsContainer />', () => {
  it('will render without crashing', () => {
    const store = mockStore(testState1);
    const props = {
      history: {
        push: jest.fn()
      }
    };


    const wrapper = shallowWithStore(<ResultsContainer {...props} />,store);

    wrapper.props().onClick(1);
    expect(props.history.push.mock.calls.length).toBe(1);
  })
});