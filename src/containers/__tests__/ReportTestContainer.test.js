//ReportStartContainer.test.js
import React from 'react';
import { shallow,mount } from 'enzyme';
import renderer from 'react-test-renderer';
import ReportTestContainer from '../ReportTestContainer';
// import { createMockStore } from 'redux-test-utils';
import configureStore from 'redux-mock-store'
import {createReportAction} from '../../actions';
import {shallowWithStore,isFunction} from '../../_test_helpers';
import {bodySections,bodySectionIds} from '../../data/body';
import {makeReport} from '../../data/report';
import {makeQuestion} from '../../data/questions';
import reducer from '../../reducers';
import thunk from 'redux-thunk';

const testState1 = {
  reportIds: [],
  reports: {},
  painLevels: {},
  painLevelIds: [],
  bodySections,
  bodySectionIds
};

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('<ReportTestContainer />', () => {
  it('will render without crashing', () => {
    const store = mockStore(testState1);
    const props = {

    }

    const ogDispatch = store.dispatch;
    //monkey patch to get round thunk issue
    const dispatchIntercept = (action) => { //w
      if(isFunction(action)){
        //ignore
        return 1; //startReport will internally call a dispatch with a thunk that returns a report id
      }
      ogDispatch(action)
    }
    store.dispatch = dispatchIntercept;

    const wrapper = shallowWithStore(<ReportTestContainer {...props} />,store);
    wrapper.props().startReport(1);
    wrapper.props().startReport();//branching for default argument
    store.dispatch = ogDispatch;//TODO remove monkey patch using a middleware solution

  })
});