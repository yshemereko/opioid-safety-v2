import {connect} from 'react-redux';
import ReportStart from '../components/ReportStart'
import {createNReports} from '../actions'


const mapStateToProps = (state, ownProps) => {
  return {

  }
}


const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    startReport: (numReports: number = 8) => {
      dispatch(createNReports(numReports));
    }
  }
}


export default connect(mapStateToProps,mapDispatchToProps)(ReportStart)