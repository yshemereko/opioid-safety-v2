import Assessment from '../components/Assessment';
import {connect} from 'react-redux';
import {getAssessmentWrapped} from './selectors';
import {answerQuestion} from '../actions';

const stateToProps = (state, ownProps) => {
  const {match} = ownProps;
  const assessmentWrapped = getAssessmentWrapped(state,match.params.assessmentId);
  const reportId = assessmentWrapped ? assessmentWrapped.assessment.reportId : 0;
  const report = reportId ? state.reports[reportId] : null;
  return {
    assessmentWrapped: assessmentWrapped,
    report
  }
}
const dispatchToProps = (dispatch) => {
  return {
    answerQuestion: (assessmentId:number,question:QuestionInterface, answer: string|any[]) => {
       dispatch(answerQuestion(assessmentId,question,answer))
    }
  }
}
export default connect(stateToProps,dispatchToProps)(Assessment);