import {
  getChartDataForReports,
  getReportPainAverage,
  getAssessmentWrapped,
  getReportAssessmentsWrapped,
  getPainLevelById,
  getAllSavedPainMarkings,
  getReportAssessments
} from '../index'
import {createNReports} from '../../../actions';
import reducer from '../../../reducers';
import {isFunction} from '../../../_test_helpers';
import {makeReport} from '../../../data/report';
import {painLevels,painLevelIds} from '../../../data/painLevels';
import {bodySectionIds, bodySections} from '../../../data/body';
import {makeAssessment, assessmentConfigIds, AssessmentConfig} from '../../../data/assessments';
import {createNewReportAndAssessments} from '../../../reducers/__tests__/index.test.js';

describe("Test selectors which are a list of functions that all containers can use to retrieved data from the state",() => {
  it("gets data from getChartDataForReports",() => {
      const state1 = createNewReportAndAssessments(reducer(undefined,{}));
      const chartData = getChartDataForReports(state1,state1.reportIds.map(rid => state1.reports[rid]));
      //only one report so we expect only one item in data
      expect(chartData.length).toBe(1);
  });

  it("gets pain average from getReportPainAverage",() => {

      const state1 =  reducer(undefined,{});

      state1.reports[1] = makeReport(1,(new Date()).getTime())
      state1.reportIds.push(1);

      const painLevelAllOver = state1.painLevels[state1.painLevelIds[3]];
      state1.reports[1].painAllOver = painLevelAllOver.id;

      const painLevel1 = state1.painLevels[state1.painLevelIds[5]];
      const painLevel2 = state1.painLevels[state1.painLevelIds[6]];
      state1.reports[1].painMarkings = {
          [bodySectionIds[0]]: painLevel1.id,
          [bodySectionIds[1]]: painLevel2.id,
          [bodySectionIds[2]]: 34534 //add bogus pain id to make sure it's ignored
      }


      const average = getReportPainAverage(state1,state1.reports[1]);
     
      expect(average).toBe((painLevel1.level + painLevel2.level + painLevelAllOver.level)/3);
  });

  it("wraps an assessment with its config using getAssessmentWrapped",() => {
      const config = AssessmentConfig[assessmentConfigIds[0]]
      const reportId = 1;
      const assessment = {...makeAssessment(123,config),reportId: reportId};
      const state1 = {
        assessments: {
          "123": assessment
        },
        reports: {
          [reportId]: {id: reportId}
        },
        reportIds: [reportId],
        assessmentIds: [123]
      }
      const assessmentWrapped = getAssessmentWrapped(state1,assessment.id);
      expect(assessmentWrapped.assessment).toEqual(assessment);
      expect(assessmentWrapped.config).toBe(config);

     const state2MissingAssessment = {
        assessments: {
        },
        reports: {
          [reportId]: {id: reportId}
        },
        reportIds: [reportId],
        assessmentIds: []
      }
      const assessmentWrappedNull1 = getAssessmentWrapped(state2MissingAssessment ,assessment.id);
      expect(assessmentWrappedNull1).toBe(null);

     const state3InvalidConfig = {
        assessments: {
          "123": {...assessment,configId: 1979}
        },
        reports: {
          [reportId]: {id: reportId}
        },
        reportIds: [reportId],
        assessmentIds: [123]
      }
      const assessmentWrappedNull2 = getAssessmentWrapped(state3InvalidConfig ,assessment.id);
      expect(assessmentWrappedNull2).toBe(null);
  });

  //getReportAssessmentsWrapped
  it("wraps a report's assessment with its config using getReportAssessmentsWrapped",() => {
      const config = AssessmentConfig[assessmentConfigIds[0]]
      const reportId = 1;
      const assessment = {...makeAssessment(123,config),reportId: reportId};
      const state1 = {
        assessments: {
          "123": assessment
        },
        reports: {
          [reportId]: {id: reportId,assessmentIds: [123]}
        },
        reportIds: [reportId],
        assessmentIds: [123]
      }
      const assessmentsWrapped = getReportAssessmentsWrapped(state1,reportId);
      expect(assessmentsWrapped[0].assessment).toEqual(assessment);
      expect(assessmentsWrapped[0].config).toBe(config);
  });

  it("gets the painAllOver level for a report with getPainLevelById",() => {
      const painAllOverLevel = painLevels[painLevelIds[5]];
      const reportId = 1;
      const state1 = {
        assessments: {
        },
        reports: {
          [reportId]: {id: reportId, assessmentIds: [],painAllOver: painAllOverLevel.id}
        },
        reportIds: [reportId],
        assessmentIds: [],
        painLevels: painLevels
      };
      const painLevel = getPainLevelById(state1,state1.reports[reportId]);
      expect(painLevel.id).toBe(painAllOverLevel.id)
  });

  it("gets all of a reports painMarking with getAllSavedPainMarkings",() => {
      const painAllOverLevel = painLevels[painLevelIds[5]];
      const reportId = 1;

      const bodySection1 = bodySections[bodySectionIds[0]];
      const bodySection2 = bodySections[bodySectionIds[1]];
      const painLevels1 = painLevels[painLevelIds[5]];
      const painLevels2 = painLevels[painLevelIds[6]];
      const state1 = {
        assessments: {
        },
        reports: {
          [reportId]: {
            id: reportId, 
            assessmentIds: [],
            painAllOver: painAllOverLevel.id,
            painMarkings: {
              [bodySection1.id]: painLevels1.id,
              [bodySection2.id]: painLevels2.id
            }
          }
        },
        reportIds: [reportId],
        assessmentIds: [],
        painLevels: painLevels,
        painLevelIds: painLevelIds,
        bodySectionIds: bodySectionIds,
        bodySections: bodySections
      };
      const reportPainMarkings = getAllSavedPainMarkings(reportId, state1);
      expect(reportPainMarkings.length).toBe(2);

      const reportPainMarkingsUnsavedReport = getAllSavedPainMarkings(0, state1);
      expect(reportPainMarkingsUnsavedReport.length).toBe(0);

      const reportPainMarkingsInvalidReportId = getAllSavedPainMarkings(777, state1);
      expect(reportPainMarkingsInvalidReportId.length).toBe(0);

  })

  it("should return an empty array if report has no assessments for getReportAssessments",() => {
      const reportId = 1;
      const state1 = {
        assessments: {

        },
        assessmentIds: [],
        reports: {
          [reportId]: {
            id: reportId 
            /* assessmentIds: [] is missing on purpose for this test */
          }
        },
        reportIds: [reportId]
      };
      const assessmentsEmpty = getReportAssessments(state1,reportId);
      expect(assessmentsEmpty).toEqual([]);
  })

});