import {AssessmentConfig,makeAssessmentWrapper,graphAssessmentGroups} from '../../data/assessments';
import moment from 'moment';


export const getAllBodySections = (allBodySections,sectionIds,region) => {
    return sectionIds.map((sid) => {
      return allBodySections[sid]
    });
};

export const getLastIncompletReport = (state: any) => {
   const incompletReportIds = state.reportIds.filter(id => state.reports[id].isComplete === false);
   const reportId = incompletReportIds[0]
   return state.reports[reportId]
}

// export function paginateArray(arr: any,pageIndex: number, perPage: number){
//   const startIdx = pageIndex * perPage;
//   const endIndexExclusive = startIdx + perPage
//   return arr.slice(startIdx,endIndexExclusive);
// }

// export function getReportsPaginated(state,pageIndex: number, perPage: number){
//   const reports = state.reportIds.map(rid => state.reports[rid]);
//   return paginateArray(reports,pageIndex,perPage)
// }

//TODO put state first like all other functions
export const getAllSavedPainMarkings = (reportId: number,state: any) => {
  
  const {reports} = state;
  let defaultMarkings = [];
  if(!reportId){
    return defaultMarkings;
  }
  if(typeof reports[reportId] === 'undefined'){
    return defaultMarkings;
  }

  //return state.painLevels[savePainLevelId];
  let savedSections = reports[reportId].painMarkings;
  let painLevelMap = state.painLevels;
  let allMarkings =  Object.keys(savedSections)
                  .filter((sectionId) => {
                     return typeof state.bodySections[sectionId] !== 'undefined'
                  })
                  .filter((sectionId) => {
                     let painId = savedSections[sectionId];

                     return typeof state.painLevels[painId] !== 'undefined'
                  })
                  .map((sectionId) => {
                    // let painId = savedSections[sectionId];
                    let painLevelId = savedSections[sectionId];
                    let painLevel = painLevelMap[painLevelId]  //state.painLevels[painId];
                    let bodySection = state.bodySections[sectionId];
                    return {section: bodySection, painLevel};
                  })
                  ;

   return allMarkings;


}


export const getReportAssessments = (state:any ,reportId: number) => {
 
  const assessmentIds = state.reports[reportId].assessmentIds;

  if(!Array.isArray(assessmentIds)){
    return [];
  }
  const assessments = assessmentIds.map(aid => state.assessments[aid])
                      .filter(assessment => assessment) //checking if null
                      .filter(assessment => AssessmentConfig[assessment.configId]) //checking if has config
                      .map(assessment => { //adding config object to assessment
                          //assessment['Config'] = AssessmentConfig[assessment.configId]
                          return assessment;
                      }); 

  return assessments;

}

export const getReportAssessmentsWrapped = (state:any ,reportId: number) => {
 
  const assessments = getReportAssessments(state,reportId).map(assessment => { //adding config object to assessment
                          return makeAssessmentWrapper(assessment,AssessmentConfig[assessment.configId]);
                      }); 

  return assessments;

}

export const getPainLevelById = (state,report) => { //TODO change name
   if(!report.painAllOver || typeof state.painLevels[report.painAllOver] === 'undefined'){
      return null
   }
   return state.painLevels[report.painAllOver];
}

export const getAssessmentWrapped = (state:any , assessmentId: number) => {
  const assessment = state.assessments[assessmentId];

  if (!assessment)
    return null;

  const report = state.reports[assessment.reportId];
  if (!report)
    return null;

  const config = AssessmentConfig[assessment.configId];
  if (!config){
    return null;
  }


  return makeAssessmentWrapper(assessment,config);

}

export function roundTo(value: number,decimalPlace:number = 1){
  const factor = 10 * decimalPlace;
  return Math.round(value * factor)/factor;
}

export const getReportPainAverage = (state,report: ReportInterface) => {
    let painRatings = Object.keys(report.painMarkings).map(sectionId => {
      const painLevelId = report.painMarkings[sectionId];
      const painLevel = typeof state.painLevels[painLevelId] !== 'undefined' ? state.painLevels[painLevelId] : null;
      if(!painLevel){
        return -1
      }
      return painLevel.level
    }).filter(level => level > -1);

    const painLevelAllOver = report.painAllOver && typeof state.painLevels[report.painAllOver] !== 'undefined' ? state.painLevels[report.painAllOver] : null;

    if(painLevelAllOver){
      painRatings.push(painLevelAllOver.level)
    }
    
    const calcTotalAccumilator = (acc,level: number) => {
      acc+=level;
      return acc
    }

    return painRatings.length > 0 ? painRatings.reduce(calcTotalAccumilator,0)/painRatings.length : -1;
}

export const getChartDataForReports = (state,reports: ReportInterface[]) => {

   return reports
      .map(report => {
        const graphAssessmentIds = graphAssessmentGroups.map(gr => gr.getConfig().id);

        const assessmentsMap: any[] = getReportAssessments(state,report.id)
                  .filter(assess => graphAssessmentIds.indexOf(assess.configId) > -1)
                  .reduce((acc,assess) => {
                      acc[assess.configId] = assess;
                      return acc;
                    },{});

        const painAverage = getReportPainAverage(state,report);

        const reportCreatedString = moment(report.created).format("MMMM D, YYYY")
        let defaultAccumilator: {[string]: any}  = {"name": reportCreatedString};
        
        if(painAverage > -1){
          defaultAccumilator['valuepain'] = roundTo(painAverage);
        }

        return graphAssessmentGroups.reduce((acc,group) => {
            if(typeof assessmentsMap[group.getConfig().id] !== 'undefined'){
                const answers = assessmentsMap[group.getConfig().id].answers;
                acc['value' + group.getId()] = roundTo(group.calcScore(answers));
            }
            return acc;
        },defaultAccumilator);
      })
}