import {connect} from 'react-redux';
import ReportStart from '../components/ReportStart'
import {createReport} from '../actions'


const mapStateToProps = (state, ownProps) => {
  return {

  }
}


const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    startReport: () => {
      dispatch(createReport());
    }
  }
}


export default connect(mapStateToProps,mapDispatchToProps)(ReportStart)