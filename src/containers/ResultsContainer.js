import Results from '../components/Results';
import {connect} from 'react-redux';


const stateToProps = (state, ownProps) => {
  return {
    reports: state.reportIds.map(rid => state.reports[rid + '']).filter(report => report.isComplete === true)
  }
}
const dispatchToProps = (dispatch,ownProps) => {
  const {history} = ownProps
  return {
    onClick: (reportId: number) => {
      history.push('/results/' + reportId);
    }
  }
}
export default connect(stateToProps,dispatchToProps)(Results);