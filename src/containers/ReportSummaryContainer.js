import {connect} from 'react-redux';
import Report from '../components/ReportSummary'

import {getAllSavedPainMarkings, getReportAssessmentsWrapped, getPainLevelById} from './selectors';

const mapStateToProps = (state, ownProps) => {
  const {match} = ownProps;
  const report = state.reports[match.params.reportId]
  
  return {
    report: report,
    painMarkings: report ? getAllSavedPainMarkings(report.id,state) : [],
    assessmentsWrapped: report ? getReportAssessmentsWrapped(state,report.id) : [],
    currentPainAllOver: report ? getPainLevelById(state,report) : null
  }
}


const mapDispatchToProps = (dispatch, ownProps) => {
  return {
  }
}


export default connect(mapStateToProps,mapDispatchToProps)(Report)