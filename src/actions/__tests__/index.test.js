import {
  confirmReport,
  createNReports,
  createAssessment,
  setStep,
  nextStep,
  prevStep,
  deletePain,
  markPain,
  answerQuestion,
  editAssessment,

  UPDATE_REPORT,
  DELETE_PAIN,
  MARK_PAIN,
  APP_MESSAGE_START
} from '../../actions';


import reducer from '../../reducers';
import { makeReport } from '../../data/report';

import { makeAssessment, AssessmentConfig, assessmentConfigIds, PainMoodAssessmentBuilder } from '../../data/assessments';

function getStateMock(state){
    return () => state;
};

function isFunction(functionToCheck) {
 return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}

describe("Redux index Actions",() => {
  it("should confirm the report by setting property isComplete to true",() => {
      const reportOg = makeReport(321,(new Date()).getTime());
      expect(reportOg.isComplete).toBe(false);

      const confirmReportAction = confirmReport(reportOg);

      expect(confirmReportAction.type).toBe(UPDATE_REPORT);
      expect(confirmReportAction.report.isComplete).toBe(true);

  });

  it("should set the step number of a report",() => {
      const reportOg = makeReport(321,(new Date()).getTime());
      expect(reportOg.step).toBe(0);

      const action = setStep(reportOg,777);

      expect(action.type).toBe(UPDATE_REPORT);
      expect(action.report.step).toBe(777);
  });

  it("should increment and decrement the current step",() => {
      const reportOg = makeReport(321,(new Date()).getTime());
      //set report to step one
      let stepAction = setStep(reportOg,1);
      expect(stepAction.report.step).toBe(1);

      //increment from step ===1 to step === 2
      stepAction = nextStep(stepAction.report,stepAction.report.step);
      expect(stepAction.report.step).toBe(2);

      //decrement from step === 2 to step === 0 by call prevStep twice
      stepAction = prevStep(stepAction.report,stepAction.report.step);
      stepAction = prevStep(stepAction.report,stepAction.report.step);
      expect(stepAction.report.step).toBe(0);

      //try to decrement to step < 0 and confirm step remains equal to 0
      stepAction = prevStep(stepAction.report,stepAction.report.step);
      expect(stepAction.report.step).toBe(0);
  });

  /**
   * For simplicity this test only creates 1 report with 1 assessment
   *
   * To test multiple reports each with multiple assessments would require some refactoring
   */
  it("Should create many reports using createNReports",() => {

      const numReports = 1;
      const assessmentsPerReport = 1;
      const reportId = 1

      const firstAssessConfId = assessmentConfigIds[0];
      const firstAssessConf = AssessmentConfig[firstAssessConfId];

      const numQuestions = firstAssessConf.questions.length;

      const assessmentId = 33
      const assessment1 = makeAssessment(assessmentId,firstAssessConf);

      const createNReportsThunk = createNReports(numReports);



      const dispatchMock1 = jest.fn()
      
      dispatchMock1.mockReturnValueOnce(reportId); //first fall should return report id === 1
      const report = {...makeReport(reportId,(new Date()).getTime()),
                    assessmentIds: [assessment1.id]
                  };

      const state1 = {
        reports: {...{},[report.id]: report},
        assessments: {...{},[assessment1.id]: assessment1},
        assessmentIds: [assessment1.id]
      };


      createNReportsThunk(dispatchMock1,getStateMock(state1));
      expect(dispatchMock1.mock.calls.length).toBe(numReports + numReports * assessmentsPerReport * numQuestions)

  });

  it("prevents deleting pain on a complete report",() => {

      jest.useFakeTimers();

      const state1 = reducer(undefined,{});
      let report = makeReport(22,(new Date()).getTime());
      const bodySection = state1.bodySections[state1.bodySectionIds[0]]
      //report IS NOT complete so delete pain should return action.type === DELETE_PAIN 
      let action = deletePain(bodySection,report);
      expect(action.type).toBe(DELETE_PAIN);

      report = {...report, isComplete: true};

      //report IS complete so delete pain should return sendMessage Thunk
      let actionThunk = deletePain(bodySection,report);
      expect(isFunction(actionThunk)).toBe(true);

      const dispatchMock1 = jest.fn();

      actionThunk(dispatchMock1,getStateMock(state1))

      jest.runAllTimers();

      expect(dispatchMock1.mock.calls.length).toBe(2)

      //get first argument of first call
      const actionMessageStart = dispatchMock1.mock.calls[0][0];
      expect(actionMessageStart.type).toBe(APP_MESSAGE_START);
      
  });
  it("prevent marking pain on a complete report",() => {

      jest.useFakeTimers();

      const state1 = reducer(undefined,{});
      let report = makeReport(22,(new Date()).getTime());
      const bodySection = state1.bodySections[state1.bodySectionIds[0]]
      //report IS NOT complete so delete pain should return action.type === DELETE_PAIN 
      let action = markPain(bodySection,2 /*painLevelId*/,report);
      expect(action.type).toBe(MARK_PAIN);

      report = {...report, isComplete: true};

      //report IS complete so delete pain should return sendMessage Thunk
      let actionThunk = markPain(bodySection,2 /*painLevelId*/,report);
      expect(isFunction(actionThunk)).toBe(true);

      const dispatchMock1 = jest.fn();

      actionThunk(dispatchMock1,getStateMock(state1))

      jest.runAllTimers();

      expect(dispatchMock1.mock.calls.length).toBe(2)

      //get first argument of first call
      const actionMessageStart = dispatchMock1.mock.calls[0][0];
      expect(actionMessageStart.type).toBe(APP_MESSAGE_START);
      
  });
  it("Prevents a multiple choice question from being submitted",() => {
    //the first question of group 2 "Type of pain" is a multiple select question
    const multiChoiceQuestion = PainMoodAssessmentBuilder.getConfig().groups[0].getQuestions()[0]
    expect(() => {
      answerQuestion(1,multiChoiceQuestion,1)
    }).toThrow("Invalid answer structure for multiple choice question. Expected an array");

  });

  it("editing an assessment that has a missing or zero id",() => {

    const firstAssessConfId = assessmentConfigIds[0];
    const firstAssessConf = AssessmentConfig[firstAssessConfId];
    expect(() => {
      editAssessment(1,makeAssessment(0,firstAssessConf))
    }).toThrow("Assessment must have an id. Use createAssessment");

  });

  it("Should throw an error if the assessment config id is invalid",() => {
      const reportId = 1;
      const bogusConfigId = 45454;
      const caThunk = createAssessment(reportId,bogusConfigId);
      const fakeState = {
        reports: {...{},[reportId]: {}}
      }
      expect(() => {
        caThunk(() => {},() => fakeState);
      }).toThrow("Invalid assessment config id");
  });

})