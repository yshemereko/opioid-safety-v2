// @flow
import { makeReport } from '../data/report';
import { makeAssessment, AssessmentConfig, assessmentConfigIds} from '../data/assessments';
import moment from 'moment';
import {nextId} from './_helper';
import {getRandomInt,createRandomPainMarkings,getRandomAnswersForAssessment} from '../data/utils';
import type {QuestionInterface} from '../data/questions';
import type {AssessmentConfigInterface, AssessmentInterface} from '../data/assessments'

export const UPDATE_REPORT = "SDD.UPDATE_REPORT";
export const MARK_PAIN = "SDD.MARK_PAIN";
export const MARK_PAIN_ALL_OVER = "SDD.MARK_PAIN_ALL_OVER";
export const DELETE_PAIN = "SDD.DELETE_PAIN";
export const SET_STEP = "SDD.SET_STEP";
export const UPDATE_ASSESSMENT = "SDD.UPDATE_ASSESSMENT";
export const ANSWER_ASSESSMENT_QUESTION = "SDD.ANSWER_ASSESSMENT_QUESTION";
export const APP_MESSAGE_START = "SDD.APP_MESSAGE_START";
export const APP_MESSAGE_CLEAR = "SDD.APP_MESSAGE_CLEAR";
export const EDIT_PAGINATOR = "SDD.EDIT_PAGINATOR";

export function makePaginator(id:number|string, page: number = 0){
  return {
    id,
    page
  }
}

export const nextPage =(paginatorId: number|string, currentPage: number) => {
   return editPagination(makePaginator(paginatorId,currentPage + 1));
}

export const prevPage =(paginatorId: number|string, currentPage: number) => {
  const prevPage = currentPage > 0 ? currentPage - 1 : 0;
  return editPagination(makePaginator(paginatorId,prevPage));
}

export const editPagination =(paginator: any) => {
  return {
    type: EDIT_PAGINATOR,
    paginator
  }
}

export const createReportAction = (report: any) => {
  return {
    type: UPDATE_REPORT,
    report
  }
}

export const updateAssessmentAction = (reportId: number, assessment:AssessmentInterface) => {

  return {
    type: UPDATE_ASSESSMENT,
    assessment,
    reportId
  }
}

export const createNReports = (reportQuantity: number) => {
    let period = 0;
    let dateRef = moment();
    dateRef.subtract(reportQuantity * 7,"days");
    return (dispatch:any, getState:any) => {
      for(let i = 0; i < reportQuantity; i++){
        let painAllOver = 1 + getRandomInt(10) //returns 1 through 11 and represents pain level ids
        let painMarkings = createRandomPainMarkings(3);
         
        const rid = dispatch(createReport({
          created: dateRef.valueOf(),
          isComplete: true,
          painAllOver,
          painMarkings
        }));

        let state = getState()
        const report = getState().reports[rid];
        const assessments = report.assessmentIds.map((aid) => {
            return state.assessments[aid];
        });

        assessments.forEach((assessment) => {
          const assessmentsAnswers = getRandomAnswersForAssessment(assessment);
          assessmentsAnswers.forEach(({assessmentId,question,answer}) => {
              dispatch(answerQuestion(assessmentId,question,answer))
          }); 
        });

        period++;
        dateRef.add(period * 7,'days');
      }
    }
}

export const createReport = (newProps:any = {}) => {
  return (dispatch:any, getState:any) => {
    const {reportIds} = getState();
    const createdDate = new Date();
    const reportId = nextId(reportIds);
    const report = {...makeReport(reportId,createdDate.getTime()),...newProps};
    dispatch(createReportAction(report));
    assessmentConfigIds.map(cid => {
      const conf = AssessmentConfig[cid];
      if(conf){
        dispatch(createAssessment(report.id,conf.id))
      }
      return cid;
    })
    return reportId;
  }
}

export const editReport = (report:any,editProps:any) => {
  if(!report.id){
     throw new Error('Report must have an id. Use createReport');
  }
  return createReportAction({...report,...editProps})
}

export const createAssessment = (reportId: number, assessmentConfigId: number, newProps: any = {}) => {
  return (dispatch:any, getState:any) => {
    const state = getState()
    const report = state.reports[reportId];

    if(!report){
      throw new Error('Invalid report id');
    }

    const assessmentConfig: AssessmentConfigInterface = AssessmentConfig[assessmentConfigId];
    if(!assessmentConfig){
      throw new Error('Invalid assessment config id');
    }

    const assessmentIds = state.assessmentIds;

    const assessment:AssessmentInterface  = {...makeAssessment(nextId(assessmentIds),assessmentConfig,''),reportId: report.id};

    dispatch(updateAssessmentAction(report.id,assessment));
  }
}

export const editAssessment = (reportId: number, assessment:AssessmentInterface,editProps:any) => {
  if(!assessment.id){
     throw new Error('Assessment must have an id. Use createAssessment');
  }
  return updateAssessmentAction(reportId,{...assessment,...editProps})
}


export const answerQuestion = (assessmentId: number, question: QuestionInterface, answer: string|string[]) => {
  if(question.multiChoice && !Array.isArray(answer)){
    throw new Error("Invalid answer structure for multiple choice question. Expected an array");
  }
  return {
    type: ANSWER_ASSESSMENT_QUESTION,
    multiChoice: question.multiChoice,
    questionId: question.id,
    assessmentId,
    answer: answer
  }
}


export const markPain = (bodySection:any, painLvlId: number,report:any,forceUpdate: boolean = false) => {
  if(report.isComplete && !forceUpdate){
    return cannotEditReportMessage()
  }
  return {
    type: MARK_PAIN,
    bodySection,
    reportId: report.id,
    painLevelId: painLvlId
  }
}


export const markPainAllOver = (painLvlId: number,report:any /*painLevelObject or Painlevel id*/) => {
  return {
    type: MARK_PAIN_ALL_OVER,
    reportId: report.id,
    painLevelId: painLvlId
  }
}


export const deletePain = (bodySection: {id: number},report:any,forceUpdate: boolean = false) => {
  if(report.isComplete && !forceUpdate){
    return cannotEditReportMessage()
  }
  return {
    type: DELETE_PAIN,
    bodySection,
    reportId: report.id
  }
}

export const nextStep = (report:any) => {
  return setStep(report,report.step + 1)
}

export const prevStep = (report:any) => {
  return setStep(report,report.step > 0 ? report.step - 1 : 0)
}

export const setStep = (report:any,step:number) => {
  return editReport(report,{step})
}

export const confirmReport = (report:any) => {
  return editReport(report,{isComplete: true})
}

export const messageStart = (message:string) => {
  return {
    type: APP_MESSAGE_START,
    message
  };
}

export const messageClear = () => {
  return {
    type: APP_MESSAGE_CLEAR
  };
}

export const cannotEditReportMessage = () => {
  return sendMessage("You cannot edit a completed report.")
}


var timeOutId = null
export const sendMessage = (message:string) => {

  return (dispatch:(any)=>any,getState:any) => {
    dispatch(messageStart(message));

    if(timeOutId){
        clearTimeout(timeOutId)
    }

    timeOutId = setTimeout(
                    () => {dispatch(messageClear())}
                    ,3000
                )
  }
};


