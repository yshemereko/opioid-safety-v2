import React from 'react';
import App from '../App';
import { Provider } from 'react-redux';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
const middlewares = [];
const mockStore = configureStore(middlewares);

// import {getMemoryStore} from '../configureStore';
// const {store} = getMemoryStore();

it('renders without crashing', () => {
  const store =  mockStore({})
  shallow(<Provider store={store}><App /></Provider>);
});
