import {
  createReport,
  createReportAction,
  editAssessment,
  markPain,
  deletePain,
  markPainAllOver,
  messageStart,
  messageClear,
  sendMessage,
  answerQuestion,

  UPDATE_REPORT,
  UPDATE_ASSESSMENT,
  DELETE_PAIN,
  APP_MESSAGE_START,
  APP_MESSAGE_CLEAR
} from '../../actions';

import reducer, {
  bodySectionIds,
  bodySections,
  painLevels,
  painLevelIds,
  reportAssessmentIds,
  assessmentsAnswers

}from '../../reducers';

import { makeAssessment, AssessmentConfig, assessmentConfigIds } from '../../data/assessments';
import { makeReport } from '../../data/report';

function getStateMock(state){
  return () => state;
};
function createNewReport(startingState,dispatchMock = jest.fn()){
    const createReportThunk = createReport({});


    createReportThunk(dispatchMock,getStateMock(startingState));

    const updateReportActionArg = dispatchMock.mock.calls[0][0];

    return reducer(startingState,updateReportActionArg);
}

function getFirstBodySection(state){
    const firstBodySectionId = state.bodySectionIds[0];
    const firstBodySection = state.bodySections[firstBodySectionId];
    expect(firstBodySection.id).toBe(1);
    return firstBodySection;
}

function getHighestPainLevel(state){
    const highestPainLevelId = state.painLevelIds[0];
    const highestPainLevel = state.painLevels[highestPainLevelId];
    expect(highestPainLevel.level).toBe(10);
    return highestPainLevel;
}

export function createNewReportAndAssessments(startingState){

    const dispatchMock = jest.fn();

    const state2 = createNewReport(startingState,dispatchMock)

    const createAssessmentDispatchMock = jest.fn();
    let assessmentState = state2;

    for(var i = 1; i < 8; i++){
      let createAssessThunk = dispatchMock.mock.calls[i][0];
      createAssessThunk(createAssessmentDispatchMock,getStateMock(assessmentState));
      let updateAssessmentActionArg = createAssessmentDispatchMock.mock.calls[i - 1][0];
      assessmentState = reducer(assessmentState,updateAssessmentActionArg);
    }

    return assessmentState;
}


describe("Reducer reports test",() => {
  it('Should create 7 assessment stubs when new report is created', () => {
    const createReportThunk = createReport({});
    const dispatchMock = jest.fn();

    const getStateMock = (state) => {
      return () => state;
    };

    const state1 = reducer(undefined,{});//this creates the initial state

    expect(state1.reportIds).toEqual([]); //should not be any report ids at first

    expect(state1.reports).toEqual({}); //should not be any report objects at first

    createReportThunk(dispatchMock,getStateMock(state1));

    expect(dispatchMock.mock.calls.length).toBe(8);


    const updateReportActionArg = dispatchMock.mock.calls[0][0];

    expect(updateReportActionArg.type).toBe(UPDATE_REPORT);

    const state2 = reducer(state1,updateReportActionArg);
    expect(state2.reportIds).toEqual([1]);//should be one report id and it should be the number "1"
    expect(state2.reports['1']).toBeDefined()
    expect(state2.reports['1'].id).toBe(1);

    const createAssessmentDispatchMock = jest.fn();
    let assessmentState = state2;

    expect(assessmentState.assessmentIds).toEqual([]); //should not be any assessment ids at first

    expect(assessmentState.assessments).toEqual({}); //should not be any assessment objects at first

    for(var i = 1; i < 8; i++){
      let createAssessThunk = dispatchMock.mock.calls[i][0];
      createAssessThunk(createAssessmentDispatchMock,getStateMock(assessmentState));
      expect(createAssessmentDispatchMock.mock.calls.length).toBe(i);
      let updateAssessmentActionArg = createAssessmentDispatchMock.mock.calls[i - 1][0];
      expect(updateAssessmentActionArg.type).toBe(UPDATE_ASSESSMENT)
      assessmentState = reducer(assessmentState,updateAssessmentActionArg);
    }

    expect(assessmentState.assessmentIds).toEqual([1,2,3,4,5,6,7]); //should be seven assessments

    expect(assessmentState.assessments['1']).toBeDefined(); 
    expect(assessmentState.assessments['7']).toBeDefined(); 

  });

  it('Should edit properties of an assessment', () => {
      const state1 = createNewReportAndAssessments(reducer(undefined,{}));

      const ogAssessment1 = state1.assessments['1'];
      const report1 = state1.reports['1'];
      expect(ogAssessment1.isComplete).toBe(false);
      const state2 = reducer(state1,editAssessment(report1.id,ogAssessment1,{isComplete: true}))
      expect(state2.assessments['1'].isComplete).toBe(true);
      
  });
})

describe("Trivial reducers",() => {
  it('Tests all reducers that are intialized but never changed [include not switch statements]', () => {
     expect(bodySectionIds([1,2,3,4])).toEqual([1,2,3,4])
     expect(bodySections({id: test})).toEqual({id: test})
     expect(painLevels({id: test})).toEqual({id: test})
     expect(painLevelIds([1,2,3])).toEqual([1,2,3])
  })
})

describe("Reports Mark Pain",() => {
  it('Should mark pain and delete pain', () => {
    const state1 = createNewReport(reducer(undefined,{}));

    expect(state1.reportIds).toEqual([1])

    const firstBodySection = getFirstBodySection(state1);

    const highestPainLevel = getHighestPainLevel(state1);

    expect(state1.reports['1'].painMarkings).toBeDefined();
    expect(state1.reports['1'].painMarkings[firstBodySection.id]).toBeUndefined();

    const state2 = reducer(state1,markPain(firstBodySection,highestPainLevel.id,state1.reports['1'],true))

    expect(state2.reports['1'].painMarkings[firstBodySection.id]).toBeDefined();

    const state3 = reducer(state2,deletePain(firstBodySection,state1.reports['1'],true))

    expect(state3.reports['1'].painMarkings[firstBodySection.id]).toBeUndefined();
  })
})

describe("Marks Pain Allover in reports",() => {
  it('Should mark pain and delete pain allover', () => {
    const state1 = createNewReport(reducer(undefined,{}));
    const highestPainLevel = getHighestPainLevel(state1);

    expect(state1.reports['1'].painAllOver).toBe(0);

    const state2 = reducer(state1,markPainAllOver(highestPainLevel.id,state1.reports['1'])); 

    expect(state1.reports['1'].painAllOver).toBe(highestPainLevel.id);
  })
});


describe("view Reducer tests",() => {

  it('Should set and clear the system flash message state', () => {
    jest.useFakeTimers();

    const dispatchMock = jest.fn();
    let currentState: any = reducer(undefined,{});
    expect(currentState.view.flash.message).toBe("");
    expect(currentState.view.flash.open).toBe(false);
    const messageStartAction = messageStart("It's a message!");
    expect(messageStartAction).toEqual({
      type: APP_MESSAGE_START,
      message: "It's a message!"
    });
    currentState = reducer(currentState,messageStartAction);
    expect(currentState.view.flash.message).toBe("It's a message!");
    expect(currentState.view.flash.open).toBe(true);

    const messageClearAction = messageClear();
    expect(messageClearAction).toEqual({
      type: APP_MESSAGE_CLEAR
    });

    currentState = reducer(currentState,messageClearAction);
    expect(currentState.view.flash.message).toBe("");
    expect(currentState.view.flash.open).toBe(false);

    sendMessage("Holy flurking snit")(dispatchMock,() => currentState);

    jest.runAllTimers();

  });
});

describe("Assessments",() => {
  it("Should answer assessment questions",() => {
    const state1 = reducer(undefined,{});
    const reportId = 55;
    const report = makeReport(55,(new Date()).getTime());


    const state2 = reducer(state1,createReportAction(report));
    expect(state2.reports[reportId]).toBeDefined();
    expect(state2.reports[reportId].id).toBe(55);

    const firstAssessConfId = assessmentConfigIds[0];
    const firstAssessConf = AssessmentConfig[firstAssessConfId];

    const assessmentId = 33;
    const assessment = {...makeAssessment(assessmentId,firstAssessConf),reportId: reportId};

    const state3 = reducer(state2,editAssessment(reportId,assessment));

    expect(state3.assessments[assessmentId]).toBeDefined();
    expect(state3.assessments[assessmentId].id).toBe(assessmentId)

    const question = firstAssessConf.questions[0];
    let answer;

    expect(question.multiChoice).toBeDefined()
    if(question.multiChoice){
      answer = [11,12]
    } else {
      answer = 12
    }

    expect(state3.assessments[assessmentId].answers[question.id]).toBeUndefined();
    const state4 = reducer(state3,answerQuestion(assessment.id,question,answer))
    
    expect(state4.assessments[assessmentId].answers[question.id]).toBeDefined();
    expect(state4.assessments[assessmentId].answers[question.id]).toEqual(answer);

    //answer with invalid assessment and make sure state.assessments remains identical after call
    const state5 = reducer(state4,answerQuestion(76767,question,answer));
    expect(state5.assessments).toBe(state4.assessments);

  });

  it("Set assessment answers",() => {
      const state1  = {};
      //to make sure all branches are tested will supply irrelevant action.
      const state2 = assessmentsAnswers(state1,messageStart("Action"));
      

      expect(state2).toBe(state1); //state should be unchanged

      const firstAssessConfId = assessmentConfigIds[0];
      const firstAssessConf = AssessmentConfig[firstAssessConfId];

      const question = firstAssessConf.questions[0];
      let answer;

      expect(question.multiChoice).toBeDefined()
      if(question.multiChoice){
        answer = [11,12]
      } else {
        answer = 12
      }

      const state3 = assessmentsAnswers(state2,answerQuestion(333,question,answer));
      expect(state3).not.toBe(state2);

      //test default state when first arg is undefined,action should be irrelevant
      const defaultState = assessmentsAnswers(undefined,{type: 'asdfasdfasdfasdf'});
      expect(defaultState).toEqual({});
  });

  it("Set update  assessmentIds",() => {
      //should default to empty array
      //
      const state1 = reportAssessmentIds(undefined,{type: 'asdfasdfasdfasdf'});
      expect(state1).toEqual([]);
  });

})





