import {bodySections as bs, bodySectionIds as bsIds} from '../data/body';
import {painLevels as pLevels, painLevelIds as pLevelIds} from '../data/painLevels';
import {
  UPDATE_REPORT, 
  MARK_PAIN, 
  DELETE_PAIN, 
  UPDATE_ASSESSMENT,
  ANSWER_ASSESSMENT_QUESTION,
  APP_MESSAGE_START,
  APP_MESSAGE_CLEAR,
  MARK_PAIN_ALL_OVER
} from '../actions'
import {combineReducers} from 'redux';
import {arrayPushUnique} from './_helper';



// const initialReportId = 1;
const initialReports = {};

const initialView = {
  flash: {
    message: '',
    open: false
  }
  // paginators: {
    
  //     [paginatorId] : {
  //       page: number
  //     }
    
  // }
}

export function bodySectionIds(state = bsIds,action){
  return state;
}

export function bodySections(state = bs, action){
  return state;
}

export function painLevels(state = pLevels,action){
  return state;
}

export function painLevelIds(state = pLevelIds, action){
  return state;
}

export function assessmentsAnswers(answersState = {}, action){
  switch(action.type){
    case ANSWER_ASSESSMENT_QUESTION:
      answersState = {...answersState,[action.questionId]: action.answer};
      break;
    default:
  }
  return answersState;
}

function assessments(state = {}, action){
  switch(action.type){
    case UPDATE_ASSESSMENT:
      state = {...state,[action.assessment.id]: action.assessment}
      break;
    case ANSWER_ASSESSMENT_QUESTION:
      if(typeof state[action.assessmentId] !== 'undefined'){
        
        let assessment = {...state[action.assessmentId]};
        assessment.answers = assessmentsAnswers(assessment.answers,action);
        state = {...state,[action.assessmentId]: assessment};
      }
      break;
    default:
  }
  return state;
}

export function assessmentIds(state = [], action){
  switch(action.type){
    case UPDATE_ASSESSMENT:
      state = arrayPushUnique(action.assessment.id,state);
      break;
    default:
      break;
  }
  return state;
}

export function view(state = {...initialView}, action){
  switch(action.type){
    case APP_MESSAGE_START:
      state = {...state,flash: {message: action.message, open: true}};
      break;
    case APP_MESSAGE_CLEAR:
      state = {...state,flash: {message: '', open: false}};
      break;
    default:
      break;
  }
  return state;
}

export function reportAssessmentIds(state=[], action){
  switch(action.type){
    case UPDATE_ASSESSMENT:
      state = arrayPushUnique(action.assessment.id,state);
      break;
    default:
  }
  return state;
}

export function reports(state = {...initialReports}, action){
  switch(action.type){
    case UPDATE_REPORT:
      state = {...state,[action.report.id]: action.report}
      break;
    case MARK_PAIN:
      var reportMP = state[action.reportId];
      reportMP.painMarkings[action.bodySection.id] = action.painLevelId;
      state = {...state,[action.reportId]: reportMP}
      break;
    case MARK_PAIN_ALL_OVER:
      var report = state[action.reportId];
      report.painAllOver = action.painLevelId;
      state = {...state,[action.reportId]: report}
      break;
    case DELETE_PAIN:
      var reportDP = state[action.reportId];
      delete reportDP.painMarkings[action.bodySection.id];
      state = {...state,[action.reportId]: {...reportDP}};
      break;
    case UPDATE_ASSESSMENT:
      var reportUA = state[action.reportId];
      reportUA.assessmentIds = reportAssessmentIds(reportUA.assessmentIds,action)
      state = {...state,[action.reportId]: {...reportUA}};
      break;
    default:
  }
  return state;
}

export function reportIds(state = [], action){
  switch(action.type){
    case UPDATE_REPORT:
      state = arrayPushUnique(action.report.id,state);
      break;
    default:
      break;
  }
  return state;
}


export default combineReducers({
  bodySectionIds,
  bodySections,
  reportIds,
  reports,
  painLevels,
  painLevelIds,
  assessmentIds,
  assessments,
  view
})