# Opioid Safety
Where the nation heals its heroes...

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](#running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](#deployment) for more information.


# Cordova

There is currently an issue when buildingiOS

https://github.com/apache/cordova-ios/issues/407

In summary there isn't currently XCode 10 support so all cordova ios build/run commands need to be run with a build flag "--buildFlag='-UseModernBuildSystem=0'" or a cordova/build.json file needs to specify this flag.

This project uses cordova/build.json

When the issue above is resolved this flag should be removed from cordova/build.json




